/**
 *@author: Vishwajeet & Aliaksandr Satskou
 *@description: Updates Idibu Sender id In Job object based On recruiter of job.
**/
trigger JobTrigger on AVTRRT__Job__c (before insert, before update, after update) {
	if (Trigger.isBefore) {
		BCST__Job_Broadcasting_Settings__c v_defaultJobBroadcastingSetting =
				BCST__Job_Broadcasting_Settings__c.getOrgDefaults();
		String v_defaultIdibuSenderId = (v_defaultJobBroadcastingSetting != null)
				? v_defaultJobBroadcastingSetting.Idibu_User_Id__c
				: null;
		
		List<BCST__Job_Broadcasting_Settings__c> v_jobBroadcastingSettingList = [
				SELECT Idibu_User_Id__c, SetupOwnerId
				FROM BCST__Job_Broadcasting_Settings__c
		];
		Map<Id, String> v_setupOwnerIdToIdibuSenderIdMap = new Map<Id, String>();
		
		for (BCST__Job_Broadcasting_Settings__c v_jobBroadcastingSetting : v_jobBroadcastingSettingList) {
			v_setupOwnerIdToIdibuSenderIdMap.put(
					v_jobBroadcastingSetting.SetupOwnerId, v_jobBroadcastingSetting.Idibu_User_Id__c);
		}
		
		for (AVTRRT__Job__c v_job : Trigger.new) {
			if (v_job.AVTRRT__Recruiter__c != null) {
				String v_idibuSenderId = v_setupOwnerIdToIdibuSenderIdMap.get(
						v_job.AVTRRT__Recruiter__c);
				
				v_job.BCST__Idibu_Sender_Id__c = (v_idibuSenderId != null)
						? v_idibuSenderId
						: '';
			} else {
				v_job.BCST__Idibu_Sender_Id__c = (v_defaultIdibuSenderId != null)
						? v_defaultIdibuSenderId
						: '';
			}
		}
		if (Trigger.isUpdate) {
			// Making a call to handle JobPosting Status Update method
			// Commented by VK,03/28/2017
			//BroadBeanJobPostingUtils.handleJobPostingStatusUpdate(Trigger.oldMap, Trigger.newMap);
		}
		/****Added by vishwajeet, Date-08/26/2013, Case# : 00020988******/
	} else {
		if (BCST__Job_Broadcasting_Settings__c.getInstance().BCST__DeleteJobsFromJobBoardOnJobStageInactive__c) {
			
			List<AVTRRT__Job__c> v_JobList = [
					SELECT Id, (
							SELECT Id,BCST__Job_Board_Id__c,BCST__Idibu_Job_Id__c,BCST__Broadcasted__c
							FROM BCST__Job_Broadcast__r
							WHERE BCST__Broadcasted__c = TRUE
					)
					FROM AVTRRT__Job__c
					WHERE AVTRRT__Stage__c = 'Inactive'
							AND AVTRRT__Job__c.Id IN :Trigger.new
			];
			
			System.debug(LoggingLevel.ERROR, '::::v_JobList=' + v_JobList);
			
			Map<String, List<String>> v_jobDeleteMap = new Map<String, List<String>>();
			for (AVTRRT__Job__c v_job : v_JobList) {
				Set<String> v_jobBoardMap = new Set<String>();
				List<String> v_jobBoardList = new List<String>();
				for (BCST__Job_Broadcast__c v_jobBroadcast : v_job.BCST__Job_Broadcast__r) {
					if (v_jobBoardMap.add(v_jobBroadcast.BCST__Job_Board_Id__c)) {
						v_jobBoardList.add(v_jobBroadcast.BCST__Job_Board_Id__c);
					}
				}
				v_jobDeleteMap.put(v_job.Id, v_jobBoardList);
				System.debug(LoggingLevel.ERROR, '::::v_jobBoardList=' + v_jobBoardList);
			}
			System.debug(LoggingLevel.ERROR, '::::TriggerJobDeleteList=' + v_jobDeleteMap);
			
			List<JobDelete> v_jobDeleteList = IdibuHelper.getJobDeleteList(v_jobDeleteMap);
			System.debug(LoggingLevel.ERROR, '::::v_jobDeleteList=' + v_jobDeleteList);
			
			//fix for case #00033280 by Pandeiswari
			if (v_jobDeleteList.size() > 0) {
				IdibuJobDelete v_idibuJobDeteteBatch = new IdibuJobDelete(v_jobDeleteList);
				Database.executeBatch(v_idibuJobDeteteBatch, 1);
			}
		
		}
	}
}