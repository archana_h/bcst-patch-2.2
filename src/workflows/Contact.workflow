<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AVTRRT__JobNotifiedtoCandidate</fullName>
        <description>Job Notified to Candidate</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__NotifyJobToCandidates</template>
    </alerts>
    <alerts>
        <fullName>AVTRRT__ThankyouEmailtoCandidate</fullName>
        <description>Thankyou Email to Candidate</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__ThankyouMailOnlineSubmissionCandidate</template>
    </alerts>
    <fieldUpdates>
        <fullName>AVTRRT__JobNotified</fullName>
        <description>Job notification to candidate</description>
        <field>AVTRRT__Jobs_Notified__c</field>
        <name>Job Notified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AVTRRT__Update_LastName</fullName>
        <description>Updating Candidate LastName with FirstName when LastName contains &quot;name not parsed&quot;</description>
        <field>LastName</field>
        <formula>FirstName</formula>
        <name>Update LastName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AVTRRT__Update_Lead_Source</fullName>
        <field>LeadSource</field>
        <literalValue>Online Submission</literalValue>
        <name>Update Lead Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AVTRRT__Update_Resume_Source</fullName>
        <field>AVTRRT__Source__c</field>
        <literalValue>Online Submission</literalValue>
        <name>Update Resume Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AVTRRT__Setting Resume Source and Lead Source</fullName>
        <actions>
            <name>AVTRRT__Update_Lead_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>AVTRRT__Update_Resume_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.OwnerId</field>
            <operation>contains</operation>
            <value>Site Guest User</value>
        </criteriaItems>
        <description>PLEASE DO NOT DEACTIVATE THIS RULE</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AVTRRT__Thankyou Email Notification To Candidate</fullName>
        <actions>
            <name>AVTRRT__ThankyouEmailtoCandidate</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.AVTRRT__Source__c</field>
            <operation>equals</operation>
            <value>Online Submission</value>
        </criteriaItems>
        <description>Notification to Candidate for online application.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AVTRRT__Update LastName</fullName>
        <actions>
            <name>AVTRRT__Update_LastName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.LastName</field>
            <operation>contains</operation>
            <value>name not parsed</value>
        </criteriaItems>
        <description>Updating Candidate LastName with FirstName when LastName contains &quot;name not parsed&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
