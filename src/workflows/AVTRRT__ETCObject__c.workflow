<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>AVTRRT__EmailToCandidate_Submission</fullName>
        <apiVersion>21.0</apiVersion>
        <endpointUrl>https://resumeservice.targetrecruit.net/importresume/soap/notifications</endpointUrl>
        <fields>AVTRRT__Candidate__c</fields>
        <fields>AVTRRT__Name__c</fields>
        <fields>AVTRRT__Status__c</fields>
        <fields>AVTRRT__User__c</fields>
        <fields>AVTRRT__sessionId__c</fields>
        <fields>AVTRRT__sfURL__c</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>bcst.archana.h@targetrecruit.net_2.2.patch</integrationUser>
        <name>EmailToCandidate Submission</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>AVTRRT__Test</fullName>
        <apiVersion>8.0</apiVersion>
        <endpointUrl>https://resumeservice.targetrecruit.net/importresume/soap/notifications</endpointUrl>
        <fields>AVTRRT__Candidate__c</fields>
        <fields>AVTRRT__Name__c</fields>
        <fields>AVTRRT__Status__c</fields>
        <fields>AVTRRT__User__c</fields>
        <fields>AVTRRT__sessionId__c</fields>
        <fields>AVTRRT__sfURL__c</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>bcst.archana.h@targetrecruit.net_2.2.patch</integrationUser>
        <name>Test</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>AVTRRT__EmailToCandidate Submission</fullName>
        <actions>
            <name>AVTRRT__EmailToCandidate_Submission</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>AVTRRT__ETCObject__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>AVTRRT__ETCObject__c.AVTRRT__Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>PLEASE DO NOT DEACTIVATE THIS RULE</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
