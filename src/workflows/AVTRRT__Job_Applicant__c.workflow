<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AVTRRT__NewCandidateassignedtoaJob</fullName>
        <description>New Candidate assigned to a Job</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__NewOnlineJobApplicantNotification</template>
    </alerts>
    <alerts>
        <fullName>AVTRRT__NotificationtoRecruiteronRejection</fullName>
        <description>Notification to Recruiter on Rejection</description>
        <protected>false</protected>
        <recipients>
            <field>AVTRRT__Recruiter__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__NotificationtoRecruiteronRejection</template>
    </alerts>
    <alerts>
        <fullName>AVTRRT__NotifyRecruiterwithnewJobApplicant</fullName>
        <description>Notify Recruiter with new Job Applicant</description>
        <protected>false</protected>
        <recipients>
            <field>AVTRRT__Assigned_Recruiter__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>AVTRRT__Recruiter__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__NewOnlineJobApplicantNotification</template>
    </alerts>
    <alerts>
        <fullName>AVTRRT__SendresumetotheAccountManager</fullName>
        <description>Send resume to the Account Manager</description>
        <protected>false</protected>
        <recipients>
            <field>AVTRRT__Account_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__SendresumetotheHiringManager</template>
    </alerts>
    <fieldUpdates>
        <fullName>AVTRRT__Job_Applicant_Stage_to_submitted</fullName>
        <field>AVTRRT__Stage__c</field>
        <literalValue>Submitted to Client</literalValue>
        <name>Job_Applicant_Stage_to_submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AVTRRT__SetSearchDate</fullName>
        <field>AVTRRT__Search_Date__c</field>
        <formula>NOW()</formula>
        <name>SetSearchDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AVTRRT__Updateworkdetails</fullName>
        <field>AVTRRT__Resume__c</field>
        <formula>AVTRRT__Contact_Candidate__r.AVTRRT__Resume__c</formula>
        <name>Update workdetails</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AVTRRT__Workdetail</fullName>
        <field>AVTRRT__Candidate_Write_Up__c</field>
        <formula>if( AVTRRT__Contact_Candidate__c !=null,AVTRRT__Contact_Candidate__c,&apos;&apos;)</formula>
        <name>Work detail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AVTRRT__Job Applicant created By Recruiter</fullName>
        <actions>
            <name>AVTRRT__SendresumetotheAccountManager</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>AVTRRT__Job_Applicant__c.Name</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>AVTRRT__Job_Applicant__c.AVTRRT__Type__c</field>
            <operation>equals</operation>
            <value>Recruiter</value>
        </criteriaItems>
        <criteriaItems>
            <field>AVTRRT__Job_Applicant__c.AVTRRT__Job_Title__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>AVTRRT__Job_Applicant__c.AVTRRT__Resume__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Notification when a Job Applicant is created by the recruiter (To Account Manager).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AVTRRT__Job_Applicant_Stage_change</fullName>
        <active>false</active>
        <criteriaItems>
            <field>AVTRRT__Job_Applicant__c.AVTRRT__Stage__c</field>
            <operation>equals</operation>
            <value>New Application</value>
        </criteriaItems>
        <description>NOT USED</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>AVTRRT__New Online Job Applicant</fullName>
        <actions>
            <name>AVTRRT__NotifyRecruiterwithnewJobApplicant</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>AVTRRT__Job_Applicant__c.AVTRRT__Type__c</field>
            <operation>equals</operation>
            <value>Online</value>
        </criteriaItems>
        <criteriaItems>
            <field>AVTRRT__Job_Applicant__c.AVTRRT__Assigned_Recruiter__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>AVTRRT__Job_Applicant__c.AVTRRT__Job_Title__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Notification to Recruiter when a candidate applies online.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>AVTRRT__Notification to Recruiter on Job Applicant Rejection</fullName>
        <actions>
            <name>AVTRRT__NotificationtoRecruiteronRejection</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>AVTRRT__Job_Applicant__c.AVTRRT__Stage__c</field>
            <operation>contains</operation>
            <value>Rejected by Account Manager,Rejected by Candidate,Rejected by Client</value>
        </criteriaItems>
        <description>Notification to Recruiter on Job Applicant Rejection</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AVTRRT__SetEOODate</fullName>
        <actions>
            <name>AVTRRT__SetSearchDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>AVTRRT__Job_Applicant__c.AVTRRT__Search_Criteria_Long__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>PLEASE DO NOT DEACTIVATE THIS RULE</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AVTRRT__Update Jobapplicant Resume</fullName>
        <actions>
            <name>AVTRRT__Updateworkdetails</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>AVTRRT__Job_Applicant__c.AVTRRT__Job_Title__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>AVTRRT__Job_Applicant__c.AVTRRT__Resume__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>PLEASE DO NOT DEACTIVATE THIS RULE</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>AVTRRT__SendresumetotheHiringManager</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Send resume to the Hiring Manager</subject>
    </tasks>
</Workflow>
