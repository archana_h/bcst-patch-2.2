<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AVTRRT__EmailtoalltheuserswithPlacementdetails</fullName>
        <description>Email to all the users with Placement details</description>
        <protected>false</protected>
        <recipients>
            <field>AVTRRT__Account_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>AVTRRT__Assigned_Recruiter__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>AVTRRT__Recruiter__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__EmailforNewPlacement</template>
    </alerts>
    <alerts>
        <fullName>AVTRRT__PlacementTerminationNotificationRecruiter</fullName>
        <description>Placement Termination Notification (Recruiter)</description>
        <protected>false</protected>
        <recipients>
            <field>AVTRRT__Assigned_Recruiter__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>AVTRRT__Recruiter__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__PlacementTerminationNotificationMailRecruiter</template>
    </alerts>
    <alerts>
        <fullName>AVTRRT__RateChangeNotificationAccountManager</fullName>
        <description>Rate Change Notification (Account Manager)</description>
        <protected>false</protected>
        <recipients>
            <field>AVTRRT__Account_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__PlacementRateChangeNotificationMailAccountManager</template>
    </alerts>
    <alerts>
        <fullName>AVTRRT__RateChangeNotificationRecruiter</fullName>
        <description>Rate Change Notification (Recruiter)</description>
        <protected>false</protected>
        <recipients>
            <field>AVTRRT__Assigned_Recruiter__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>AVTRRT__Recruiter__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__PlacementRateChangeNotificationMailRecruiter</template>
    </alerts>
    <alerts>
        <fullName>AVTRRT__SendsemailtoalltheuserswithPlacementdetails</fullName>
        <description>Sends email to all the users with Placement details</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__NewPlacementNotificationAllUsers</template>
    </alerts>
    <alerts>
        <fullName>AVTRRT__UpdateAccountOwnerwithTerminationdetailsAccountManager</fullName>
        <description>Update Account Owner with Termination details (Account Manager)</description>
        <protected>false</protected>
        <recipients>
            <field>AVTRRT__Account_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__PlacementTerminationNotificationMailAccountingManager</template>
    </alerts>
    <alerts>
        <fullName>AVTRRT__UpdateRecruiterwithTerminationdetails</fullName>
        <description>Update Recruiter with Termination details</description>
        <protected>false</protected>
        <recipients>
            <field>AVTRRT__Recruiter__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__PlacementTerminationNotificationMailRecruiter</template>
    </alerts>
    <fieldUpdates>
        <fullName>AVTRRT__UpdateNetMarginforRating</fullName>
        <field>AVTRRT__NetMargin__c</field>
        <formula>AVTRRT__Net_Margin__c</formula>
        <name>Update NetMargin for Rating</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AVTRRT__Notification on Placement Done</fullName>
        <actions>
            <name>AVTRRT__EmailtoalltheuserswithPlacementdetails</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>AVTRRT__Placement__c.Name</field>
            <operation>notEqual</operation>
            <value>P00000000</value>
        </criteriaItems>
        <criteriaItems>
            <field>AVTRRT__Placement__c.AVTRRT__Bill_Rate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Whenever a placement is done - email alert for everyone in the company</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AVTRRT__Notification on Placement Termination</fullName>
        <actions>
            <name>AVTRRT__PlacementTerminationNotificationRecruiter</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>AVTRRT__UpdateAccountOwnerwithTerminationdetailsAccountManager</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>AVTRRT__Placement__c.AVTRRT__Terminated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notification for Placement Termination (To Account Manager and Recruiter).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AVTRRT__Rate Change Notification</fullName>
        <actions>
            <name>AVTRRT__RateChangeNotificationAccountManager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>AVTRRT__RateChangeNotificationRecruiter</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Rate Change Notification</description>
        <formula>ISCHANGED(AVTRRT__Bill_Rate__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>AVTRRT__Update NetMargin for Rating</fullName>
        <actions>
            <name>AVTRRT__UpdateNetMarginforRating</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>AVTRRT__Placement__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>PLEASE DO NOT DEACTIVATE THIS RULE</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
