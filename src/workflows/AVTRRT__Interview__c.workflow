<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AVTRRT__Candidate_Interview_email</fullName>
        <description>Candidate Interview email notification</description>
        <protected>false</protected>
        <recipients>
            <field>AVTRRT__Contact_Candidate__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__InterviewScheduledMailToCandidate</template>
    </alerts>
    <alerts>
        <fullName>AVTRRT__InterviewScheduledMailAccountManager</fullName>
        <description>Interview Scheduled Mail (Account Manager)</description>
        <protected>false</protected>
        <recipients>
            <field>AVTRRT__Account_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__InterviewScheduledMailToAccountManager</template>
    </alerts>
    <alerts>
        <fullName>AVTRRT__InterviewScheduledMailCandidate</fullName>
        <description>Interview Scheduled Mail (Candidate)</description>
        <protected>false</protected>
        <recipients>
            <field>AVTRRT__Contact_Candidate__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__InterviewScheduledMailToCandidate</template>
    </alerts>
    <alerts>
        <fullName>AVTRRT__InterviewScheduledMailHiringManager</fullName>
        <description>Interview Scheduled Mail (Hiring Manager)</description>
        <protected>false</protected>
        <recipients>
            <field>AVTRRT__Hiring_Manager__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__InterviewScheduledMailToRecruiter</template>
    </alerts>
    <alerts>
        <fullName>AVTRRT__InterviewSetup</fullName>
        <description>Interview Setup</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>AVTRRT__Hiring_Manager__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__InterviewSetup</template>
    </alerts>
    <alerts>
        <fullName>AVTRRT__Interviewing_Person_Event_notification</fullName>
        <description>Interviewing Person Event notification</description>
        <protected>false</protected>
        <recipients>
            <field>AVTRRT__Hiring_Manager__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__InterviewScheduledMailToRecruiter</template>
    </alerts>
    <alerts>
        <fullName>AVTRRT__NotificationonInterviewChanged</fullName>
        <description>Notification on Interview Changed</description>
        <protected>false</protected>
        <recipients>
            <field>AVTRRT__Candidate_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>AVTRRT__Interviewing_Person_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__InterviewChange</template>
    </alerts>
    <alerts>
        <fullName>AVTRRT__RateInterview</fullName>
        <description>Rate Interview</description>
        <protected>false</protected>
        <recipients>
            <field>AVTRRT__Account_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__InterviewReminder</template>
    </alerts>
    <alerts>
        <fullName>AVTRRT__RatetheInterview</fullName>
        <description>Rate the Interview</description>
        <protected>false</protected>
        <recipients>
            <field>AVTRRT__Account_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AVTRRT__RecruitingTemplates/AVTRRT__InterviewReminder</template>
    </alerts>
    <rules>
        <fullName>AVTRRT__Candidate Event Notification</fullName>
        <actions>
            <name>AVTRRT__Candidate_Interview_email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>AVTRRT__Interview__c.AVTRRT__Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>NOT USED</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AVTRRT__Interview Schedule Notification Email</fullName>
        <actions>
            <name>AVTRRT__InterviewScheduledMailAccountManager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>AVTRRT__InterviewScheduledMailCandidate</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>AVTRRT__InterviewScheduledMailHiringManager</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>AVTRRT__Interview__c.AVTRRT__Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Notification for Interview scheduled (To Recruiter, Account Manager and Candidate). NOT USED</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AVTRRT__Interview Set Up</fullName>
        <actions>
            <name>AVTRRT__InterviewSetup</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>AVTRRT__Interview__c.Name</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>AVTRRT__Interview__c.AVTRRT__Type_Of_Interview__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>NOT USED</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>AVTRRT__Interviewing Person Event Notification</fullName>
        <actions>
            <name>AVTRRT__Interviewing_Person_Event_notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>AVTRRT__Interview__c.AVTRRT__Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>NOT USED</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AVTRRT__Notification on Interview Change</fullName>
        <active>false</active>
        <description>NOT USED</description>
        <formula>AVTRRT__Start_Date__c &gt; PRIORVALUE( AVTRRT__Start_Date__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>AVTRRT__InterviewReminder</fullName>
        <assignedTo>bcst.archana.h@targetrecruit.net_2.2.patch</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Interview Reminder</description>
        <dueDateOffset>-1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>AVTRRT__Interview__c.AVTRRT__Start_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Interview Reminder</subject>
    </tasks>
</Workflow>
