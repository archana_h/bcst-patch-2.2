/**
* @description Helper class for Grouping fields based on various criteria like by Id, etc.
*/
public with sharing class GroupByHelper {
	private static Logger logger = new Logger('GroupByHelper');
	
	/**
* @name groupByField
* @param recordsToGroup List<sObject>
* @param keyField String
* @return Map<Object, List<sObject>>
*/
	// Usage: (Map<Object, List<TMS__Time__c>>)groupByField(times, 'TMS__Case__c');
	public static Map<Object, List<SObject>> groupByField(
			List<SObject> recordsToGroup, String keyField) {
		
		Map<Object, List<SObject>> keyToRecordsMap = new Map<Object, List<SObject>>();
		
		if (recordsToGroup != null) {
			for (SObject record : recordsToGroup) {
				Object key = MetadataUtilNew.getValue(record, keyField);
				
				logger.log('groupByField', 'key', key);
				
				List<SObject> recordList = keyToRecordsMap.get(key);
				if (recordList == null) {
					recordList = new List<SObject>();
					keyToRecordsMap.put(key, recordList);
				}
				
				recordList.add(record);
			}
		}
		
		return keyToRecordsMap;
	}

/**
* @name groupByFieldKeyId
* @param recordsToGroup List<sObject>
* @param keyField String
* @return Map<Id, List<sObject>>
*/
	public static Map<Id, List<SObject>> groupByFieldKeyId(
			List<SObject> recordsToGroup, String keyField) {
		
		Map<Id, List<SObject>> mapKeyId = new Map<Id, List<SObject>>();
		
		Map<Object, List<SObject>> mapKeyObject = groupByField(recordsToGroup, keyField);
		for (Object keyObj : mapKeyObject.keySet()) {
			Id keyId = (Id)keyObj;
			mapKeyId.put(keyId, mapKeyObject.get(keyObj));
		}
		
		return mapKeyId;
	}
}