/**
 Class

 @name MappingHelper
 @author Aliaksandr Satskou
 @description Helper class, which allows to get mapped values for specified extrafields.
 @data 08/14/2012
*/
public with sharing class MappingHelper {
	
	private static Map<Id, AVTRRT__Job__c> jobsMap;
	private static AVTRRT__Job__c job;
	private static List<BCST__JobBroadcasting_Field_Mapping__c> mappingList;
	
	/**
	 Method

	 @name initData
	 @author Aliaksandr Satskou
	 @data 08/14/2012
	 @description Getting list of 'BCST__JobBroadcasting_Field_Mapping__c' objects; Getting list of 'Job'
		objects.
	 @param Set of 'Job' object IDs.
	*/
	public static void initData(Set<String> p_jobIdSet) {
		mappingList = getMappingList();
		jobsMap = getJobs(p_jobIdSet);
	}
	
	/**
	 Method

	 @name initJob
	 @author Aliaksandr Satskou
	 @data 08/14/2012
	 @description Initialize 'Job' object.
	 @param Id of 'Job' object.
	*/
	public static void initJob(String p_jobId) {
		job = jobsMap.get(p_jobId);
	}
	
	/**
	 Method

	 @name getMappingList
	 @author Aliaksandr Satskou
	 @data 08/14/2012
	 @description Getting list of all 'BCST__JobBroadcasting_Field_Mapping__c' objects.
	 @return: List of 'BCST__JobBroadcasting_Field_Mapping__c' objects.
	*/
	public static List<BCST__JobBroadcasting_Field_Mapping__c> getMappingList() {
		return [
				SELECT BCST__Extrafield_Name__c, BCST__Mapped_Formula__c, (
						SELECT BCST__Extrafield_Mapped_Value__c, BCST__Extrafield_Parent_Value__c,
								BCST__Job_Field_Value__c
						FROM BCST__JobBroadcasting_Values_Mapping__r
				)
				FROM BCST__JobBroadcasting_Field_Mapping__c
		];
	}
	
	/**
	 Method

	 @name getMappingByName
	 @author Aliaksandr Satskou
	 @data 08/14/2012
	 @description Getting 'BCST__JobBroadcasting_Field_Mapping__c' by extrafield name.
	 @param Extrafield name.
	 @return: 'BCST__JobBroadcasting_Field_Mapping__c' object.
	*/
	private static BCST__JobBroadcasting_Field_Mapping__c getMappingByName(String p_extrafieldName) {
		if (mappingList != null) {
			for (BCST__JobBroadcasting_Field_Mapping__c v_mapping : mappingList) {
				if (v_mapping.BCST__Extrafield_Name__c == p_extrafieldName) {
					return v_mapping;
				}
			}
		}
		
		return null;
	}
	
	/**
	 Method

	 @name getJobs
	 @author Aliaksandr Satskou
	 @data 08/14/2012
	 @description Getting 'BCST__JobBroadcasting_Field_Mapping__c' by extrafield name.
	 @param Set of 'Job' object IDs.
	 @return: Map of selected 'Job' objects.
	*/
	private static Map<Id, AVTRRT__Job__c> getJobs(Set<String> p_jobIdSet) {
		if (mappingList != null) {
			String v_query = 'SELECT ';
			String v_whatClause = '';
			
			Set<String> v_jobFieldsSet = new Set<String> {'Id'};
			
			for (BCST__JobBroadcasting_Field_Mapping__c v_mapping : mappingList) {
				String v_mappedFormula = v_mapping.BCST__Mapped_Formula__c;
				
				if (v_mappedFormula.startsWith('{')) {
					v_jobFieldsSet.add(v_mappedFormula.substring(2, v_mappedFormula.length() - 1));
				}
			}
			
			for (String v_jobFieldName : v_jobFieldsSet) {
				v_whatClause += ', ' + v_jobFieldName;
			}
			v_whatClause = v_whatClause.substring(2);
			
			v_query += v_whatClause + ' FROM AVTRRT__Job__c WHERE Id IN :p_jobIdSet';
			List<AVTRRT__Job__c> v_jobList = Database.query(v_query);
			
			Map<Id, AVTRRT__Job__c> v_idJobMap = new Map<Id, AVTRRT__Job__c>();
			
			for (AVTRRT__Job__c v_job : v_jobList) {
				v_idJobMap.put(v_job.Id, v_job);
			}
			
			return v_idJobMap;
		}
		
		return null;
	}
	
	/**
	 Method

	 @name getMappedFormulaValue
	 @author Aliaksandr Satskou
	 @data 08/14/2012
	 @description Getting mapped value by extrafield name.
	 @param Extrafield name.
	 @return: Mapped value.
	*/
	public static String getMappedFormulaValue(String p_extrafieldName) {
		//mappingList = getMappingList();
		BCST__JobBroadcasting_Field_Mapping__c v_mapping = getMappingByName(p_extrafieldName);
		
		System.debug(LoggingLevel.ERROR, ':::::::::::::::getMappedFormulaValue p_extrafieldName=' + p_extrafieldName);
		System.debug(LoggingLevel.ERROR, ':::::::::::::::getMappedFormulaValue v_mapping=' + v_mapping);
		
		if (v_mapping == null) {
			return null;
		}
		
		String v_mappedFormula = v_mapping.BCST__Mapped_Formula__c;
		
		if (v_mappedFormula.startsWith('{')) {
			
			String v_jobFieldName = v_mappedFormula.substring(2, v_mappedFormula.length() - 1);
			System.debug(LoggingLevel.ERROR, ':::::::::::::::job' + job);
			String v_jobFieldValue = (String)job.get(v_jobFieldName);
			
			System.debug(LoggingLevel.ERROR, ':::::::::::::::v_jobFieldValue=' + v_jobFieldValue);
			
			if ((v_jobFieldValue == null) || (v_jobFieldValue == '')) {
				return '';
			}
			
			for (BCST__JobBroadcasting_Value_Mapping__c v_valueMapping :
					v_mapping.BCST__JobBroadcasting_Values_Mapping__r) {
				
				if (v_valueMapping.BCST__Job_Field_Value__c == v_jobFieldValue) {
					if ((v_valueMapping.BCST__Extrafield_Parent_Value__c != null) &&
							(v_valueMapping.BCST__Extrafield_Parent_Value__c != '')) {
						return v_valueMapping.BCST__Extrafield_Parent_Value__c + '?' +
								v_valueMapping.BCST__Extrafield_Mapped_Value__c;
					} else {
						return v_valueMapping.BCST__Extrafield_Mapped_Value__c;
					}
				}
			}
			
			return v_jobFieldValue;
		}
		
		return v_mappedFormula;
	}
}