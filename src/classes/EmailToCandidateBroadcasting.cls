public class EmailToCandidateBroadcasting {
	
	private AVTRRT__ETCObject__c etcObject;
	private Boolean resumeIsAttached = false;
	
	// ETC record which is used to temporarily store cover letters sent to Email service.
	private AVTRRT__ETCObject__c etcForCoverLetter;
	
	public EmailToCandidateBroadcasting() {
		
		etcForCoverLetter = [
				SELECT Id
				FROM AVTRRT__ETCObject__c
				WHERE AVTRRT__Name__c = 'Cover Attachments'
				LIMIT 1
		];
	}
	
	/**
	* @author Mikhail Ivanov
	* @date 08/20/2012
	* @description This method should be called from EmailToCandidate service.
	Example in class EmailToCandidateEmailHandler.
	* @param p_email Email entity which was sent to Email Service.
	*/
	public void createETCObject(Messaging.InboundEmail p_email) {
		
		etcObject = new AVTRRT__ETCObject__c(
				AVTRRT__Name__c = p_email.subject,
				AVTRRT__Status__c = 'New',
				AVTRRT__Job_Id__c = getJobNumbersFromSubject(p_email.subject),
				AVTRRT__From__c = p_email.fromAddress,
				AVTRRT__To__c = '' + p_email.toAddresses);
		
		/* Added by Aliaksandr Satskou, 05/13/2014 (case #00027557) */
		Boolean isSaveEmailBody =
				(Boolean)ConfigHelper.getConfigSetting('AVTRRT__Save_Email_Body_to_ETC__c');
		if (isSaveEmailBody) {
			etcObject.AVTRRT__Email_Body__c = p_email.plainTextBody;
		}
		
		customizeETCObject(etcObject, p_email);
		
		insert etcObject;
		
		Messaging.InboundEmail.BinaryAttachment[] v_binaryAttachments = p_email.binaryAttachments;
		if (v_binaryAttachments != null) {
			for (Messaging.InboundEmail.BinaryAttachment v_attachment : v_binaryAttachments) {
				insertAttachment(v_attachment.fileName, v_attachment.body);
			}
		}
		
		Messaging.InboundEmail.TextAttachment[] v_textAttachments = p_email.textAttachments;
		if (v_textAttachments != null) {
			for (Messaging.InboundEmail.TextAttachment v_attachment : v_textAttachments) {
				insertAttachment(v_attachment.fileName, Blob.valueOf(v_attachment.body));
			}
		}
		
		Boolean enableGetEmailBody =
				(Boolean)ConfigHelper.getConfigSetting('AVTRRT__Enable_Get_Email_Body__c');
		if (enableGetEmailBody == true) {
			if (p_email.htmlBody != null && p_email.htmlBody != '') {
				String plainText = HtmlHelper.StripHtml(p_email.htmlBody);
				plainText = HtmlHelper.plainToHtml(plainText);
				/* Edited by Aliaksandr Satskou, 08/25/2014 (case #00030820, point 12) */
				Boolean isEmptyBody = (plainText.replace('<br />', '').trim() == '');
				
				if (!isEmptyBody) {
					insertAttachment(
							resumeIsAttached
									? 'Cover Letter.pdf'
									: 'Resume.pdf',
							Blob.toPdf(plainText));
				}
			}
		}
	}
	
	/**
	* @author Mikhail Ivanov
	* @date 08/20/2012
	* @description Parses email subject to find all numbers there
	* and then checks if such numbers there in the database as Job Numbers.
	* @param p_subject Email subject
	* @return String Separated list of found real jobs from email title. Example:
	*       '103;23;45;24'
	*/
	private String getJobNumbersFromSubject(String p_subject) {
		if (p_subject != null && p_subject != '') {
			Pattern v_pattern = Pattern.compile('\\d+');
			Matcher v_matcher = v_pattern.matcher(p_subject);
			
			Set<String> v_jobNumbers = new Set<String>();
			
			while (v_matcher.find()) {
				v_jobNumbers.add(v_matcher.group(0));
			}
			
			List<AVTRRT__Job__c> v_jobList = [
					SELECT Name
					FROM AVTRRT__Job__c
					WHERE Name IN :v_jobNumbers
			];
			
			String commaSeparatedJobNumbers = '';
			for (AVTRRT__Job__c v_job : v_jobList) {
				commaSeparatedJobNumbers += ';' + v_job.Name;
			}
			
			if (commaSeparatedJobNumbers != '') {
				return commaSeparatedJobNumbers.substring(1);
			}
		}
		
		return null;
	}
	
	/**
	* @author Aliaksandr Satskou
	* @date 09/27/2012
	* @name customizeETCObject
	* @description Method parse e-mail headers and populate 'ETCObject' fields.
	* @param ETCObject p_etcObject
	* @param Messaging.InboundEmail p_email
	* @return void
	*/
	public static void customizeETCObject(
			AVTRRT__ETCObject__c p_etcObject, Messaging.InboundEmail p_email) {
		
		if (p_email.headers != null) {
			for (Messaging.InboundEmail.Header v_header : p_email.headers) {
				/* to populate AVTRRT__Job_Id__c field */
				if (v_header.name == 'X-Itrackid-Reference') {
					/*Modified by vishwajeet,case#-14769,date-03/04/20138 */
					//p_etcObject.AVTRRT__Job_Id__c = v_header.value.substring(0, v_header.value.indexOf('/'));
					p_etcObject.AVTRRT__Job_Id__c = v_header.value;
				} /* to populate AVTRRT__Source__c field */ else if (v_header.name == 'X-Itrackid-Portal-Name') {
					p_etcObject.AVTRRT__Source__c = v_header.value;
				}
				/* commented by Aliaksandr Satskou, 09/27/2012 */
				/*
				**	else if (v_header.name == 'X-Itrackid-Title') {
				**		p_etcObject.AVTRRT__Name__c = v_header.value + ' - Resume';
				**	}
				*/
			}
		}
	}
	/**
	* @author Mikhail Ivanov
	* @date 08/20/2012
	* @description Checks the type of attachment and attach it to new ETC record or
	* to special ETC record for temporarily stored Cover Letters
	* @param p_fileName Attachment File Name
	* @param p_fileBody Attachment Body
	*/
	private void insertAttachment(String p_fileName, Blob p_fileBody) {
		
		String v_fileNameLowerCase = p_fileName.toLowerCase();
		if (
				v_fileNameLowerCase.endsWith('doc') ||
						v_fileNameLowerCase.endsWith('docx') ||
						v_fileNameLowerCase.endsWith('rtf') ||
						v_fileNameLowerCase.endsWith('txt') ||
						v_fileNameLowerCase.endsWith('pdf') ||
						v_fileNameLowerCase.endsWith('html')) {
			
			// List of possible cover letter name keywords specified in 'Cover Letter' config setting. Default value:
			// 'Cover;Letter;cvr;ltr;cov'
			String[] coverLetterWordList =
					ConfigHelper.GetConfig('Cover Letter').AVTRRT__Value__c.split(';');
			
			Boolean v_isCoverLetter = false;
			for (String v_coverLetterWord : coverLetterWordList) {
				if (v_fileNameLowerCase.contains(v_coverLetterWord.toLowerCase())) {
					v_isCoverLetter = true;
					break;
				}
			}
			
			Id etcId;
			if (v_isCoverLetter) {
				etcId = etcForCoverLetter.Id;
			} else {
				etcId = etcObject.Id;
				
				resumeIsAttached = true;
			}
			
			Files.create(new Files.File(p_fileName, p_fileBody, etcId));
		}
	}
}