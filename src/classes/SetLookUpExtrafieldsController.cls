/**
* @author Aliaksandr Satskou
* @date 07/08/2013
* @description Custom lookup window controller.
*/
public with sharing class SetLookUpExtrafieldsController {

	public String boardId { get; set; }
	public String parentValue { get; set; }
	public String extraName { get; set; }

	public String htmlElementId { get; set; }

	public String keyword { get; set; }
	public List<SelectOption> baseItemList;

	public List<SelectOption> itemList { get; set; }
	public String itemSelected { get; set; }
	public static String boardXml;
	//public String boardXml;

	/**
	* @name SetLookUpExtrafieldsController
	* @author Aliaksandr Satskou
	* @data 07/08/2013
	* @description Getting URL parameters and loading all needed data.
	*/
	public SetLookUpExtrafieldsController() {
		boardId = ApexPages.currentPage().getParameters().get('board_id');
		extraName = ApexPages.currentPage().getParameters().get('extra_name');

		parentValue = ApexPages.currentPage().getParameters().get('parent_val');
		System.debug('..parentValue..' + parentValue);

		htmlElementId = ApexPages.currentPage().getParameters().get('target_id');

		itemSelected = ApexPages.currentPage().getParameters().get('val');

		if (boardId != null) {
			Id idibuFolderId = IdibuHelper.getIdibuFolderId();
			boardXml = IdibuHelper.getJobBoardIdXMLMap(idibuFolderId).get(boardId);
		}

		createBaseItemList();
		go();
	}

	/**
	* @name createBaseItemList
	* @author Aliaksandr Satskou
	* @data 07/08/2013
	* @description Parsing XML of job board.
	* @return void
	*/
	public void createBaseItemList() {
		baseItemList = new List<SelectOption>();

		if (boardXml != null) {
			Dom.Document document = new Dom.Document();
			document.load(boardXml);

			Dom.XmlNode boardsNode = document.getRootElement().getChildElement('boards', null);
			Dom.XmlNode boardNode = boardsNode.getChildElement('board', null);

			Dom.XmlNode extrafieldsNode = boardNode.getChildElement('extrafields', null);

			if (extrafieldsNode != null) {
				for (Dom.XmlNode extrafieldNode : extrafieldsNode.getChildElements()) {
					if (extrafieldNode.getAttributeValue('name', null) == extraName) {
						Dom.XmlNode dataNode = extrafieldNode.getChildElement('data', null);

						if (dataNode != null) {
							for (Dom.XmlNode optionNode : dataNode.getChildElements()) {
								String parentAttr = optionNode.getAttributeValue('parent', null);
								String idAttr = optionNode.getAttributeValue('id', null);
								String nodeText = optionNode.getText();
								System.debug(parentAttr + '....parentAttr...idAttr..' + idAttr + '....nodeText...' + nodeText + '..isParent...' + parentValue);
								if (idAttr != null) {
									if ((parentAttr == null) && (parentValue == '')) {
										baseItemList.add(new SelectOption(idAttr, nodeText));
									} else if ((parentAttr != null) && (parentAttr == parentValue)) {
										baseItemList.add(new SelectOption(idAttr, nodeText));
									}
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	* @name go
	* @author Aliaksandr Satskou
	* @data 07/08/2013
	* @description Searching options by keyword.
	* @return void
	*/
	public void go() {
		if ((keyword == null) || (keyword == '')) {
			itemList = baseItemList;
		} else {
			itemList = new List<SelectOption>();

			for (SelectOption item : baseItemList) {
				if (checkLabel(item.getLabel())) {
					itemList.add(item);
				}
			}
		}
		System.debug('itemList>>>' + itemList);
	}

	/**
	* @name checkLabel
	* @author Aliaksandr Satskou
	* @data 07/08/2013
	* @description Checking, if label is corresponds to the condition.
	* @return Boolean
	*/
	private Boolean checkLabel(String label) {
		List<String> wordList = label.split(' ');
		Integer keyLength = keyword.length();

		for (String word : wordList) {
			if (word.length() >= keyLength) {
				if (word.substring(0, keyLength).toLowerCase() == keyword.toLowerCase()) {
					return true;
				}
			}
		}

		return false;
	}
}