public class IdibuJobBoardPostingStatusController {
	private String selectedJobId; // Selected Job record Id
	public List<BCST__Job_Broadcast__c> jobBroadCastList { get; private set; } //pending job broadcasts
	public Boolean hasFailedPosts { get; private set; }
	//public boolean processingError{get; private set;}   // tracks if their is any error in getting status

	/* Constructor */

	public IdibuJobBoardPostingStatusController(ApexPages.StandardController controller) {
		selectedJobId = ApexPages.currentPage().getParameters().get('id');
		jobBroadCastList = new List<BCST__Job_Broadcast__c>();
		hasFailedPosts = false;
		//processingError = false;
	}

	/**
* @description Method for loading the status for all advert posts done for a job record. 
* The status details are shown as inline Vf on job details record. 
* This method is called from ShowBroadbeanJobStatus VF page.
**/

	public void loadCurrentJobPostingStatus() {
		hasFailedPosts = false; // setup to default value, vishwajeet,8/29/2017
		//processingError = false; // setup to default value, vishwajeet,8/29/2017
		try {
			List<BCST__Job_Broadcast__c> v_jobBroadCastList = [
					SELECT Id,Name,BCST__Job_Board_Name__c,
							BCST__Posting_Queue_ID__c,BCST__Job_Board_Error__c,
							BCST__Broadcasted_Status__c,CreatedDate
					FROM BCST__Job_Broadcast__c
					WHERE BCST__Broadbean_Posting__c = FALSE
							AND BCST__Broadcasted_Status__c IN ('Pending', 'Fail')
							AND BCST__Jobs__c = :selectedJobId
					ORDER BY CreatedDate DESC
			];

			if (v_jobBroadCastList != null && v_jobBroadCastList.size() > 0) {
				jobBroadCastList.clear(); //clear old list
				List<BCST__Job_Broadcast__c> v_UpdatejobBroadCastList = new List<BCST__Job_Broadcast__c>();

				//make call to idibu to get status
				for (BCST__Job_Broadcast__c v_Broadcast : v_jobBroadCastList) {
					//if(! 'fail'.equalsIgnoreCase(v_Broadcast.BCST__Broadcasted_Status__c)){
					if ('Pending'.equalsIgnoreCase(v_Broadcast.BCST__Broadcasted_Status__c)) {
						Dom.Document v_Doc = new Dom.Document();
						v_Doc.load(IdibuHelper.getJobBoardPostingStatus(v_Broadcast.BCST__Posting_Queue_ID__c));
						Dom.XmlNode v_responseNode = v_Doc.getRootElement().getChildElement('response', null);
						Dom.XmlNode v_PostNode = v_responseNode.getChildElement('post', null);

						//Check if their post node
						if (v_PostNode != null) {
							Dom.XmlNode v_Status = v_PostNode.getChildElement('status', null);
							if (v_Status != null) {
								if ('fail'.equalsIgnoreCase(v_Status.getText())) {
									v_Broadcast.BCST__Broadcasted_Status__c = 'Fail';
									v_Broadcast.BCST__Broadcasted__c = false;
									//Get Error Details and solution
									Dom.XmlNode v_errorsNode = v_PostNode.getChildElement('errordetails', null);
									for (Dom.XmlNode v_errorNode : v_errorsNode.getChildElements()) {
										String v_DescSt = '';
										String v_SolSt = '';

										Dom.XmlNode v_ReasonsNode = v_errorNode.getChildElement('reasons', null);
										if (v_ReasonsNode != null) {
											v_DescSt += 'Reason = ' + v_ReasonsNode.getText() + '\n';
										}

										Dom.XmlNode v_DescNode = v_errorNode.getChildElement('description', null);
										if (v_DescNode != null) {
											v_DescSt += 'Description = ' + v_DescNode.getText() + '\n';
										}

										Dom.XmlNode v_SolNode = v_errorNode.getChildElement('solution', null);
										if (v_SolNode != null) {
											v_SolSt += v_SolNode.getText() + '\n';
										}

										//update data in database,vk,11/16/2017
										if (String.isNotBlank(v_DescSt)) {
											v_Broadcast.BCST__Job_Board_Error__c = v_DescSt.left(32768);
										}

										if (String.isNotBlank(v_SolSt)) {
											v_Broadcast.BCST__Job_Board_Solution__c = v_SolSt.left(32768);
										}
									}
								} else if ('success'.equalsIgnoreCase(v_Status.getText())) {
									v_Broadcast.BCST__Broadcasted_Status__c = 'Active';
								}
							}
						}/*else{
                            //9/7/2017, Post node doesn't exists check status node, should happen only if status is active with one post
                            Dom.XmlNode v_Status = v_Doc.getRootElement().getChildElement('status', null);
                            if('success'.equalsIgnoreCase(v_Status.getText()))
                                v_Broadcast.BCST__Broadcasted_Status__c = 'Active';
                        }*/

						// Add for updating it in database
						v_UpdatejobBroadCastList.add(v_Broadcast);
					} else {
						jobBroadCastList.add(v_Broadcast);
					}
				}

				//update all data in objects
				if (v_UpdatejobBroadCastList.size() > 0) {
					update v_UpdatejobBroadCastList;
					for (BCST__Job_Broadcast__c v_Broadcast : v_UpdatejobBroadCastList) {
						if ('fail'.equalsIgnoreCase(v_Broadcast.BCST__Broadcasted_Status__c)
								|| 'pending'.equalsIgnoreCase(v_Broadcast.BCST__Broadcasted_Status__c)) {
							jobBroadCastList.add(v_Broadcast);
						}
					}
				}
				if (jobBroadCastList.size() > 0) {
					hasFailedPosts = true;
				} // Job Contains Failed Posts
			}

		} catch (Exception e) {
			//processingError = true;
			ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, String.format(system.Label.Idibu_Job_Board_Status_Loading_Error, new List<String> {e.getMessage()})));
		}
	}

}