global class PostingBatch implements Database.Batchable<SObject>, Database.AllowsCallouts {
	
	private final String criteria;
	private final String boardIdsString;
	
	global PostingBatch(String p_criteria, String p_boardIdsString) {
		criteria = p_criteria;
		boardIdsString = p_boardIdsString;
		
		System.debug(LoggingLevel.ERROR, ':::::::::::::::::::::::PostingBatch');
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator('SELECT Id FROM AVTRRT__Job__c WHERE ' + criteria);
	}
	
	global void execute(Database.BatchableContext BC, List<SObject> p_batch) {
		AVTRRT__Job__c v_job = (AVTRRT__Job__c)p_batch[0];
		
		System.debug(LoggingLevel.ERROR, ':::::::::::::::::::::::v_job.Id=' + v_job.Id);
		System.debug(LoggingLevel.ERROR, ':::::::::::::::::::::::boardIdsString=' + boardIdsString);
		if (!Test.isRunningTest()) {
			BroadcastingController.broadcast(v_job.Id, boardIdsString);
		}
	}
	
	global void finish(Database.BatchableContext BC) {
	}
}