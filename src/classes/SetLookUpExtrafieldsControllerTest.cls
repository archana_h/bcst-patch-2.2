/**
* @author Aliaksandr Satskou
* @date 07/08/2013
* @description Test class for SetLookUpExtrafieldsController class.
*/
@IsTest
private class SetLookUpExtrafieldsControllerTest {
	
	/**
	* @name initData
	* @author Aliaksandr Satskou
	* @data 07/08/2013
	* @description Initialize all needed test data.
	* @return void
	*/
	private static void initData() {
		ApexPages.currentPage().getParameters().put('board_id', '338');
		ApexPages.currentPage().getParameters().put('extra_name', 'seek_ind');
		ApexPages.currentPage().getParameters().put('target_id', 'id_1');
		ApexPages.currentPage().getParameters().put('val', 'Y');
		
		String jbSeekXml = '<?xml version="1.0" encoding="UTF-8" ?><idibu><boards><board id="338">' +
				'<extrafields><extrafield name="seek_ind"><data><option id="Y">Yes</option>' +
				'<option id="N">No</option></data></extrafield></extrafields></board></boards></idibu>';
		
		Document document = new Document(
				Name = '338-Seek(2013-07-08).xml',
				Body = Blob.valueOf(jbSeekXml),
				FolderId = IdibuHelper.getIdibuFolderId()
		);
		insert document;
	}
	
	/**
	* @name testSetLookUpExtrafieldsController
	* @author Aliaksandr Satskou
	* @data 07/08/2013
	* @description Testing SetLookUpExtrafieldsController class.
	* @return void
	*/
	static testMethod void testSetLookUpExtrafieldsController() {
		initData();
		
		SetLookUpExtrafieldsController controller = new SetLookUpExtrafieldsController();
		controller.createBaseItemList();
		controller.go();
		
		controller.keyword = 'y';
		controller.go();
	}
}