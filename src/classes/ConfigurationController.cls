public with sharing class ConfigurationController {
	
	/**
	* @name setupIdibuJobBoards
	* @author Vishwajeet
	* @date 28/06/2013
	* @description Getting and updating Job Board XML's from Idibu.
	* @return PageReference
	*/
	public PageReference setupIdibuJobBoards() {
		Id v_idibuFolderId = IdibuHelper.getIdibuFolderId();
		String v_idibuHash = IdibuHelper.getHashFromCustomSettings();
		String v_idibuJobBoardsXML = IdibuHelper.getJobBoardsFromIdibu(v_idibuHash);
		
		System.debug(LoggingLevel.ERROR, ':::::v_idibuJobBoardsXML>>>' + v_idibuJobBoardsXML);
		
		Map<String, String> v_idibuJobBoardIDNameMap = IdibuHelper.getJobBoardsIDNameMap(v_idibuJobBoardsXML);
		
		System.debug(LoggingLevel.ERROR, ':::::v_idibuJobBoardIDNameMap>>>' + v_idibuJobBoardIDNameMap);
		
		List<IdibuJobBoard> v_jobBoardList = new List<IdibuJobBoard>();
		/*
		*@author-vishwajeet
		*@date-09/03/2013
		*@case# : 00021059
		*@description : Added to resolve callout error's when more than 10 job boards are subscribed
		*/
		for (String v_jobBoardId : v_idibuJobBoardIDNameMap.keySet()) {
			IdibuJobBoard v_idibuJobBoard = new IdibuJobBoard();
			v_idibuJobBoard.board_id = v_jobBoardId;
			v_idibuJobBoard.name = v_idibuJobBoardIDNameMap.get(v_jobBoardId);
			v_jobBoardList.add(v_idibuJobBoard);
		}
		
		System.debug(LoggingLevel.ERROR, ':::::IdibuJobBoardSetupList' + v_jobBoardList);
		
		delete [
				SELECT Id
				FROM Document
				WHERE FolderId = :v_idibuFolderId
		];
		
		insert IdibuHelper.createJobBoardXMLDocument(
				'0-IdibuJobBoardsXML', v_idibuJobBoardsXML, v_idibuFolderId);
		
		Database.executeBatch(new IdibuJobBoardXmlBatch(v_idibuFolderId, v_idibuHash, v_jobBoardList), 1);
		
		/***************Commented by vishwajeet,date-09/03/2013,case#:00021059***************************/
		
		/*		
		String v_idibuJobBoardsXML = IdibuHelper.getJobBoardsFromIdibu(v_idibuHash);
		Map<String, String> v_idibuJobBoardIDNameMap = IdibuHelper.getJobBoardsIDNameMap(v_idibuJobBoardsXML);

		List<Document> v_documentList = new List<Document>();

		for (String v_jobBoardId : v_idibuJobBoardIDNameMap.keySet()) {
			String v_jobBoardXML = IdibuHelper.getJobBoardDataFromIdibu(v_jobBoardId, v_idibuHash);
			v_documentList.add(IdibuHelper.createJobBoardXMLDocument(
					v_jobBoardId + '-' + v_idibuJobBoardIDNameMap.get(v_jobBoardId),
					v_jobBoardXML,
					v_idibuFolderId));
		}

		v_documentList.add(IdibuHelper.createJobBoardXMLDocument('0-IdibuJobBoardsXML',
				v_idibuJobBoardsXML, v_idibuFolderId));

		delete [SELECT Id FROM Document WHERE FolderId = :v_idibuFolderId];

		insert v_documentList;
		*/
		/*************************************************************************/
		
		return null;
	}
	
	/**
	* @name openCreateIdibuAccountPage
	* @author Shashish kumar
	* @date 28/06/2013
	* @description Redirect to "CreateIdibuAccount" page.
	* @return PageReference
	*/
	public PageReference openCreateIdibuAccountPage() {
		PageReference pg = Page.CreateIdibuAccount;
		pg.setRedirect(true);
		return pg ;
	}
	
	public String msg { get; set; } // Used to show success message, after successfull creation of account or user.
	
	public ConfigurationController() {
		msg = System.currentPageReference().getParameters().get('msg');
		//msg = 'Success Message.';
	}
}