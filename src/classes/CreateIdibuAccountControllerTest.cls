/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

/**
* @author Shashish kumar
* @date 28/06/2013
* @description Test class is used to test CreateIdibuAccountController and IdibuAccountCreationHelper class.
*/
@IsTest
private class CreateIdibuAccountControllerTest {
	/**
	* @name testCreateIdibuAccount
	* @author Shashish kumar
	* @date 28/06/2013
	* @description Test method to Test createAccount method of CreateIdibuAccountController class(while
				   adding Account) with createAccountCreationXml, createAptrackServicesXml,
				   callHttpRequest method of IdibuAccountCreationHelper class.
	* @return Void.
	*/
	static testMethod void testCreateIdibuAccount() {
		/*
		*Commented by vishwajeet,Date-08/23/2013,Case#:00020694
		BCST__Job_Broadcasting_Settings__c jbsObj = new BCST__Job_Broadcasting_Settings__c();
		jbsObj.BCST__Idibu_User_Id__c = 'u007';
		insert jbsObj;
		*/

		CreateIdibuAccountController controllerObj = new CreateIdibuAccountController();
		controllerObj.firstName = 'fName';
		controllerObj.lastName = 'lName';
		controllerObj.email = 'abc@xyz.com';
		controllerObj.userName = 'uName';
		controllerObj.password = 'pass1234';
		controllerObj.phoneNum = '1234';
		controllerObj.companyName = 'cName';

		controllerObj.createAccount();

	}

	/**
	* @name testCreateIdibuAccountElseMsg
	* @author Shashish kumar
	* @date 28/06/2013
	* @description Test method to Test CreateIdibuAccountController and createAccount method
				   (With error message) of CreateIdibuAccountController class.
	* @return Void.
	*/
	static testMethod void testCreateIdibuAccountElseMsg() {
		BCST__Job_Broadcasting_Settings__c jbsObj = new BCST__Job_Broadcasting_Settings__c();
		jbsObj.BCST__Idibu_User_Id__c = 'u007';
		insert jbsObj;

		CreateIdibuAccountController controllerObj = new CreateIdibuAccountController();
		controllerObj.firstName = 'fName';

		controllerObj.createAccount();

	}

	/**
	* @name testCreateIdibuUserElseMsg
	* @author Shashish kumar
	* @date 28/06/2013
	* @description Test method to Test else part of createAccount method
				   (With error message) of CreateIdibuAccountController class while adding user.
	* @return Void.
	*/
	static testMethod void testCreateIdibuUserElseMsg() {
		BCST__Job_Broadcasting_Settings__c jbsObj = new BCST__Job_Broadcasting_Settings__c();
		jbsObj.BCST__Idibu_Hash__c = '0123';
		jbsObj.BCST__Idibu_User_Id__c = 'u007';
		insert jbsObj;

		CreateIdibuAccountController controllerObj = new CreateIdibuAccountController();
		controllerObj.firstName = 'fName';

		controllerObj.createAccount();

	}

	/**
	* @name testInvalidEmail
	* @author Shashish kumar
	* @date 28/06/2013
	* @description Test method to test isValid method of CreateIdibuAccountController class.
	* @return Void.
	*/
	static testMethod void testInvalidEmail() {
		BCST__Job_Broadcasting_Settings__c jbsObj = new BCST__Job_Broadcasting_Settings__c();
		jbsObj.BCST__Idibu_User_Id__c = 'u007';
		insert jbsObj;

		CreateIdibuAccountController controllerObj = new CreateIdibuAccountController();
		controllerObj.firstName = 'fName';
		controllerObj.lastName = 'lName';
		controllerObj.email = 'abc@xyz';
		controllerObj.userName = 'uName';
		controllerObj.password = 'pass1234';
		controllerObj.phoneNum = '1234';
		controllerObj.companyName = 'cName';

		controllerObj.createAccount();

	}

	/**
	* @name testCreateIdibuUser
	* @author Shashish kumar
	* @date 28/06/2013
	* @description Test method to Test createAccount method of CreateIdibuAccountController class(while
				   adding user) with createUserInfoXml,callHttpRequest method of IdibuAccountCreationHelper class.
	* @return Void.
	*/
	static testMethod void testCreateIdibuUser() {

		Profile pObj = [
				SELECT Id
				FROM Profile
				WHERE Name = 'Standard Platform User'
				LIMIT 1
		];
		User userObj = new User(FirstName = 'Dev',
				LastName = 'Smith',
				Email = 'smith@gmail.com',
				Username = 'smith@gmail.com.JBC',
				ProfileId = pObj.Id,
				Alias = 'dSmith',
				CommunityNickname = 'dSmith',
				TimeZoneSidKey = 'America/Los_Angeles',
				LocaleSidKey = 'en_IN',
				EmailEncodingKey = 'UTF-8',
				LanguageLocaleKey = 'en_US');
		insert userObj;

		BCST__Job_Broadcasting_Settings__c jbsObj = new BCST__Job_Broadcasting_Settings__c();
		jbsObj.BCST__Idibu_Hash__c = '0123';
		jbsObj.BCST__Idibu_User_Id__c = 'noUser';
		insert jbsObj;

		CreateIdibuAccountController controllerObj = new CreateIdibuAccountController();
		controllerObj.firstName = 'fName';
		controllerObj.lastName = 'lName';
		controllerObj.email = 'abc@xyz.com';
		controllerObj.userName = 'uName';
		controllerObj.password = 'pass1234';
		controllerObj.phoneNum = '1234';
		controllerObj.companyName = 'cName';
		controllerObj.address = 'add';
		controllerObj.addressLine1 = 'line1';
		controllerObj.addressLine2 = 'line2';
		controllerObj.addressLine3 = 'line3';
		controllerObj.country = 'india';
		controllerObj.postcode = '22222';
		controllerObj.www = 'www.google.com';

		controllerObj.jobBroadcastingSettingList[0].SetupOwnerId = userObj.Id;
		controllerObj.createAccount();
		ApexPages.currentPage().getParameters().put('msg', 'Success');
		SuccessMessageController messageController = new SuccessMessageController();

	}
}