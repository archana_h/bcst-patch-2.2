@IsTest
private class LightningHeaderControllerTest {
	static testMethod void lightningHeaderControllerMethod() {
		Test.startTest();
		
		Account account = new Account();
		account.Name = 'Test';
		insert account;
		
		BCST__Intermediate_Pages__c intermediate = new BCST__Intermediate_Pages__c();
		intermediate.BCST__Field_API_Name__c = 'name';
		intermediate.BCST__Page_Name__c = 'TestPage';
		intermediate.Name = 'test';
		insert intermediate;
		
		LightningHeaderController controller = new LightningHeaderController();
		controller.recordIds = account.Id;
		controller.sObjectName = 'account';
		controller.intermediatePageName = intermediate.BCST__Page_Name__c;
		controller.init();
		
		LightningHeaderController.recordData(String.valueOf(account.Id), 'account', 'Name');
		
		Test.stopTest();
	}
}