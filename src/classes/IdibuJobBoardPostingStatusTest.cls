@IsTest
private class IdibuJobBoardPostingStatusTest {
	
	private static AVTRRT__Job__c jobObj;
	@IsTest
	private static void test() {
		initData();
		
		ApexPages.StandardController controller = new ApexPages.StandardController(jobObj);
		ApexPages.currentPage().getParameters().put('id', jobObj.Id);
		
		IdibuJobBoardPostingStatusController idibuController = new IdibuJobBoardPostingStatusController(controller);
		idibuController.loadCurrentJobPostingStatus();
	
	}
	
	private static void initData() {
		AVTRRT__Config_Settings__c configSetting = new AVTRRT__Config_Settings__c(
				Name = 'Default',
				AVTRRT__Enable_Geosearch_for_Job__c = false,
				AVTRRT__Enable_Geosearch_for_Account__c = false,
				AVTRRT__Enable_Geosearch_for_Candidate__c = false,
				AVTRRT__Enable_Geosearch_for_Contact__c = false
		);
		insert configSetting;
		
		Account accObj = new Account(Name = 'TestAcc');
		insert accObj;
		
		jobObj = new AVTRRT__Job__c(
				AVTRRT__Job_Title__c = 'Java developer',
				AVTRRT__Job_Description__c = 'Java developer description',
				AVTRRT__Start_Date__c = Date.valueOf('2012-10-21'),
				AVTRRT__Job_Term__c = 'Permanent',
				BCST__Salary_Payment_Interval__c = 'Month',
				AVTRRT__Experience__c = '01-03 Years',
				AVTRRT__Salary_Range__c = '25000 - 35000',
				AVTRRT__Country_Locale__c = 'India',
				Idibu_Job_Type__c = '3',
				Idibu_Location__c = '37,1',
				AVTRRT__Account_Job__c = accObj.Id);
		insert jobObj;
		
		BCST__Job_Broadcast__c broadcast = new BCST__Job_Broadcast__c(
				BCST__Broadbean_Posting__c = false,
				BCST__Broadcasted_Status__c = 'Pending',
				BCST__Jobs__c = jobObj.Id
		);
		insert broadcast;
	
	}
}