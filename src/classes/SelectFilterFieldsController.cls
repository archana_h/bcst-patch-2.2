public class SelectFilterFieldsController {
	public Boolean topCheckbox { get; set; }

	public Boolean saveWindow { get; set; }
	public static String objName { get; set; }

	public List<SelectOption> jobFields {
		get {
			Map<String, Schema.SObjectField> jobFieldsMap = Schema.SObjectType.AVTRRT__Job__c.fields.getMap();
			List<SelectOption> schemaJobFields = new List<SelectOption>();

			for (SearchHelper.QueryField queryField : new SearchHelper().GetQueryFields(jobFieldsMap)) {
				schemaJobFields.add(new SelectOption(
						queryField.Name,
						queryField.Label));
			}

			return schemaJobFields;
		}
		set;
	}

	//map of object fields
	public Map<String, Schema.SObjectField> fMap;

	public SelectFilterFieldsController(ApexPages.StandardController controller) {
		objName = ApexPages.currentPage().getParameters().get('object');
		if (objName == 'AVTRRT__Job__c') {
			fMap = Schema.SObjectType.AVTRRT__Job__c.fields.getMap();
		}
		if (objName == 'Contact') {
			fMap = Schema.SObjectType.Contact.fields.getMap();
		}
	}

	List<rcrdWrapper> rcrdList;
	public List<rcrdWrapper> rcrds {
		get {
			if (rcrdList == null) {
				Map<String, String> filterFieldsSelected = new Map<String, String>();
				if (objName == 'Contact') {
					filterFieldsSelected = new SearchHelper().GetSelectedFilterFields();
				}
				if (objName == 'AVTRRT__Job__c') {
					filterFieldsSelected = new SearchHelper().GetSelectedFilterFieldsForJob();
				}

				rcrdList = new List<rcrdWrapper>();

				for (SearchHelper.QueryField r : new SearchHelper().GetQueryFields(fMap)) {
					Boolean isSelected = filterFieldsSelected.get(r.Name) != null;

					String priority;// = isSelected ? filterFieldsSelected.get(r.Name): '';

					rcrdList.add(new rcrdWrapper(
							r, isSelected, priority = isSelected ? filterFieldsSelected.get(r.Name) : ''));
				}
			}

			return rcrdList;
		}

		set;
	}

	private List<rcrdWrapper> selectedRows {
		get {
			List<rcrdWrapper> selectedRows = new List<rcrdWrapper>();
			
			for (rcrdWrapper row : rcrds) {
				if (row.selected == true) {
					selectedRows.add(row);
				}
			}

			return selectedRows;
		}
	}

	public class rcrdWrapper {
		public SearchHelper.QueryField rcrd { get; set; }
		public Boolean selected { get; set; }
		public String priority { get; set; }
		public String jobFieldSelected { get; set; }

		public rcrdWrapper(SearchHelper.QueryField r, Boolean s, String p) {
			rcrd = r;
			selected = s;
			priority = p;
			jobFieldSelected = '';
		}
	}

	public void selectFields() {
		AVTRRT__Config__c config;
		String pageObj = ApexPages.currentPage().getParameters().get('object');
		
		if (pageObj == 'Contact') {
			config = ConfigHelper.GetConfig(ConfigHelper.FilterFieldsUser);
		}
		if (pageObj == 'AVTRRT__Job__c') {
			config = ConfigHelper.GetConfig('broadcasting_filter_settings');
		}

		String value = '';
		for (rcrdWrapper selectedRow : selectedRows) {
			try {
				Integer.valueOf(selectedRow.priority);
			} catch (Exception ex) {
				AVTRRT.Logger.addErrorMessage('For the selected fields, the priority should be a number.');
				return;
			}

			value += selectedRow.rcrd.Name + '=' + selectedRow.priority + ';';
		}
		config.AVTRRT__Value__c = value;

		upsert config;

		saveWindow = true;
	}
}