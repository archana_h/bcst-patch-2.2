@IsTest
private class EmailToCandidateBroadcastingTest {
	
	private static Messaging.InboundEmail email;
	private static Messaging.InboundEmail.BinaryAttachment resumeFile;
	private static Messaging.InboundEmail.TextAttachment coverLetterFile;
	private static EmailToCandidateBroadcasting handler;
	
	private static List<AVTRRT__Job__c> jobList;
	
	private static void initData() {

		AVTRRT__Config_Settings__c customSetting = new AVTRRT__Config_Settings__c(
				Name = 'Default',
				AVTRRT__Trigger_Disabled_attachCoverletter__c = false,
				AVTRRT__Enable_Get_Email_Body__c = true
		);
		insert customSetting;

		ConfigHelper.SetConfig('Cover Letter', 'Cover;Letter;cvr;ltr;cov');
		
		Account accObj = new Account(Name = 'TestAcc');
		insert accObj;

		AVTRRT__Job__c jobObj = new AVTRRT__Job__c(
				AVTRRT__Job_Title__c = 'Java developer',
				AVTRRT__Job_Description__c = 'Java developer description',
				AVTRRT__Start_Date__c = Date.valueOf('2012-10-21'),
				AVTRRT__Job_Term__c = 'Permanent',
				BCST__Salary_Payment_Interval__c = 'Month',
				AVTRRT__Experience__c = '01-03 Years',
				AVTRRT__Salary_Range__c = '25000 - 35000',
				AVTRRT__Country_Locale__c = 'India',
				Idibu_Job_Type__c = '3',
				Idibu_Location__c = '37,1',
				AVTRRT__Account_Job__c = accObj.Id);
		insert jobObj;
		
		AVTRRT__ETCObject__c etcForCoverLetter = new AVTRRT__ETCObject__c(
				AVTRRT__Name__c = 'Cover Attachments');
		
		insert etcForCoverLetter;

		email = new Messaging.InboundEmail();
		
		String emailBody = generateEmailBody();
		email.htmlBody = emailBody;
		
		handler = new EmailToCandidateBroadcasting();
	}
	
	private static void addResumeToEmailAttachment() {
		resumeFile = new Messaging.InboundEmail.BinaryAttachment();
		resumeFile.body = Blob.valueOf('Attachment');
		resumeFile.fileName = 'file.txt';
		email.binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[] {resumeFile};
	}
	
	private static void addCoverLetterToEmailAttachment() {
		coverLetterFile = new Messaging.InboundEmail.TextAttachment();
		coverLetterFile.body = 'Attachment';
		coverLetterFile.fileName = 'Cover Letter' + '.txt';
		email.textAttachments = new Messaging.InboundEmail.TextAttachment[] {coverLetterFile};
	}
	
	private static void createSubjectWithJobNumbers() {
		jobList = [
				SELECT Name
				FROM AVTRRT__Job__c
		];
		email.subject = 'Title 00000000001. Job Number: ' +
				jobList[0].Name;
	}
	
	private static String generateEmailBody() {
		String emailBody = '{\r\n' +
				'\r\n' +
				'   "First Name"                }';

		return emailBody;
	}
	
	private static testMethod void testEmailToCandidateBrodcasting() {
		initData();

		addResumeToEmailAttachment();

		addCoverLetterToEmailAttachment();

		createSubjectWithJobNumbers();

		Test.startTest();

		handler.createETCObject(email);
		
		Test.stopTest();
	}
}