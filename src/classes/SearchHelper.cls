global with sharing class SearchHelper {
	public SearchHelper() {
	}
	
	public static Schema.SObjectField GetSObjectField(String fieldName, Map<String, Schema.SObjectField> fMap) {
		return fMap.get(fieldName != null && fieldName.startsWith('AVTRRT__')
				? fieldName/*.substring(8)*/ : fieldName);
	}
	
	public static Schema.DescribeFieldResult GetDescribeFieldResult(String fieldName, Map<String,
			Schema.SObjectField> fMap) {
		Schema.SObjectField sObjectField = GetSObjectField(fieldName, fMap);
		return sObjectField != null ? sObjectField.getDescribe() : null;
	}
	
	public static String GetFieldLabel(String fieldName, Map<String, Schema.SObjectField> fMap) {
		Schema.DescribeFieldResult describeFieldResult = GetDescribeFieldResult(fieldName, fMap);
		return describeFieldResult != null ? describeFieldResult.label : null;
	}
	
	// This and next methods is to retrive record name field value from lookup field.
	public static Object getFieldValue(String name, Object value, Map<String, Schema.SObjectField> fMap) {
		Schema.DescribeFieldResult describeFieldResult = GetDescribeFieldResult(name, fMap);
		return describeFieldResult != null && describeFieldResult.type == DisplayType.REFERENCE
				? getNameByID((String)value) : value;
	}
	public static String getNameByID(String id) {
		try {
			Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
			Map<String, String> keyPrefixMap = new Map<String, String> {};
			Set<String> keyPrefixSet = gd.keySet();
			for (String sObj : keyPrefixSet) {
				Schema.DescribeSObjectResult r = gd.get(sObj).getDescribe();
				String tempName = r.getName();
				String tempPrefix = r.getKeyPrefix();
				keyPrefixMap.put(tempPrefix, tempName);
			}
			
			String tPrefix = id.substring(0, 3);
			String objectName = keyPrefixMap.get(tPrefix);
			
			SObject retObject = Database.query(
					' SELECT id, name' +
							' FROM ' + objectName +
							' WHERE id=\'' + id + '\'');
			
			return (String)retObject.get('name');
		} catch (Exception ex) {
			return null;
		}
	}
	
	private List<QueryField> queryFields;
	public List<QueryField> GetQueryFields(Map<String, Schema.SObjectField> fMap) {
		if (queryFields == null) {
			queryFields = new List<QueryField>();
			
			Map<String, List<Schema.DescribeFieldResult>> forSorting =
					new Map<String, List<Schema.DescribeFieldResult>>();
			
			for (String key : fMap.keySet()) {
				Schema.DescribeFieldResult f = GetDescribeFieldResult(key, fMap);
				
				if (forSorting.get(f.getLabel()) == null) {
					forSorting.put(f.getLabel(), new List<Schema.DescribeFieldResult>());
				}
				
				forSorting.get(f.getLabel()).add(f);
			}
			List<String> keys = new List<String>(forSorting.keySet());
			keys.sort();
			
			for (String label : keys) {
				for (Schema.DescribeFieldResult f : forSorting.get(label)) {
					if (f.getName() != 'Name') {
						queryFields.add(new QueryField(label, f.getLocalName(), f));
					}
				}
			}
		}
		
		return queryFields;
	}
	
	public static String GetAllQueryFieldsString(Map<String, Schema.SObjectField> fMap) {
		String result = '';
		
		for (Schema.SObjectField field : fMap.values()) {
			result += ',' + field.getDescribe().name;
		}
		
		return result.substring(1);
	}
	
	public List<QueryField> GetSelectedQueryFields(Map<String, Schema.SObjectField> fMap) {
		Map<String, String> filterFields = GetSelectedFilterFields();
		
		System.debug(LoggingLevel.ERROR, '::::::filterFields=' + filterFields.keySet());
		
		List<QueryField> queryFieldsByFilter = GetQueryFieldsByFilter(filterFields, fMap);
		
		List<QueryField> queryFieldsByFilterResult = new List<QueryField>();
		for (QueryField queryField : queryFieldsByFilter) {
			if (queryField.Filterable) {
				queryFieldsByFilterResult.add(queryField);
			}
		}
		
		return queryFieldsByFilterResult;
	}
	
	public List<QueryField> GetDefaultQueryFields(Map<String, Schema.SObjectField> fMap) {
		Map<String, String> filterFields = GetDefaultFilterFields();
		return GetQueryFieldsByFilter(filterFields, fMap);
	}
	
	private List<QueryField> GetQueryFieldsByFilter(Map<String, String> filterFields, Map<String,
			Schema.SObjectField> fMap) {
		List<QueryField> queryFields = GetQueryFields(fMap);
		
		System.debug(LoggingLevel.ERROR, '::::queryFields' + queryFields);
		
		List<QueryField> queryFieldsResult = new List<QueryField>();
		
		for (SearchHelper.QueryField f : queryFields) {
			String priorityString = filterFields.get(f.Name); // RETURN ' '.
			
			System.debug(LoggingLevel.ERROR, '::::priorityString' + priorityString);
			
			if (priorityString != null) {
				DescribeFieldResult describeFieldResult = f.SObjectField.getDescribe();
				
				List<Schema.SObjectType> references = describeFieldResult.getReferenceTo();
				if (references.size() > 0) {
					Schema.DescribeSObjectResult parentObject = references[0].getDescribe();
					f.ParentObjectPrefix = parentObject.getKeyPrefix();
				}
				try {
					f.Priority = Integer.valueOf(priorityString);
				} catch (Exception e) {
				
				}
				queryFieldsResult.add(f);
			}
		}
		
		return queryFieldsResult;
	}
	// ---------------------------------------------------------------
	public Map<String, String> GetSelectedFilterFieldsForJob() {
		AVTRRT__Config__c config = ConfigHelper.GetConfig('broadcasting_filter_settings');
		if (config.AVTRRT__Value__c == null) {
			return new Map<String, String>();
		}
		return GetFilterFields(config);
	}
	
	public List<QueryField> GetSelectedQueryFieldsForJob(Map<String, Schema.SObjectField> fMap) {
		Map<String, String> filterFields = GetSelectedFilterFieldsForJob();
		
		System.debug(LoggingLevel.ERROR, '::::::filterFields=' + filterFields.keySet());
		
		List<QueryField> queryFieldsByFilter = GetQueryFieldsByFilter(filterFields, fMap);
		
		List<QueryField> queryFieldsByFilterResult = new List<QueryField>();
		for (QueryField queryField : queryFieldsByFilter) {
			if (queryField.Filterable) {
				queryFieldsByFilterResult.add(queryField);
			}
		}
		
		return queryFieldsByFilterResult;
	}
	// ---------------------------------------------------------------
	public Map<String, String> GetSelectedFilterFields() {
		System.debug(LoggingLevel.ERROR, '::::::::::config=');
		
		AVTRRT__Config__c config = ConfigHelper.GetConfig(ConfigHelper.FilterFieldsUser);
		
		System.debug(LoggingLevel.ERROR, ':::::config=' + config);
		
		if (config.AVTRRT__Value__c == null) {
			config = ConfigHelper.GetConfig(ConfigHelper.FilterFieldsDefault);
		}
		return GetFilterFields(config);
	}
	
	public Map<String, String> GetDefaultFilterFields() {
		return GetFilterFields(ConfigHelper.GetConfig(ConfigHelper.FilterFieldsDefault));
	}
	
	private Map<String, String> GetFilterFields(AVTRRT__Config__c config) {
		return ConfigHelper.getConfigNameValue(config.AVTRRT__Value__c);
	}
	
	public static List<String> GetKeywordsList(String keywords) {
		List<String> keywordsList = new List<String>();
		
		try {
			if (keywords != null && keywords.trim() != '') {
				keywords = keywords.toUpperCase();
				for (String part : keywords.split(' AND ')) {
					for (String part2 : part.trim().split(' OR ')) {
						for (String part3 : part2.trim().split('\\(')) {
							for (String part4 : part3.trim().split('\\)')) {
								for (String part6 : part4.trim().split('^NOT ')) {
									String part7 = part6.trim();
									if (part7 != '') {
										List<String> parts = new List<String>();
										if (part7.startsWith('"') && part7.endsWith('"')) {
											parts.add(part7.substring(1, part7.length() - 1));
										} else {
											parts = part7.split(' ');
										}
										
										for (String keyword : parts) {
											keyword = keyword.trim();
											if (keyword != '') {
												keywordsList.add(keyword);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception ex) {
		}
		
		return keywordsList;
	}
	
	// Highlight keywords in 'val'.
	public static String getTextWithKeywords(String val, List<String> keywordsList) {
		if (val != null) {
			// We should encode values for security reasons
			val = ESAPI.encoder().SFDC_HTMLENCODE(val);
			val = val.replaceAll('(?i)' + '\r\n', '<br />');
			
			for (String keyword : keywordsList) {
				String keywordNew = keyword.replaceAll('\\*', '\\\\w*');
				keyword = keyword.replace('*', '');
				keywordNew = keywordNew.replaceAll(Pattern.quote(keyword), '\\\\Q$0\\\\E');
				
				String replacePattern = '(?i)(\\W|^)(' + keywordNew + ')(\\W|$)';
				val = val.replaceAll(replacePattern, '$1<b style="background-color:yellow">$2</b>$3');
			}
		}
		
		return val;
	}
	
	public class QueryField {
		public String Label { get; set; }
		public String Name { get; set; }
		public String FieldType { get; set; }
		public String ParentObjectPrefix { get; set; }
		public Boolean Filterable { get; set; }
		public Integer Priority { get; set; }
		
		public Schema.SObjectField SObjectField { get; set; }
		
		public QueryField(String l, String n, Schema.DescribeFieldResult f) {
			SObjectField = f.getSobjectField();
			Label = l;
			Name = n;
			FieldType = String.valueOf(f.getType());
			Filterable = f.isFilterable();
		}
	}
	
	public static List<String> ListOfFields(Map<String, Schema.SObjectField> fMap) {
		List<String> fieldList = new List<String>();
		
		for (Schema.SObjectField field : fMap.values()) {
			fieldList.add(field.getDescribe().name);
		}
		
		return fieldList;
	}
}