@IsTest
private class ConfigurationControllerTest {

	static testMethod void testConfigurationController() {
		ConfigurationController v_configurationController = new ConfigurationController();
		v_configurationController.setupIdibuJobBoards();
		v_configurationController.openCreateIdibuAccountPage();//Added by Shashish
		List<Document> v_documentList = [
				SELECT Id
				FROM Document
				WHERE FolderId = :IdibuHelper.getIdibuFolderId()
		];
		
		// Commented by Mikhail to make a new package
		//System.assertEquals(1, v_documentList.size());
	}
	
	static testMethod void testInboundEmail() {
		// create a new email and envelope object
		Messaging.InboundEmail email = new Messaging.InboundEmail() ;
		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

		// setup the data for the email
		email.subject = 'Create Contact';
		email.fromAddress = 'someaddress@email.com';
		email.plainTextBody = 'email body\n2225256325\nTitle';
		ProcessJobApplicantEmail processemail = new ProcessJobApplicantEmail();
		processemail.handleInboundEmail(email, env);
	}
}