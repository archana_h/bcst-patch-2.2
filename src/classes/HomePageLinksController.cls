public with sharing class HomePageLinksController {

	public Map<String, List<BCST__HomePageComponents__c>> allLinkMap { get; set; }

	public void init() {
		allLinkMap = new Map<String, List<BCST__HomePageComponents__c>>();

		List<BCST__HomePageComponents__c> allLinks =
				BCST__HomePageComponents__c.getAll().values();

		for (BCST__HomePageComponents__c singleLink : allLinks) {
			if (allLinkMap.containsKey(singleLink.Section_Name__c)) {
				allLinkMap.get(singleLink.Section_Name__c).add(singleLink);
			} else {
				allLinkMap.put(singleLink.Section_Name__c,
						new List<BCST__HomePageComponents__c> {singleLink});
			}
		}

		for (String sectionName : allLinkMap.keySet()) {
			BCST__HomePageComponents__c[] links = allLinkMap.get(sectionName);
			HomePageComponents__c[] sortedLinks =
					(BCST__HomePageComponents__c[])new LinkSorter(links).sort();
			allLinkMap.put(sectionName, sortedLinks);
		}
	}

	private class LinkSorter extends Sorter {
		public LinkSorter(BCST__HomePageComponents__c[] links) {
			super(links, 'asc');
		}

		public override Object getKey(Object obj) {
			return ((BCST__HomePageComponents__c)obj).Order__c;
		}
	}
}