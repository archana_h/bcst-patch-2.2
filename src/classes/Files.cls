public without sharing class Files {
	private static Logger logger = new Logger('Files');
	
	/**
	 * Each attachment record should have these fields populated:
	 *  - Id
	 *  - ParentId
	 *  - OwnerId
	 *  - Name
	 *  - Description
	 *  - Body
	 *  - IsPrivate
	 *  - CreatedById
	 *  - CreatedDate
	 *  - LastModifiedById
	 *  - LastModifiedDate
	 */
	public static ContentVersion[] convert(Attachment[] attachments, Boolean deleteConverted) {
		return convert(attachments, deleteConverted, null);
	}
	
	public static ContentVersion[] convert(Attachment[] attachments,
			Boolean deleteConverted, Id contentDocumentId) {
		
		Map<Id, Id> alreadyConvertedAttachmentIds = getAlreadyConvertedAttachmentIds(attachments);
		
		Attachment[] attachmentsToConvert = new Attachment[] {};
		for (Attachment attachment : attachments) {
			if (alreadyConvertedAttachmentIds.containsKey(attachment.Id)) {
				continue;
			}
			
			SObjectType parentType = attachment.ParentId.getSobjectType();
			if (EmailMessage.SObjectType == parentType || EmailTemplate.SObjectType == parentType) {
				continue;
			}
			
			attachmentsToConvert.add(attachment);
		}
		
		ContentVersion[] versions = new ContentVersion[] {};
		if (contentDocumentId == null) {
			versions = create(attachmentsToConvert);
		} else {
			versions = create(attachmentsToConvert, contentDocumentId);
		}
		
		if (deleteConverted) {
			deleteConverted(versions, attachmentsToConvert);
		}
		
		return versions;
	}
	
	/**
	 * Given a list of attachments then returns those
	 * that have already been converted and their new file IDs.
	 */
	private static Map<Id, Id> getAlreadyConvertedAttachmentIds(List<Attachment> attachments) {
		Map<Id, Id> attachmentIdToVersionId = new Map<Id, Id>();
		
		Set<Id> attachmentIds = new Set<Id>();
		Set<Id> parentIds = new Set<Id>();
		
		for (Attachment att : attachments) {
			attachmentIds.add(att.Id);
			parentIds.add(att.ParentId);
		}
		
		Db.isReadable(ContentDocumentLink.SObjectType, '' +
				'ContentDocument.LatestPublishedVersionId, ' +
				'ContentDocument.LatestPublishedVersion.BCST__Attachment_ID__c, ' +
				'LinkedEntityId');
		ContentDocumentLink[] links = [
				SELECT ContentDocument.LatestPublishedVersionId,
						ContentDocument.LatestPublishedVersion.Attachment_ID__c
				FROM ContentDocumentLink
				WHERE ContentDocument.LatestPublishedVersion.Attachment_ID__c IN :attachmentIds
						AND LinkedEntityId IN :parentIds
		];
		
		for (ContentDocumentLink link : links) {
			if (link.ContentDocument == null) {
				continue;
			}
			
			ContentVersion version = link.ContentDocument.LatestPublishedVersion;
			if (version != null && attachmentIds.contains(version.Attachment_ID__c)) {
				attachmentIdToVersionId.put(version.Attachment_ID__c, version.Id);
			}
		}
		
		return attachmentIdToVersionId;
	}
	
	/**
	 * Insert a new file. Note: Org must have "Salesforce CRM Content" enabled. User is not required to have "Salesforce CRM Content User" license. The method sets contentDocumentId, contentVersion, contentDocumentLink in files parameter
	 *
	 * @param file File to insert.
	 */
	public static ContentVersion create(File file) {
		return create(file, null);
	}
	
	public static ContentVersion create(File file, Id contentDocumentId) {
		return create(new File[] {file}, contentDocumentId)[0];
	}
	
	public static ContentVersion[] create(File[] files) {
		return create(files, null);
	}
	
	/**
	 * Insert new files. Note: Org must have "Salesforce CRM Content" enabled. User is not required to have "Salesforce CRM Content User" license. The method sets contentDocumentId, contentVersion, contentDocumentLink in files parameter
	 *
	 * @param files Files to insert.
	 */
	public static ContentVersion[] create(File[] files, Id contentDocumentId) {
		Attachment[] attachments = new Attachment[] {};
		for (File file : files) {
			attachments.add(new Attachment(
					Name = file.name,
					Body = file.body,
					ParentId = file.parentId
			));
		}
		
		return create(attachments, contentDocumentId);
	}
	
	private static ContentVersion[] create(Attachment[] attachments) {
		return create(attachments, null);
	}
	
	private static ContentVersion[] create(Attachment[] attachments, Id contentDocumentId) {
		logger.log('create', 'contentDocumentId', contentDocumentId);
		
		ContentVersion[] versions = new ContentVersion[] {};
		for (Attachment attachment : attachments) {
			logger.log('create', 'attachment', attachment);
			
			if (attachment.Body.size() == 0) {
				attachment.Body = Blob.valueOf('No body');
			}
			
			// We set the owner of the new content file to be the
			// same as the attachment's owner because both fields
			// must have same value to insert the content file.
			// If they do not match then we get error:
			// "Documents in a user's private library must always be owned by that user."
			// The other reason to reference the old record's owner
			// is if the original creator is inactive and the admin
			// needs the new converted file to be owned by an active user.
			// The owner of records can be changed, the created by cannot.
			ContentVersion version = new ContentVersion(
					// data fields
					VersionData = attachment.Body,
					Title = attachment.Name,
					ContentDocumentId = contentDocumentId,
					PathOnClient = attachment.Name,
					FirstPublishLocationId = contentDocumentId == null ? attachment.ParentId : null,
					ContentLocation = 'S' // 'S' specify this document is in SF.
			);
			
			if (attachment.Id != null) {
				version.SharingPrivacy = (attachment.IsPrivate ? 'P' : 'N');
				version.Description = attachment.Description;
				
				// audit fields
				version.OwnerId = attachment.OwnerId; // system requirement, owner and creator must be the same
				version.CreatedById = attachment.OwnerId;
				version.CreatedDate = attachment.CreatedDate;
				version.LastModifiedById = attachment.LastModifiedById;
				version.LastModifiedDate = attachment.LastModifiedDate;
				
				// custom fields for history tracking and conversion purposes
				version.Attachment_ID__c = attachment.Id;
				version.Attachment_Parent_ID__c = attachment.ParentId;
				version.Attachment_Owner_ID__c = attachment.OwnerId;
			}
			
			/*
			// determine if communities are enabled and if so then we will need
			// to assign the network id field when inserting the content versions
			// otherwise error "INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY" occurs
			// if community user uploads an attachment and it tries to get converted
			// https://github.com/DouglasCAyers/sfdc-convert-attachments-to-chatter-files/issues/9
			MetadataUtilNew mdVersion = MetadataUtilNew.getInstance(ContentVersion.SObjectType);
			Boolean communitiesEnabled = (mdVersion.getSObjectField('NetworkId') != null);

			// if communities are enabled then assign network id
			if (communitiesEnabled) {
				newFileVersion.put('NetworkId', this.networkId);
			}*/
			
			logger.log('create', 'version', version);
			
			versions.add(version);
		}
		
		Db.doInsert(versions, new SObjectField[] {
				ContentVersion.VersionData,
				ContentVersion.Title,
				ContentVersion.PathOnClient,
				ContentVersion.FirstPublishLocationId,
				ContentVersion.ContentLocation,
				ContentVersion.ContentDocumentId,
				ContentVersion.SharingPrivacy,
				ContentVersion.Description,
				
				ContentVersion.OwnerId,
				ContentVersion.CreatedById,
				ContentVersion.CreatedDate,
				ContentVersion.LastModifiedById,
				ContentVersion.LastModifiedDate,
				
				ContentVersion.Attachment_ID__c,
				ContentVersion.Attachment_Parent_ID__c,
				ContentVersion.Attachment_Owner_ID__c
		});
		
		logger.log('create', 'versions', versions);
		
		return versions;
	}
	
	private static void deleteConverted(ContentVersion[] versions, Attachment[] attachments) {
		List<Attachment> attachmentsToDelete = new List<Attachment>();
		
		Map<Id, Attachment> attachmentMap = new Map<Id, Attachment>(attachments);
		for (ContentVersion version : versions) {
			attachmentsToDelete.add(attachmentMap.get(version.Attachment_ID__c));
		}
		
		if (attachmentsToDelete.size() > 0) {
			Db.doDelete(attachmentsToDelete);
		}
	}

	/**
	 * @param fileName File's name. Make sure extension is there.
	 * @param body File's content.
	 * @param parentId Id of an entity the file will be attached to. Can be Chatter users, groups, records (any that support Chatter feed tracking including custom objects), and Salesforce CRM Content libraries.
	*/
	public with sharing class File {
		public String name { get; set; }
		public Blob body { get; set; }
		public Id parentId { get; set; }
		
		public File() {
		}
		
		public File(String name, Blob body, Id parentId) {
			this.name = name;
			this.body = body;
			this.parentId = parentId;
		}
	}
}