@IsTest
public with sharing class BroadbeanTest {
	
	static testMethod void testBroadbean() {
		String recruiterId = UserInfo.getUserId();
		
		BCST__Job_Broadcasting_Settings__c customSetting = new BCST__Job_Broadcasting_Settings__c(
				BCST__API_Key_Broadbean__c = '2038195388',
				BCST__Broadbean_Currency_Code__c = 'USD',
				BCST__Broadbean_Password__c = 'manjuTARGET',
				BCST__Broadbean_Target_URL__c = 'https://api.adcourier.com/hybrid/hybrid.cgi',
				BCST__Broadbean_User_Name__c = 'Manjunath.P@hybridtest.TargetRecruit',
				BCST__Idibu_User_Id__c = '12121',
				SetupOwnerId = recruiterId
		);
		insert customSetting;
		
		AVTRRT__Config_Settings__c configSetting = new AVTRRT__Config_Settings__c(
				Name = 'Default',
				AVTRRT__Enable_Geosearch_for_Job__c = false,
				AVTRRT__Enable_Geosearch_for_Account__c = false,
				AVTRRT__Enable_Geosearch_for_Candidate__c = false,
				AVTRRT__Enable_Geosearch_for_Contact__c = false
		);
		insert configSetting;
		
		AVTRRT__Job__c job = new AVTRRT__Job__c();
		job.AVTRRT__Job_Title__c = 'Java developer UTest';
		job.AVTRRT__Job_Description__c = 'Java developer description';
		job.AVTRRT__Start_Date__c = Date.valueOf('2011-10-21');
		job.AVTRRT__Job_Term__c = 'Permanent';
		job.AVTRRT__Experience__c = '01-03 Years';
		job.AVTRRT__Salary_Range__c = '25000 - 35000';
		job.AVTRRT__Country_Locale__c = 'USA';
		job.AVTRRT__Job_City__c = 'Alabama';
		job.AVTRRT__Recruiter__c = recruiterId;
		insert job;
		
		System.currentPageReference().getParameters().put('id', job.Id);
		ApexPages.StandardController broadbean = new ApexPages.StandardController(job);
		BroadBeanJobPostingController broadbeanController = new BroadBeanJobPostingController(broadbean);
		broadbeanController.init();
		broadbeanController.exportJobPosting();
		broadbeanController.loadCurrentBroadbeanJobStatus();
		String[] chanls = new String[] {'Broadbean Test Board'};
		broadbeanController.setChannels(chanls);
		broadbeanController.deleteFromBroadBean();
		broadbeanController.loadCurrentBroadbeanJobStatus();
		//add by VK, for code coverage
		Boolean test = broadbeanController.isBroadbeanJobPostingStatusAvailable;
		broadbeanController.getBoards();
	
	}
	
	static testMethod void testGroupByHelper() {
		AVTRRT__Config_Settings__c configSetting = new AVTRRT__Config_Settings__c(
				Name = 'Default',
				AVTRRT__Trigger_Disabled_Contact__c = true
		);
		insert configSetting;
		
		Account account = new Account(Name = 'Test Account');
		insert account;
		
		List<Contact> contacts = new List<Contact>{
				new Contact(
						LastName = 'Test',
						AccountId = account.Id
				)
		};
		insert contacts;
		
		GroupByHelper.groupByFieldKeyId(contacts, 'AccountId');
	}
}