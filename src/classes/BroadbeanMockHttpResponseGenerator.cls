/**
    Creating Http response mockup.
 */
@IsTest
global class BroadbeanMockHttpResponseGenerator implements HttpCalloutMock {

	public static Integer httpResponseStatus = 200;
	public static String httpResponseBody = '';
	public static String EXPORT_URL = 'https://www.adcourier.com/broadcast-step-1.cgi?stored_id=MjAxNDA2MjctMTQw';

	// Implement this interface method
	global HttpResponse respond(HttpRequest req) {

		// Create a fake response
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'text/xml');

		res.setBody(httpResponseBody);

		res.setStatusCode(httpResponseStatus);

		res.setStatus('Making a fake callout success');
		return res;
	}

	/*
		Mock response for Export call
	*/
	public static String getExportMockResponse() {
		String exportResponseXML = '<?xml version="1.0" encoding="utf-8"?>' +
				' <AdCourierAPIResponse><TimeNow>2014-06-27T12:50:17Z</TimeNow>' +
				' <ResponseId>crmtest-06-27-1403873417-api-01.gs-18743-561506</ResponseId>' +
				' <ExportResponse>' +
				' <StoreJobURL>' + EXPORT_URL + '</StoreJobURL>' +
				' </ExportResponse></AdCourierAPIResponse>';
		return exportResponseXML;
	}

	/*
		Mock response for Check Status call
	*/
	public static String getCheckStatusMockResponse() {
		String checkStatusXML = '<?xml version="1.0" encoding="utf-8"?>' +
				' <AdCourierAPIResponse><TimeNow>2014-06-27T12:50:17Z</TimeNow>' +
				' <ResponseId>crmtest-06-27-1403873417-api-01.gs-18743-561506</ResponseId>' +
				' <StatusCheckResponse>' +
				' <Advert>' +
				'     <Id>98519</Id>' +
				'     <CreateTime>2014-06-26T15:20:14Z</CreateTime>' +
				'     <Consultant>Manjunath</Consultant>' +
				'     <Team>hybridtest</Team>' +
				'    <Office>Appirio</Office>' +
				'     <UserName>Manjunath.P@hybridtest.TargetRecruit</UserName>' +
				'     <JobTitle>Java Developer</JobTitle>' +
				'     <JobReference>1234</JobReference>' +
				'     <JobType>Permanent</JobType>' +
				' </Advert>' +
				' <ChannelList>' +
				'    <Channel>' +
				'         <ChannelId>broadbean</ChannelId> ' +
				'        <ChannelName>Broadbean Test Board</ChannelName> ' +
				'       <ChannelStatus PostedTime="2014-06-26"  RemovalTime="2014-07-03" ' +
				'     AdvertURL="http://broadbean.broadbeantech.com/index.php?ID=610162" Responses="3" ReturnCode="0" >Delivered</ChannelStatus> ' +
				'     </Channel>' +
				' </ChannelList> ' +
				' </StatusCheckResponse> </AdCourierAPIResponse>';

		return checkStatusXML;
	}

	/*
		Mock response for Get List Channel call
	*/
	public static String getListChannelMockResponse() {

		String listChannelXML = '<?xml version="1.0" encoding="utf-8"?>' +
				' <AdCourierAPIResponse><TimeNow>2014-06-27T12:50:17Z</TimeNow>' +
				' <ResponseId>crmtest-06-27-1403873417-api-01.gs-18743-561506</ResponseId>' +
				' <ListChannelsResponse>' +
				'     <ChannelList>' +
				'         <Channel><ChannelId>broadbean</ChannelId>' +
				'             <ChannelName>Broadbean Test Board</ChannelName>' +
				'             <BoardId>999</BoardId>' +
				'             <FreeToPost>true</FreeToPost>' +
				'        </Channel>' +
				'         <Channel>' +
				'             <ChannelId>jobscabi</ChannelId>' +
				'             <ChannelName>Jobs Cabinet (Test Board)</ChannelName>' +
				'             <BoardId>2336</BoardId>' +
				'            <FreeToPost>true</FreeToPost>' +
				'          </Channel> </ChannelList>' +
				'  </ListChannelsResponse></AdCourierAPIResponse>';

		return listChannelXML;
	}

	/*
		Mock response for Delete advert call
	*/
	public static String getDeleteAdvertMockResponse() {

		String deleteResponseXML = '<?xml version="1.0" encoding="utf-8"?>' +
				' <AdCourierAPIResponse><TimeNow>2014-06-27T12:50:17Z</TimeNow>' +
				' <ResponseId>crmtest-06-27-1403873417-api-01.gs-18743-561506</ResponseId>' +
				' <DeleteResponse>' +
				' <Advert>' +
				'     <Id>14710</Id>' +
				'     <CreateTime>2009-01-23T16:45:11Z</CreateTime>' +
				'     <Consultant>user</Consultant>' +
				'     <Team>team</Team>' +
				'     <Office>office</Office>' +
				'     <UserName>Manjunath.P@hybridtest.TargetRecruit</UserName>' +
				'     <JobTitle>Java Deeloper</JobTitle>' +
				'     <JobReference>1234</JobReference>' +
				'     <JobType>Permanent</JobType>' +
				' </Advert>' +
				'<ChannelList> <Channel><ChannelId>broadbean</ChannelId> <ChannelName>Broadbean Test Board</ChannelName>' +
				'<ChannelStatus PostedTime="2016-05-04T17:11:17Z" RemovalTime="2016-05-04T17:13:24Z" ReturnCode="5" ReturnCodeClass="Success">Deleted</ChannelStatus>' +
				'</Channel> </ChannelList>' +
				'</DeleteResponse>' +
				'</AdCourierAPIResponse>';

		return deleteResponseXML;
	}

}