public with sharing class Support {
	
	public static void sendSupportEmail(String p_description, String p_body, Boolean p_isHtmlBody) {
		System.debug(LoggingLevel.ERROR, ':::::::sendSupportEmail');
		
		Boolean v_isSendDebugLogs = BCST__Job_Broadcasting_Settings__c.getInstance().BCST__Send_Debug_Logs__c;
		
		if (v_isSendDebugLogs == true) {
			String v_supportEmailAddress =
					BCST__Job_Broadcasting_Settings__c.getInstance().BCST__Support_Email_Address__c;
			
			if ((v_supportEmailAddress != null) && (v_supportEmailAddress != '')) {
				Messaging.SingleEmailMessage v_eMailMessage = new Messaging.SingleEmailMessage();
				
				v_eMailMessage.setToAddresses(new List<String> {v_supportEmailAddress});
				v_eMailMessage.setSenderDisplayName(UserInfo.getUserName());
				v_eMailMessage.setSubject('Job Broadcasting Log');
				v_eMailMessage.setUseSignature(false);
				
				if (p_isHtmlBody == true) {
					String v_htmlBody = '<div><div style="border: 1px solid #fed483; ' +
							'background-color: #fef4e0;"><p style="font-family: Verdana; font-size: ' +
							'11px; padding-left: 20px;">� Job Broadcasting Log, type of content: html' +
							'</p></div><div style="border: 1px solid #fed483; background-color: ' +
							'#f5f5f5; margin-top: 2px;"><p style="border: 1px solid #b3d1ff; ' +
							'background-color: #e5efff; margin-left: 10px; margin-right: 10px; ' +
							'font-family: Verdana; font-size: 11px; padding: 5px 5px 5px 5px; ' +
							'color: #563636;">' + p_description + '</p><p style="font-family: ' +
							'\'Courier New\'; font-size: 12px; padding-left: 20px;">' + p_body +
							'</p></div></div>';
					v_eMailMessage.setHtmlBody(v_htmlBody);
				} else {
					String v_plainTextBody = '� Job Broadcasting Log, type of content: plain text ' +
							'\n\nDescription: ' + p_description + '\n\n\n' + p_body;
					v_eMailMessage.setPlainTextBody(v_plainTextBody);
				}
				if (!Test.isRunningTest()) {
					Messaging.sendEmail(new Messaging.SingleEmailMessage[] {v_eMailMessage});
				}
			}
		}
	}
}