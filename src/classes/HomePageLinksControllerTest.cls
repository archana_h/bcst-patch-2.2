@IsTest
private class HomePageLinksControllerTest {
	static testMethod void homePageLinksControllerMethod() {
		Test.startTest();

		insert new BCST__HomePageComponents__c(Name = 'test');

		HomePageLinksController instance = new HomePageLinksController();
		instance.init();

		Test.stopTest();
	}
}