global with sharing class MissingJobPostingBatch implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {
	private final String criteria;
	private final String boardIdsString;
	private Set<String> failedJobSet = new Set<String>();
	
	global MissingJobPostingBatch(String p_criteria, String p_boardIdsString) {
		criteria = p_criteria;
		boardIdsString = p_boardIdsString;
		
		System.debug(LoggingLevel.ERROR, ':::::::::::::::::::::::PostingBatch');
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator('SELECT Id FROM AVTRRT__Job__c WHERE ' + criteria);
	}
	
	global void execute(Database.BatchableContext BC, List<SObject> p_batch) {
		
		AVTRRT__Config__c config = [
				SELECT a.AVTRRT__Value__c
				FROM AVTRRT__Config__c a
				WHERE a.AVTRRT__Name__c = 'Failed Jobs'
		];
		
		List<AVTRRT__Job__c> matchingJobs = [
				SELECT Id
				FROM AVTRRT__Job__c
				WHERE Id IN :p_batch
		];
		
		System.debug('matchingJobs>>>' + matchingJobs);
		
		Set<AVTRRT__Job__c> matchingJobSet = new Set<AVTRRT__Job__c>();
		matchingJobSet.addAll(matchingJobs);
		
		System.debug('matchingJobSet>>>' + matchingJobSet);
		
		List<BCST__Job_Broadcast__c> broadcasted = [
				SELECT Id,BCST__Jobs__c
				FROM BCST__Job_Broadcast__c
				WHERE CreatedDate = TODAY
						AND BCST__Jobs__c IN :p_batch
						AND BCST__Job_Board_Id__c = :boardIdsString
						AND BCST__Broadcasted__c = TRUE
		];
		
		System.debug('broadcasted>>>' + broadcasted);
		
		Set<Id> broadcastedJobSet = new Set<Id>();
		
		for (BCST__Job_Broadcast__c broadcastObj : broadcasted) {
			broadcastedJobSet.add(broadcastObj.BCST__Jobs__c);
		}
		
		System.debug('broadcastedJob>>>' + broadcastedJobSet);
		
		List<AVTRRT__Job__c> broadcastedJobList = new List<AVTRRT__Job__c>();
		broadcastedJobList = [
				SELECT Id
				FROM AVTRRT__Job__c
				WHERE Id IN :broadcastedJobSet
		];
		
		matchingJobSet.removeAll(broadcastedJobList);
		
		System.debug('matchingJobSet>>>' + matchingJobSet);
		
		List<AVTRRT__Job__c> pendingjobs = new List<AVTRRT__Job__c>();
		
		pendingjobs.addAll(matchingJobSet);
		
		System.debug('pendingjobs>>>' + pendingjobs);
		
		if (pendingjobs.size() > 0 && !Test.isRunningTest()) {
			BroadcastingController.broadcast(pendingjobs[0].Id, boardIdsString);
			String jobNumber = [
					SELECT Id,Name
					FROM AVTRRT__Job__c
					WHERE Id = :pendingjobs[0].Id
			].Name;
			String failedjobboardId = jobNumber + '-' + boardIdsString;
			failedJobSet.add(failedjobboardId + '-' + pendingjobs[0].Id);
			if (config.AVTRRT__Value__c != null) {
				config.AVTRRT__Value__c = config.AVTRRT__Value__c + ';' + failedjobboardId;
			} else {
				config.AVTRRT__Value__c = failedjobboardId;
			}
			update config;
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		
		String body = 'Failed Job is ' ;
		
		if (failedJobSet.size() > 0) {
			for (String jobObj : failedJobSet) {
				List<String> jobList = jobObj.split('-');
				String jobnumber = jobList[0];
				String BoardId = jobList[1];
				String jobId = jobList[2];
				body += ' Job number ' + jobnumber;
				body += ' Job Board Id ' + BoardId;
				body += 'Link to Job is ' + Url.getSalesforceBaseUrl().toExternalForm() + '/' + jobId + '\n';
			}
		}
		Boolean enableFailedJob =
				BCST__Job_Broadcasting_Settings__c.getInstance().BCST__Enable_Failed_Job_Email_Sent__c;
		if (enableFailedJob) {
			String emailAddress = BCST__Job_Broadcasting_Settings__c.getInstance().BCST__Failed_Jobs_To_Sent_Email__c;
			// Send an email to the Apex job's submitter notifying of job completion.
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			// String[] toAddresses = new String[] {a.CreatedBy.Email};
			String[] toAddresses = new String[] {emailAddress};
			//String[] toAddresses = new String[] {'pandeiswari.s@targetrecruit.net'};
			mail.setToAddresses(toAddresses);
			mail.setSubject('Failed Jobs ');
			mail.setPlainTextBody(body);
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
		}
	}
}