/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@IsTest
private class TestConfigHelper {
	private static AVTRRT__Config__c testConfig { get; set; }
	
	private static void initConfig() {
		testConfig = new AVTRRT__Config__c();
		testConfig.AVTRRT__Name__c = 'config_Name';
		testConfig.AVTRRT__Value__c = 'config_Value';
		insert testConfig;
	}
	
	static testMethod void testGetConfig() {
		initConfig();
		AVTRRT__Config__c thisConfig = ConfigHelper.GetConfig('config_Name');
		System.assertEquals(thisConfig.AVTRRT__Value__c, 'config_Value');
		thisConfig = ConfigHelper.GetConfig('config_Other');
	}
	
	static testMethod void testSetConfig() {
		initConfig();
		AVTRRT__Config__c thisConfig = ConfigHelper.SetConfig('config_Name', 'config_NewValue');
		System.assertEquals(thisConfig.AVTRRT__Value__c, 'config_NewValue');
	}
	
	static testMethod void testGetConfigDataSet() {
		AVTRRT__Config_Settings__c thisConfigSettings = ConfigHelper.getConfigDataSet();
		//System.assertEquals(thisConfigSettings.AVTRRT__Last_Candidate_Id__c, 690);
	}
	
	static testMethod void testGetConfigSetting() {
		Object thisObject = ConfigHelper.getConfigSetting('Name');
	}
	
	static testMethod void testGetConfigSettingNameValue() {
		Map<String, String> thisMap = ConfigHelper.getConfigSettingNameValue('Name');
		Map<String, String> actualMap = new Map<String, String>();
		actualMap.put('Default', '');
		thisMap = ConfigHelper.getConfigSettingNameValue('AVTRRT__Candidates_Batch_Size__c');
		//System.assertEquals(thisMap, null);
	}
	
	static testMethod void testSetConfigSetting() {
		ConfigHelper.setConfigSetting('AVTRRT__No_Interview_Message__c',
				'Candidate does not have any interviews');
	}
}