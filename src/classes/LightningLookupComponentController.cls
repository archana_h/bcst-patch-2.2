global with sharing class LightningLookupComponentController {
	
	/*
	* Author : Hari S
	* Description : Returns the matched records
	* Parameters:
	* objName : Accepts querying object name
	* searchField : Search field value
	* pattern : Accepts User entered input
*/
	@RemoteAction
	global static List<SObject> getLookupRecords(
			String objName, String searchField, String pattern, Boolean defaultFilter) {
		
		try {
			String query =
					' SELECT ' + searchField +
							' FROM ' + objName +
							' WHERE ' + searchField +
							' LIKE \'%' + pattern + '%\'';
			
			query += ' LIMIT 50';
			
			return Database.query(query);
		} catch (Exception e) {
			return null;
		}
	}
}