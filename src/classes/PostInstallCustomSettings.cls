/**
* @author Pandeiswari
* @date 05/25/2014
* @description Package post install script to load country code custom settings.
*/

global class PostInstallCustomSettings implements InstallHandler {

	Map<String, String> countryCodeMap = new Map<String, String>();

	/**
	* @author Pandeiswari
	* @date 05/25/2014
	* @param context Install Context
	* @description Package post install script.
	* @return void
	*/
	global void onInstall(InstallContext context) {

		List<BCST__JobBroadcastingCountryCodeSettings__c> v_JobBroadcastingCountryCodeList = new List<BCST__JobBroadcastingCountryCodeSettings__c> {

				createCountrySetting('Angola', 'Angola,AGO', 'AN'),
				createCountrySetting('Argentina', 'Argentina,ARG,ARGENTINA', 'AR'),
				createCountrySetting('Australia', 'Australia,AUS,AUSTRALIA', 'AU'),
				createCountrySetting('Austria', 'Austria,AUT,AUSTRIA', 'AT'),
				createCountrySetting('Barbados', 'Barbados', 'BB'),
				createCountrySetting('Belgium', 'Belgium,BE,BELGIUM', 'BE'),
				createCountrySetting('Brazil', 'Brazil,BR,BRAZIL', 'BR'),
				createCountrySetting('Bulgaria', 'Bulgaria,BG,BULGARIA', 'BG'),
				createCountrySetting('Canada', 'Canada,CA,CANADA', 'CA'),
				createCountrySetting('Channel Islands', 'Channel Islands', 'GG'),
				createCountrySetting('China', 'China,CHINA', 'CN'),
				createCountrySetting('Croatia', 'Croatia', 'HR'),
				createCountrySetting('Denmark', 'Denmark,DENMARK', 'DM'),
				createCountrySetting('Egypt', 'Egypt,EGYPT', 'EG'),
				createCountrySetting('Faroe Islands', 'Faroe Islands', 'FO'),
				createCountrySetting('France', 'France,FR,FRANCE', 'FR'),
				createCountrySetting('Germany', 'Germany,GERMANY', 'DE'),
				createCountrySetting('Guadeloupe', 'Guadeloupe', 'GP'),
				createCountrySetting('Guam', 'Guam,GUAM', 'GU'),
				createCountrySetting('Hong Kong', 'Hong Kong,HONG KONG', 'HG'),
				createCountrySetting('India', 'India,INDIA', 'IN'),
				createCountrySetting('Indonesia', 'Indonesia,INDONESIA', 'ID'),
				createCountrySetting('Ireland', 'Ireland,IRELAND', 'IE'),
				createCountrySetting('Isle of Man', 'Isle of Man', 'IM'),
				createCountrySetting('Italy', 'Italy,ITALY,IT', 'IT'),
				createCountrySetting('Japan', 'Japan,JAPAN', 'JP'),
				createCountrySetting('Jersey', 'Jersey', 'JE'),
				createCountrySetting('Liechtenstein', 'Liechtenstein', 'LI'),
				createCountrySetting('Luxembourg', 'Luxembourg', 'LU'),
				createCountrySetting('Macedonia', 'Macedonia', 'MK'),
				createCountrySetting('Malaysia', 'Malaysia,MALAYSIA', 'MY'),
				createCountrySetting('Malta', 'Malta', 'MA'),
				createCountrySetting('Marshall Islands', 'Marshall Islands', 'MH'),
				createCountrySetting('Martinique', 'Martinique', 'MQ'),
				createCountrySetting('Mayotte', 'Mayotte', 'YT'),
				createCountrySetting('Mexico', 'Mexico,MEXICO', 'MX'),
				createCountrySetting('Moldova', 'Moldova', 'MD'),
				createCountrySetting('Monaco', 'Monaco', 'MC'),
				createCountrySetting('Netherlands', 'Netherlands', 'NL'),
				createCountrySetting('New Zealand', 'New Zealand', 'NZ'),
				createCountrySetting('Northern Mariana Islands', 'Northern Mariana Islands', 'MP'),
				createCountrySetting('Norway', 'Norway,NORWAY', 'NO'),
				createCountrySetting('Pakistan', 'Pakistan,PAKISTAN,PK', 'PK'),
				createCountrySetting('Portugal', 'Portugal,PORTUGAL,PT', 'PT'),
				createCountrySetting('Puerto Rico', 'Puerto Rico', 'PR'),
				createCountrySetting('Rest of the wold (Other countries)', 'Rest of the wold (Other countries),ZZ', 'ZZ'),
				createCountrySetting('San Marino', 'San Marino', 'SM'),
				createCountrySetting('Singapore', 'Singapore,SINGAPORE,SI', 'SI'),
				createCountrySetting('South Africa', 'South Africa', 'ZA'),
				createCountrySetting('Spain', 'Spain,SPAIN', 'ES'),
				createCountrySetting('Sri Lanka', 'Sri Lanka,SRILANKA', 'LK'),
				createCountrySetting('St. Pierre and Miquelon', 'St. Pierre and Miquelon', 'PM'),
				createCountrySetting('Switzerland', 'Switzerland', 'CH'),
				createCountrySetting('Thailand', 'Thailand', 'TH'),
				createCountrySetting('Turkey', 'Turkey', 'TR'),
				createCountrySetting('United Kingdom', 'United Kingdom,UK,UNITED KINGDOM', 'GB'),
				createCountrySetting('United States', ',UNITED STATES,United States, United States of America,USA,US', 'US'),
				createCountrySetting('Vatican City', 'Vatican City', 'VA'),
				createCountrySetting('Virgin Islands (U.S.)', 'Virgin Islands (U.S.)', 'VI')
		};

		//condition check whether the package is being installed or upgraded
		if (!Test.isRunningTest()) {
			System.debug('Previous Version is ' + context.previousVersion());
			if (context.previousVersion() == null) {
				insert v_JobBroadcastingCountryCodeList;
			}

			else {
				List<BCST__JobBroadcastingCountryCodeSettings__c> existingCountryCodeList = [
						SELECT BCST__Country_Code__c, BCST__Country_Name__c
						FROM BCST__JobBroadcastingCountryCodeSettings__c
				];
				delete existingCountryCodeList;

				insert v_JobBroadcastingCountryCodeList;

				/*   List<BCST__JobBroadcastingCountryCodeSettings__c> v_JobBroadcastingCustomCountryCodeList = [
						SELECT BCST__Country_Code__c, BCST__Country_Name__c
					  FROM BCST__JobBroadcastingCountryCodeSettings__c];

					  System.debug('First List Size -----> '+v_JobBroadcastingCustomCountryCodeList.size());

				List<BCST__JobBroadcastingCountryCodeSettings__c> updatedCountryList = new List<BCST__JobBroadcastingCountryCodeSettings__c>();
				Set<String> countryList = new Set<String>();

				for (BCST__JobBroadcastingCountryCodeSettings__c countryToBeAdded : v_JobBroadcastingCustomCountryCodeList) {
					String country = countryToBeAdded.BCST__Country_Name__c;
					if (!countryList.contains(country)) {
						countryList.add(country);
					}
					else{
					countryList.remove(country);
					}
				}

				for (BCST__JobBroadcastingCountryCodeSettings__c countryToBeAdded : v_JobBroadcastingCountryCodeList) {
					String country = countryToBeAdded.BCST__Country_Name__c;
					if (!countryList.contains(country)) {
						countryList.add(country);
					}
					else{
					countryList.remove(country);
					}
				}

				for(BCST__JobBroadcastingCountryCodeSettings__c customSettingCountry : v_JobBroadcastingCustomCountryCodeList){
				if(countryList.contains(customSettingCountry.BCST__Country_Name__c)){
					updatedCountryList.add(customSettingCountry);
					}
				}

				for(BCST__JobBroadcastingCountryCodeSettings__c newCountryToAdd : v_JobBroadcastingCountryCodeList){
				if(countryList.contains(newCountryToAdd.BCST__Country_Name__c)){
					updatedCountryList.add(newCountryToAdd);
					}
				}

				insert updatedCountryList;  // insert the updated List of Countries
						*/
			}

		}

	}

	private static BCST__JobBroadcastingCountryCodeSettings__c createCountrySetting(
			String name, String countryName, String countryCode) {

		return new BCST__JobBroadcastingCountryCodeSettings__c(
				Name = name,
				BCST__Country_Name__c = countryName,
				BCST__Country_Code__c = countryCode
		);
	}
}