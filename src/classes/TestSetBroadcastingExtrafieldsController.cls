@IsTest(SeeAllData=true)
private class TestSetBroadcastingExtrafieldsController {

	static testMethod void testInit() {
		createCustomSettings();

		Profile pObj = [
				SELECT Id
				FROM Profile
				WHERE Name = 'Standard Platform User'
				LIMIT 1
		];
		User userObj = new User(FirstName = 'Dev',
				LastName = 'Smith',
				Email = 'smith@gmail.com',
				Username = 'smith@gmail.com.JBC',
				ProfileId = pObj.Id,
				Alias = 'dSmith',
				CommunityNickname = 'dSmith',
				TimeZoneSidKey = 'America/Los_Angeles',
				LocaleSidKey = 'en_IN',
				EmailEncodingKey = 'UTF-8',
				LanguageLocaleKey = 'en_US');
		insert userObj;

		Account accObj = new Account(Name = 'TestAcc');
		insert accObj;

		AVTRRT__Job__c jobObj = new AVTRRT__Job__c(
				AVTRRT__Job_Title__c = 'Java developer',
				AVTRRT__Job_Description__c = 'Java developer description',
				AVTRRT__Start_Date__c = Date.valueOf('2012-10-21'),
				AVTRRT__Job_Term__c = 'Permanent',
				BCST__Salary_Payment_Interval__c = 'Month',
				AVTRRT__Experience__c = '01-03 Years',
				AVTRRT__Salary_Range__c = '25000 - 35000',
				AVTRRT__Country_Locale__c = 'India',
				Idibu_Job_Type__c = '3',
				Idibu_Location__c = '37,1',
				AVTRRT__Account_Job__c = accObj.Id,
				AVTRRT__Recruiter__c = userObj.Id);
		insert jobObj;

		System.currentPageReference().getParameters().put('id', jobObj.Id);
		// AVTRRT__Job__c jobObj = new AVTRRT__Job__c(AVTRRT__Job_Title__c = 'testJob',
		//    AVTRRT__Start_Date__c = date.Today(), AVTRRT__Account_Job__c =accObj.id, AVTRRT__Recruiter__c =userObj.id);

		ApexPages.StandardController sc = new ApexPages.StandardController(jobObj);
		SetBroadcastingExtrafieldsController controllerObj = new SetBroadcastingExtrafieldsController(sc);
		controllerObj.init();
		//controllerObj.isShowExtrafieldNames = false;
		controllerObj.jobTimeSelectOption = null;
		List<SelectOption> getjobTimeSelectOption = controllerObj.jobTimeSelectOption;
		controllerObj.salaryRangeSelectOption = null;
		List<SelectOption> getsalaryRangeSelectOption = controllerObj.salaryRangeSelectOption;
		controllerObj.categorySelectOption = null;
		List<SelectOption> getcategorySelectOption = controllerObj.categorySelectOption;
		controllerObj.jobTypeSelectOption = null;
		List<SelectOption> getjobTypeSelectOption = controllerObj.jobTypeSelectOption;
		//controllerObj.jobTypeSelectOption = null;
		//controllerObj.apply();
		//Folder fObj = [select id from Folder limit 1];
		//Folder fObj = new Folder(Name = 'Idibu');
		//insert fObj;
		//Document docObj = new Document(Name = 'default',FolderId = IdibuHelper.getIdibuFolderId(), Body = Blob.valueof('Test Body'));
		//insert docObj;
		/*List<Document> v_documentList = [
				SELECT Id
				FROM Document
				WHERE FolderId = :IdibuHelper.getIdibuFolderId()];

		v_documentList[0].Name = 'default'  ;
		controllerObj.getJobBoardExtraFields();
		*/
	}

	static testMethod void testInit2() {
		createCustomSettings();

		Profile pObj = [
				SELECT Id
				FROM Profile
				WHERE Name = 'Standard Platform User'
				LIMIT 1
		];
		User userObj = new User(FirstName = 'Dev',
				LastName = 'Smith',
				Email = 'smith@gmail.com',
				Username = 'smith@gmail.com.JBC',
				ProfileId = pObj.Id,
				Alias = 'dSmith',
				CommunityNickname = 'dSmith',
				TimeZoneSidKey = 'America/Los_Angeles',
				LocaleSidKey = 'en_IN',
				EmailEncodingKey = 'UTF-8',
				LanguageLocaleKey = 'en_US');
		insert userObj;

		Account accObj = new Account(Name = 'TestAcc');
		insert accObj;

		AVTRRT__Job__c jobObj = new AVTRRT__Job__c(
				AVTRRT__Job_Title__c = 'Java developer',
				AVTRRT__Job_Description__c = 'Java developer description',
				AVTRRT__Start_Date__c = Date.valueOf('2012-10-21'),
				AVTRRT__Job_Term__c = 'Permanent',
				BCST__Salary_Payment_Interval__c = 'Month',
				AVTRRT__Experience__c = '01-03 Years',
				AVTRRT__Salary_Range__c = '25000 - 35000',
				AVTRRT__Country_Locale__c = 'India',
				Idibu_Job_Type__c = '3',
				Idibu_Location__c = '37,1',
				AVTRRT__Account_Job__c = accObj.Id,
				AVTRRT__Recruiter__c = userObj.Id);
		insert jobObj;

		System.currentPageReference().getParameters().put('id', jobObj.Id);
		// AVTRRT__Job__c jobObj = new AVTRRT__Job__c(AVTRRT__Job_Title__c = 'testJob',
		//    AVTRRT__Start_Date__c = date.Today(), AVTRRT__Account_Job__c =accObj.id, AVTRRT__Recruiter__c =userObj.id);

		ApexPages.StandardController sc = new ApexPages.StandardController(jobObj);
		SetBroadcastingExtrafieldsController controllerObj = new SetBroadcastingExtrafieldsController(sc);
		controllerObj.init();
		controllerObj.isShowExtrafieldNames = true;
		controllerObj.jobTimeSelectOption = null;
		List<SelectOption> getjobTimeSelectOption = controllerObj.jobTimeSelectOption;
		controllerObj.salaryRangeSelectOption = null;
		List<SelectOption> getsalaryRangeSelectOption = controllerObj.salaryRangeSelectOption;
		controllerObj.categorySelectOption = null;
		List<SelectOption> getcategorySelectOption = controllerObj.categorySelectOption;
		controllerObj.jobTypeSelectOption = null;
		List<SelectOption> getjobTypeSelectOption = controllerObj.jobTypeSelectOption;
		//controllerObj.jobTypeSelectOption = null;
		//controllerObj.apply();
		//Folder fObj = [select id from Folder limit 1];
		//Folder fObj = new Folder(Name = 'Idibu');
		//insert fObj;
		//Document docObj = new Document(Name = 'default',FolderId = IdibuHelper.getIdibuFolderId(), Body = Blob.valueof('Test Body'));
		//insert docObj;
		/*List<Document> v_documentList = [
				SELECT Id
				FROM Document
				WHERE FolderId = :IdibuHelper.getIdibuFolderId()];

		v_documentList[0].Name = 'default'  ;
		controllerObj.getJobBoardExtraFields();
		*/
	}
	static testMethod void testJobBoardExtraFields() {
		System.debug('-------------' + IdibuHelper.getIdibuFolderId());
		Profile pObj = [
				SELECT Id
				FROM Profile
				WHERE Name = 'Standard Platform User'
				LIMIT 1
		];
		User userObj = new User(FirstName = 'Dev',
				LastName = 'Smith',
				Email = 'smith@gmail.com',
				Username = 'smith@gmail.com.JBC',
				ProfileId = pObj.Id,
				Alias = 'dSmith',
				CommunityNickname = 'dSmith',
				TimeZoneSidKey = 'America/Los_Angeles',
				LocaleSidKey = 'en_IN',
				EmailEncodingKey = 'UTF-8',
				LanguageLocaleKey = 'en_US');
		insert userObj;

		Account accObj = new Account(Name = 'TestAcc');
		insert accObj;

		AVTRRT__Job__c jobObj = new AVTRRT__Job__c(
				AVTRRT__Job_Title__c = 'Java developer',
				AVTRRT__Job_Description__c = 'Java developer description',
				AVTRRT__Start_Date__c = Date.valueOf('2012-10-21'),
				AVTRRT__Job_Term__c = 'Permanent',
				AVTRRT__Experience__c = '01-03 Years',
				BCST__Salary_Payment_Interval__c = 'Month',
				AVTRRT__Salary_Range__c = '25000 - 35000',
				AVTRRT__Country_Locale__c = 'India',
				Idibu_Job_Type__c = '3',
				Idibu_Location__c = '37,1',
				AVTRRT__Account_Job__c = accObj.Id,
				AVTRRT__Recruiter__c = userObj.Id);
		insert jobObj;

		System.currentPageReference().getParameters().put('id', jobObj.Id);
		// AVTRRT__Job__c jobObj = new AVTRRT__Job__c(AVTRRT__Job_Title__c = 'testJob',
		//    AVTRRT__Start_Date__c = date.Today(), AVTRRT__Account_Job__c =accObj.id, AVTRRT__Recruiter__c =userObj.id);
		/*IdibuJobBoard*/
		IdibuJobBoard testIdibuJobBoard = new IdibuJobBoard();
		testIdibuJobBoard.board_id = '338';
		testIdibuJobBoard.name = 'seek';
		testIdibuJobBoard.description = 'Testing using seek test job board';
		ApexPages.StandardController sc = new ApexPages.StandardController(jobObj);
		SetBroadcastingExtrafieldsController controllerObj = new SetBroadcastingExtrafieldsController(sc);
		controllerObj.init();
		controllerObj.isShowExtrafieldNames = true;
		controllerObj.jobTimeSelectOption = null;
		controllerObj.salaryRangeSelectOption = null;
		controllerObj.locationSelectOption = null;
		controllerObj.categorySelectOption = null;
		controllerObj.jobTypeSelectOption = null;

		List<String> jobBoards = controllerObj.jobBoardsList;
		//controllerObj.getJobBoardsList();
		controllerObj.apply();
		//Folder fObj = [select id from Folder limit 1];
		//Folder fObj = new Folder(Name = 'Idibu');
		//insert fObj;
		System.debug('---------------' + IdibuHelper.getIdibuFolderId());
		Document docObj = new Document(Name = 'Twitter', FolderId = IdibuHelper.getIdibuFolderId(), Body = Blob.valueOf('Test Body'));
		// insert docObj;
		List<Document> v_documentList = [
				SELECT Id
				FROM Document
				WHERE FolderId = :IdibuHelper.getIdibuFolderId()
		];

		//v_documentList[0].Name = 'Indeed';
		controllerObj.jobId = jobObj.Id;
		controllerObj.jobBoardName = 'seek';
		controllerObj.v_jobBoardNameMap.put(controllerObj.jobBoardName,testIdibuJobBoard);
		//controllerObj.getJobBoardExtraFields(); --commented by VK, 03/22/2017
		controllerObj.findJobBoardExtraFields();
		controllerObj.apply();
		controllerObj.doNothing();
		controllerObj.cancel();
		controllerObj.populateLocations();
		/*SetBroadcastingExtrafieldsController.extrafieldEx extrafieldEx = new
				SetBroadcastingExtrafieldsController.extrafieldEx();
		extrafieldEx.jobBoardName =  'seek';
		//extrafieldEx = SetBroadcastingExtrafieldsController.extrafieldEx;
		List<SelectOption> childOptionList = extrafieldEx.childOptionList;*/

		/*controllerObj.jobBoardName = 'seek';
		controllerObj.getJobBoardExtraFields();
		controllerObj.apply();*/

	}
	
	public static void createCustomSettings() {
		AVTRRT__Config_Settings__c cs = AVTRRT__Config_Settings__c.getValues('Default');
		if (cs == null) {
			cs = new AVTRRT__Config_Settings__c(Name = 'Default');
		}
		cs.AVTRRT__Enable_Geosearch_for_Account__c = false;
		cs.AVTRRT__Enable_Geosearch_for_Candidate__c = false;
		cs.AVTRRT__Enable_Geosearch_for_Contact__c = false;
		cs.AVTRRT__Enable_Geosearch_for_Job__c = false;
		upsert cs;
		
		Job_Broadcasting_Settings__c v_jobBroadcastingSettings =
				Job_Broadcasting_Settings__c.getInstance();
		v_jobBroadcastingSettings.Idibu_User_Id__c = '31000602';
		upsert v_jobBroadcastingSettings;
	}
}