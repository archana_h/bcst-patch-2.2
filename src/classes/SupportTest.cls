@IsTest
private class SupportTest {

	private static void initData() {
		BCST__Job_Broadcasting_Settings__c v_jobBroadcastingSettings =
				BCST__Job_Broadcasting_Settings__c.getInstance();
		v_jobBroadcastingSettings.BCST__Send_Debug_Logs__c = true;
		v_jobBroadcastingSettings.BCST__Support_Email_Address__c = 'support@mail.com';
		v_jobBroadcastingSettings.BCST__Idibu_User_Id__c = '1230';
		/* Edited by Aliaksandr Satskou, 03/14/2013 */
		upsert v_jobBroadcastingSettings;
	}

	static testMethod void testSupport() {
		initData();

		Test.startTest();
		Support.sendSupportEmail('eMail Description', 'eMail Body', false);
		Support.sendSupportEmail('eMail Description', 'eMail Body', true);
		Test.stopTest();
	}
}