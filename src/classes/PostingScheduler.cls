/**
 Class

 @Name: PostingScheduler
 @Author: Aliaksandr Satskou
 @Data: 08/07/2012
*/
global class PostingScheduler implements Schedulable {

	/**
	 Method

	 @Name: execute
	 @Author: Aliaksandr Satskou
	 @Data: 08/07/2012
	 @Parameters:
		1. |SchedulableContext p_schedulableContext| - Schedulable Context.
	 @Description: Execute body of scheduler.
	*/
	global void execute(SchedulableContext p_schedulableContext) {
		//String v_portionString = 'a0BU0000004gbtH, ,1080, , , , , , ';

		//PostingHelper.doBroadcast(v_portionString, false);
		
		Map<String, BCST__Job_Broadcasting_Scheduler__c> bsList =
				BCST__Job_Broadcasting_Scheduler__c.getAll();

		for (BCST__Job_Broadcasting_Scheduler__c bs : bsList.values()) {
			if (CronParser.isRunToday(bs.BCST__Cron__c)) {
				Database.executeBatch(new PostingBatch(bs.BCST__Criteria__c, bs.BCST__Board_IDs__c), 1);
			}
		}

	}
}