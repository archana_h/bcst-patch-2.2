/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@IsTest
private class TestSearchHelper {
	private static Map<String, Schema.SObjectField> fieldsMap {
		get {
			return Schema.SObjectType.AVTRRT__Job__c.fields.getMap();
		}
		private set;
	}

	static testMethod void testGetSObjectField() {
		Schema.SObjectField thisResult = SearchHelper.GetSObjectField('AVTRRT__Closed__c', fieldsMap);
		System.assertEquals(thisResult.getDescribe().getLabel(), 'Closed');
	}

	static testMethod void testGetDescribeFieldResult() {
		Schema.DescribeFieldResult thisResult = SearchHelper.GetDescribeFieldResult('AVTRRT__Closed__c',
				fieldsMap);
		System.assertEquals(thisResult.getLabel(), 'Closed');
	}

	static testMethod void testGetFieldLabel() {
		String thisResult = SearchHelper.GetFieldLabel('AVTRRT__Closed__c', fieldsMap);
		System.assertEquals(thisResult, 'Closed');
	}

	static testMethod void testGetFieldValue() {
		Object thisResult = SearchHelper.getFieldValue('AVTRRT__Closed__c', 'Closed', fieldsMap);
		System.assertEquals(thisResult, 'Closed');
	}

	static testMethod void testGetNameByID() {
		createCustomSettings();
		
		AVTRRT__Job__c job = new AVTRRT__Job__c();
		job.AVTRRT__Job_Title__c = 'Java developer';
		job.AVTRRT__Job_Description__c = 'Java developer description';
		job.AVTRRT__Start_Date__c = Date.valueOf('2011-10-21');
		job.AVTRRT__Job_Term__c = 'Permanent';
		job.AVTRRT__Experience__c = '01-03 Years';
		job.AVTRRT__Salary_Range__c = '25000 - 35000';
		job.AVTRRT__Country_Locale__c = 'India';
		job.Idibu_Job_Type__c = '3';
		job.Idibu_Location__c = '37,1';
		insert job;

		AVTRRT__Job__c testJob = [
				SELECT Name
				FROM AVTRRT__Job__c
				WHERE Id = :job.Id
		];

		String thisResult = SearchHelper.getNameByID(job.Id);
		System.assertEquals(thisResult, testJob.Name);
	}

	static testMethod void testGetQueryFields() {
		SearchHelper searchHelperObj = new SearchHelper();
		List<SearchHelper.QueryField> thisResult = searchHelperObj.GetQueryFields(fieldsMap);
		System.assertEquals(thisResult[0].Name, 'AVTRRT__AAP_Group__c');
	}

	static testMethod void testGetAllQueryFieldsString() {
		String thisResult = SearchHelper.GetAllQueryFieldsString(fieldsMap);
		System.assertNotEquals(null, thisResult);
	}

	static testMethod void testGetTextWithKeywords() {
		List<String> keyWordList = new List<String> {'java'};
		String val = '<html><head><title>h1_java</title></head><body><h1>java</h1></body></html>';
		String thisResult = SearchHelper.getTextWithKeywords(val, keyWordList);
		System.assertEquals(thisResult, '&lt;html&gt;&lt;head&gt;&lt;title&gt;h1_java&lt;/title&gt;' +
				'&lt;/head&gt;&lt;body&gt;&lt;h1&gt;<b style="background-color:yellow">java</b>&lt;' +
				'/h1&gt;&lt;/body&gt;&lt;/html&gt;');
	}

	static testMethod void testListOfFields() {
		List<String> thisResult = SearchHelper.ListOfFields(fieldsMap);
		System.assertNotEquals(thisResult.size(), 0);
	}

	static testMethod void testGetKeywordsList() {
		String keyWords = 'java and salesforce';
		List<String> thisResult = SearchHelper.GetKeywordsList(keyWords);
		System.assertEquals(thisResult[0], 'JAVA');
	}

	private static testMethod void testOthers() {
		SearchHelper searchHelperObj = new SearchHelper();
		Map<String, String> thisResult1 = searchHelperObj.GetSelectedFilterFieldsForJob();

		List<SearchHelper.QueryField> thisResult2 = searchHelperObj.GetSelectedQueryFieldsForJob(fieldsMap);
	}
	
	public static void createCustomSettings() {
		AVTRRT__Config_Settings__c cs = AVTRRT__Config_Settings__c.getValues('Default');
		if (cs == null) {
			cs = new AVTRRT__Config_Settings__c(Name = 'Default');
		}
		cs.AVTRRT__Enable_Geosearch_for_Account__c = false;
		cs.AVTRRT__Enable_Geosearch_for_Candidate__c = false;
		cs.AVTRRT__Enable_Geosearch_for_Contact__c = false;
		cs.AVTRRT__Enable_Geosearch_for_Job__c = false;
		upsert cs;
		
		Job_Broadcasting_Settings__c v_jobBroadcastingSettings =
				Job_Broadcasting_Settings__c.getInstance();
		v_jobBroadcastingSettings.Idibu_User_Id__c = '31000602';
		upsert v_jobBroadcastingSettings;
	}
}