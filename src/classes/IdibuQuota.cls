/**
 Class

 @name IdibuQuota
 @author Aliaksandr Satskou
 @description Class, which contains information about Job Board Quota.
 @data 09/05/2012
*/
public with sharing class IdibuQuota {
	public String status;
	public List<Quota> quotaList;

	public class Quota {
		public String portal_id;
		public String portal_name;

		public String posted_user;
		public String posted_team;
		public String posted_office;

		public String queued_user;
		public String queued_team;
		public String queued_office;

		public String remain_user;
		public String remain_team;
		public String remain_office;

		public String quota_user;
		public String quota_team;
		public String quota_office;
	}
}