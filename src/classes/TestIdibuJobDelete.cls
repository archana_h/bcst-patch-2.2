/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@IsTest
private class TestIdibuJobDelete {
	static testMethod void testBatch() {
		createCustomSettings();
		
		AVTRRT__Job__c job = new AVTRRT__Job__c();
		job.AVTRRT__Job_Title__c = 'Java developer';
		job.AVTRRT__Job_Description__c = 'Java developer description';
		job.AVTRRT__Start_Date__c = Date.valueOf('2011-10-21');
		job.AVTRRT__Job_Term__c = 'Permanent';
		job.AVTRRT__Experience__c = '01-03 Years';
		job.AVTRRT__Salary_Range__c = '25000 - 35000';
		job.AVTRRT__Country_Locale__c = 'India';
		insert job;
		Job_Broadcast__c jobBroadcast = new Job_Broadcast__c();
		jobBroadcast.Jobs__c = job.Id;
		jobBroadcast.Job_Board_Id__c = '517';
		jobBroadcast.Idibu_Job_Id__c = 'controlID';
		insert jobBroadcast;
		Job_Broadcast__c jobBroadcast2 = new Job_Broadcast__c();
		jobBroadcast2.Jobs__c = job.Id;
		jobBroadcast2.Job_Board_Id__c = '517';
		jobBroadcast2.Idibu_Job_Id__c = 'controlID';
		insert jobBroadcast2;

		JobDelete jDelete = new JobDelete();
		jDelete.idibuControlId = 'controlID';
		jDelete.jobId = job.Id;
		jDelete.idibuJobBoardId = new List<String> {'517'};
		List<JobDelete> jobDeleteList = new List<JobDelete> {jDelete};

		Test.startTest();
		IdibuJobDelete idibuJobDeteteBatch = new IdibuJobDelete(jobDeleteList);
		Database.executeBatch(idibuJobDeteteBatch, 1);
		Test.stopTest();
	}

	/**
	* @name testSort
	* @author Shashish kumar
	* @date 08/08/2013
	* @description Test method to Test doSendEmail method of IdibuJobDelete class.
	* @return Void.
	*/
	static testMethod void testDoSendEmail() {
		createCustomSettings();
		
		AVTRRT__Job__c job = new AVTRRT__Job__c();
		job.AVTRRT__Job_Title__c = 'Java developer';
		job.AVTRRT__Job_Description__c = 'Java developer description';
		job.AVTRRT__Start_Date__c = Date.valueOf('2011-10-21');
		job.AVTRRT__Job_Term__c = 'Permanent';
		job.AVTRRT__Experience__c = '01-03 Years';
		job.AVTRRT__Salary_Range__c = '25000 - 35000';
		job.AVTRRT__Country_Locale__c = 'India';
		insert job;
		Job_Broadcast__c jobBroadcast = new Job_Broadcast__c();
		jobBroadcast.Jobs__c = job.Id;
		jobBroadcast.Job_Board_Id__c = '517';
		jobBroadcast.Idibu_Job_Id__c = 'controlID';
		insert jobBroadcast;
		Job_Broadcast__c jobBroadcast2 = new Job_Broadcast__c();
		jobBroadcast2.Jobs__c = job.Id;
		jobBroadcast2.Job_Board_Id__c = '517';
		jobBroadcast2.Idibu_Job_Id__c = 'controlID';
		insert jobBroadcast2;

		JobDelete jDelete = new JobDelete();
		jDelete.idibuControlId = 'controlID';
		jDelete.jobId = job.Id;
		jDelete.idibuJobBoardId = new List<String> {'517'};
		List<JobDelete> jobDeleteList = new List<JobDelete> {jDelete};
		
		Test.startTest();
		IdibuResponse idibuResObj = new IdibuResponse();
		IdibuResponse.posts postObj = new IdibuResponse.posts();
		postObj.posts_status = 'testStatus';
		postObj.innerText = 'testInner';
		List<IdibuResponse.posts> postLst = new List<IdibuResponse.posts>();
		postLst.add(postObj);
		idibuResObj.postsList = postLst;
		IdibuJobDelete idibuJobDeteteBatch = new IdibuJobDelete(jobDeleteList);
		idibuJobDeteteBatch.doSendEmail(idibuResObj);
		Database.executeBatch(idibuJobDeteteBatch, 1);
		Test.stopTest();

	}
	
	public static void createCustomSettings() {
		AVTRRT__Config_Settings__c cs = AVTRRT__Config_Settings__c.getValues('Default');
		if (cs == null) {
			cs = new AVTRRT__Config_Settings__c(Name = 'Default');
		}
		cs.AVTRRT__Enable_Geosearch_for_Account__c = false;
		cs.AVTRRT__Enable_Geosearch_for_Candidate__c = false;
		cs.AVTRRT__Enable_Geosearch_for_Contact__c = false;
		cs.AVTRRT__Enable_Geosearch_for_Job__c = false;
		upsert cs;
		
		Job_Broadcasting_Settings__c v_jobBroadcastingSettings =
				Job_Broadcasting_Settings__c.getInstance();
		v_jobBroadcastingSettings.Idibu_User_Id__c = '31000602';
		upsert v_jobBroadcastingSettings;
	}
}