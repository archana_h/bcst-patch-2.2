// Used to get values from Config object
public class ConfigHelper {
	public static String Skillset = 'Skillset';
	public static String ImportResume = 'ImportResume';
	public static String ResumeParse = 'resumeparse';
	public static String JobNotificationTemplateName = 'Job Notification Template Name';
	public static String CoverLetter = 'Cover Letter';
	public static String JobNotificationToVendors = 'Job Notification to Vendors';
	public static String CloseJobOnPlacement = 'Close Job on Placement';
	public static String EmploymentParseMapping = 'employmentparsemapping';
	public static String EducationalParseMapping = 'educationalparsemapping';
	public static String SkillsParseMapping = 'skillsparsemapping';
	public static String DaxtraParseMapping = 'daxtraparsemapping';
	public static String CoverAttachments = 'Cover Attachments';

	public static String FilterFieldsDefault = 'Filter Fields';
	public static String FilterFieldsUser = FilterFieldsDefault + ' - ' + UserInfo.getUserId();

	public static String DuplicatesDetectionRule = 'Duplicates Detection Rule';
	public static String UpdateIfduplicates = 'Update If Duplicates';

	// Gets config record by config name.
	// Returns existing record from database or new record if no such record in database
	public static AVTRRT__Config__c GetConfig(String configName) {
		AVTRRT__Config__c config;

		// Query record from database
		List<AVTRRT__Config__c> configs = [
				SELECT a.AVTRRT__Value__c
				FROM AVTRRT__Config__c a
				WHERE a.AVTRRT__Name__c = :configName
				LIMIT 1
		];
		
		if (configs.size() > 0) {
			config = configs[0];
		}

		// If there is no such record in database create a new one
		if (config == null) {
			config = new AVTRRT__Config__c(AVTRRT__Name__c = configName);
		}

		return config;
	}

	public static AVTRRT__Config__c SetConfig(String configName, String value) {
		// Gets config record from database by config name
		AVTRRT__Config__c config = GetConfig(configName);

		// Changes value and update config record
		config.AVTRRT__Value__c = value;
		upsert config;

		return config;
	}

	public static AVTRRT__Config_Settings__c getConfigDataSet() {
		return AVTRRT__Config_Settings__c.getValues('Default');
	}

	// Gets config record by config name.
	public static Object getConfigSetting(String configName) {
		AVTRRT__Config_Settings__c config = getConfigDataSet();
		return (config != null) ? config.get(configName) : null;
	}

	// Gets config record values parsed by Name/Value from Custom Settings.
	public static Map<String, String> getConfigSettingNameValue(String configName) {
		Object obj = getConfigSetting(configName);
		if (obj != null) {
			return getConfigNameValue((String)obj);
		}

		return null;
	}

	// Gets config record values parsed by Name/Value.
	public static Map<String, String> getConfigNameValue(String configString) {
		Map<String, String> pairs = new Map<String, String>();

		for (String singlePair : configString.split(';')) {
			if (singlePair.trim() != '') {
				List<String> nameValue = singlePair.split('=');
				pairs.put(nameValue[0], nameValue.size() > 1 ? nameValue[1] : '');
			}
		}

		return pairs;
	}

	public static void setConfigSetting(String configName, Object value) {
		AVTRRT__Config_Settings__c config = getConfigDataSet();
		if (config == null) {
			config = setConfigDataSet(configName, value);
		} else {
			config.put(configName, value);
			update config;
		}
	}

	private static AVTRRT__Config_Settings__c setConfigDataSet(String configName, Object value) {
		AVTRRT__Config_Settings__c config = new AVTRRT__Config_Settings__c(Name = 'Default');
		config.put(configName, value);
		insert config;

		return config;
	}
}