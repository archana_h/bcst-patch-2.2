/*
* @author Vishwajeet
* @date 09/03/2013
* @description Class is using to fetch Job Board XML's from Idibu and save in 'Idibu' folder (case #00021059).
*/
global with sharing class IdibuJobBoardXmlBatch
		implements Database.Batchable<IdibuJobBoard>, Database.AllowsCallouts {
	
	private final Id idibuFolderId;
	private final String idibuHash;
	private List<IdibuJobBoard> jobBoardList;
	
	/*
	* @author Vishwajeet
	* @name IdibuJobBoardXmlBatch
	* @date 09/03/2013
	* @description Constructor. Init data.
	* @return -
	*/
	global IdibuJobBoardXmlBatch(Id p_idibuFolderId, String p_idibuHash, List<IdibuJobBoard> p_jobBoardList) {
		idibuFolderId = p_idibuFolderId;
		idibuHash = p_idibuHash;
		jobBoardList = p_jobBoardList;

		System.debug(LoggingLevel.ERROR, ':::::::::::::::::::::::IdibuJobBoardXmlBatch=' + jobBoardList);
	}
	
	/*
	* @author Vishwajeet
	* @name start
	* @date 09/03/2013
	* @description Apex Batch "start" method.
	* @return Iterable<IdibuJobBoard>
	*/
	global Iterable<IdibuJobBoard> start(Database.BatchableContext BC) {
		return jobBoardList;
	}
	
	/*
	* @author Vishwajeet
	* @name execute
	* @date 09/03/2013
	* @description Apex Batch "execute" method. Getting and saving XML's.
	* @return void
	*/
	global void execute(Database.BatchableContext BC, List<IdibuJobBoard> p_jobBoardList) {
		for (IdibuJobBoard v_idibuJobBoard : p_jobBoardList) {
			String v_jobBoardXML = IdibuHelper.getJobBoardDataFromIdibu(
					v_idibuJobBoard.board_id, idibuHash);
			System.debug(LoggingLevel.ERROR, 'v_jobBoardXML--' + v_jobBoardXML);
			//Added by vishwajeet, to remove heap size error, 05/17/2017
			v_jobBoardXML = IdibuHelper.findAttrStr(v_jobBoardXML);
			v_jobBoardXML = v_jobBoardXML.replace('\'', '');
			v_jobBoardXML = v_jobBoardXML.replace('`', '');

			insert IdibuHelper.createJobBoardXMLDocument(
					v_idibuJobBoard.board_id + '-' + v_idibuJobBoard.name, v_jobBoardXML, idibuFolderId);
		}

	}
	
	/*
	* @author Vishwajeet
	* @name finish
	* @date 09/03/2013
	* @description Apex Batch "finish" method.
	* @return void
	*/
	global void finish(Database.BatchableContext BC) {
	}
}