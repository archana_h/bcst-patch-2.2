global with sharing class Logger {
	private static String messages = '';
	
	private String className;
	
	public Boolean throwException = false;
	public static Boolean throwExceptionOnTest = true;
	
	global Logger(String className) {
		this.className = className;
	}
	
	global Logger(String className, Boolean throwException) {
		this.className = className;
		this.throwException = throwException;
	}
	
	global void log(String methodName) {
		log(methodName, '');
	}
	
	global void log(String methodName, String variable, Object value) {
		log(methodName, variable + ' = ' + value);
	}
	
	global void log(String methodName, String message) {
		String line;
		
		try {
			line = className + ' : ' + methodName + ' : ' + message;
			System.debug(line);
			//throw new LoggerException();
		} catch (LoggerException le) {
			String stackTrace = le.getStackTraceString();
			
			List<String> stackLines = stackTrace.split('\n');
			
			if (stackLines.size() > 1) {
				String caller = stackLines[1]; // stackLines[0] = CrazyLogger, we need to return info from 1 level up.
				line = caller.left(caller.indexOf(':'));
			} else {
				line = 'Unknown Line. Stacktrace was empty';
			}
		}
	
	}
	
	global void catchBlockMethodNew(Exception e) {
		addErrorMessage(e.getMessage());
		log('exception', e.getMessage());
		addErrorMessage(e.getStackTraceString());
		log('exception', e.getStackTraceString());
		
		sendLog(e.getMessage(), className);
		
		if ((Test.isRunningTest() && throwExceptionOnTest) || throwException) {
			throw e;
		}
	}
	
	global String getMessagesString() {
		List<String> results = new List<String>();
		for (ApexPages.Message message : ApexPages.getMessages()) {
			results.add(message.getSeverity() + ': ' + message.getSummary());
		}
		return String.join(results, '<br />');
	}
	
	global static void addErrorMessage(String message) {
		// We can show the message if request was done from VF page only.
		// We can't call ApexPages.addMessage if we made request from webservice or trigger.
		if (ApexPages.currentPage() != null) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, message);
			ApexPages.addMessage(msg);
		}
	}
	
	global static void addWarningMessage(String message) {
		// We can show the message if request was done from VF page only.
		// We can't call ApexPages.addMessage if we made request from webservice or trigger.
		if (ApexPages.currentPage() != null) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, message);
			ApexPages.addMessage(msg);
		}
	}
	
	global static void addInfoMessage(String message) {
		// We can show the message if request was done from VF page only.
		// We can't call ApexPages.addMessage if we made request from webservice or trigger.
		if (ApexPages.currentPage() != null) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, message);
			ApexPages.addMessage(msg);
		}
	}
	
	/**
	* @name sendLog
	* @param lastMessage String
	* @param subject String
	* @return void
	*/
	global static void sendLog(String lastMessage, String subject) {
		messages =
				'Organization: \'' + UserInfo.getOrganizationName() + '\'\r\n' +
						((ApexPages.currentPage() != null) ? ('Page Url: \'' + ApexPages.currentPage().getUrl() + '\'\r\n') : '') +
						((Site.getName() != null) ? ('Site Name: \'' + Site.getName() + '\'\r\n') : '') +
						'\r\n' + messages;
		
		/*String fullLog = messages + lastMessage;

		Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
		emailMessage.setSubject(subject);
		emailMessage.setPlainTextBody(fullLog);
		emailMessage.setToAddresses(new List<String> {
				ConfigHelper.getConfigDataSet().ErrorsReceiver__c });

		Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailMessage});*/
	}
	
	public with sharing class LoggerException extends Exception {
	}
	
	public void addPageErrorMessage(String message) {
	}
	public Logger(String className, Boolean throwException, Boolean showMessages) {
	}
	public static void addMessage(String className, String methodName, String message, Boolean showMessages) {
	}

}