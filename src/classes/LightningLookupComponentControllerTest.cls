@IsTest
private class LightningLookupComponentControllerTest {
	static testMethod void successMethod() {
		Test.startTest();
		
		Account account = new Account();
		account.Name = 'test';
		insert account;
		
		LightningLookupComponentController.getLookupRecords('account', 'name', 'test', false);
		
		Test.stopTest();
	}
}