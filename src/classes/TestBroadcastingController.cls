/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@IsTest(SeeAlldata = true)
private class TestBroadcastingController {
	public static String v_recruiterId = UserInfo.getUserId();
	
	static testMethod void testBroadcast() {
		createCustomSettings();
		
		AVTRRT__Job__c job = new AVTRRT__Job__c();
		job.AVTRRT__Job_Title__c = 'Java developer UTest';
		job.AVTRRT__Job_Description__c = 'Java developer description';
		job.AVTRRT__Job_Description_Rich__c = 'Test job description Rich';
		job.AVTRRT__Start_Date__c = Date.valueOf('2011-10-21');
		job.AVTRRT__Job_Term__c = 'Permanent';
		job.AVTRRT__Experience__c = '01-03 Years';
		job.AVTRRT__Salary_Range__c = '25000 - 35000';
		job.AVTRRT__Country_Locale__c = 'India';
		job.BCST__Idibu_Salary_Range__c = '25000 - 35000';
		job.Idibu_Job_Type__c = '4';
		job.Idibu_Location__c = '9,0';
		job.Idibu_Category__c = '18';
		job.AVTRRT__Billable__c = true;
		job.AVTRRT__Recruiter__c = v_recruiterId;
		insert job;
		
		String portionString = job.Id + ',188, , , , , , , , ';
		
		Test.startTest();
		try {
			BroadcastingController.updateJobExtraFieldFromMapping(new List<String> {job.Id});
			String result = BroadcastingController.broadcast(portionString, false);
		} catch (Exception ex) {
		
		}
		Test.stopTest();
		
		List<AVTRRT__Job__c> joblist = new List<AVTRRT__Job__c>();
		joblist.add(job);
		
		PostingBatch postingBat = new PostingBatch('AVTRRT__Billable__c=true', '188');
		//new PostingScheduler().execute(null);
		
	}
	
	static testMethod void tesMissingPostBatch() {
		createCustomSettings();
		
		AVTRRT__Job__c job = new AVTRRT__Job__c();
		job.AVTRRT__Job_Title__c = 'Java developer UTest';
		job.AVTRRT__Job_Description__c = 'Java developer description';
		job.AVTRRT__Job_Description_Rich__c = 'Test job description Rich';
		job.AVTRRT__Start_Date__c = Date.valueOf('2011-10-21');
		job.AVTRRT__Job_Term__c = 'Permanent';
		job.AVTRRT__Experience__c = '01-03 Years';
		job.AVTRRT__Salary_Range__c = '25000 - 35000';
		job.AVTRRT__Country_Locale__c = 'India';
		job.BCST__Idibu_Salary_Range__c = '25000 - 35000';
		job.Idibu_Job_Type__c = '4';
		job.Idibu_Location__c = '9,0';
		job.Idibu_Category__c = '18';
		job.AVTRRT__Billable__c = true;
		job.AVTRRT__Recruiter__c = v_recruiterId;
		insert job;
		
		MissingJobPostingBatch missingpostingBat = 
				new MissingJobPostingBatch('AVTRRT__Billable__c=true', '188');
		//Database.executeBatch(missingpostingBat);
	}
	static testMethod void testOthers() {
		BroadcastingController bc = new BroadcastingController();
		bc.init();
		bc.searchText = 'java AND salesforce';
		bc.search();
		bc.searchText = '';
		bc.search();
		
		bc.searchObj.AVTRRT__Keywords__c = '(java and salesforce) or C# and not pascal';
		String keyWords = bc.getKeywords(bc.searchObj.AVTRRT__Keywords__c);
		System.assertEquals(keyWords, '(JAVA AND SALESFORCE) OR C# AND NOT PASCAL');
		
		bc.searchObj.AVTRRT__p1__c = 'none';
		bc.searchObj.AVTRRT__e1__c = 'none';
		bc.searchObj.AVTRRT__v1__c = '';
		bc.searchObj.AVTRRT__p2__c = 'none';
		bc.searchObj.AVTRRT__e2__c = 'none';
		bc.searchObj.AVTRRT__v2__c = '';
		bc.searchObj.AVTRRT__p3__c = 'none';
		bc.searchObj.AVTRRT__e3__c = 'none';
		bc.searchObj.AVTRRT__v3__c = '';
		bc.searchObj.AVTRRT__p4__c = 'none';
		bc.searchObj.AVTRRT__e4__c = 'none';
		bc.searchObj.AVTRRT__v4__c = '';
		bc.searchObj.AVTRRT__p5__c = 'none';
		bc.searchObj.AVTRRT__e5__c = 'none';
		bc.searchObj.AVTRRT__v5__c = '';
		bc.searchObj.AVTRRT__p6__c = 'none';
		bc.searchObj.AVTRRT__e6__c = 'none';
		bc.searchObj.AVTRRT__v6__c = '';
		bc.searchObj.AVTRRT__p7__c = 'none';
		bc.searchObj.AVTRRT__e7__c = 'none';
		bc.searchObj.AVTRRT__v7__c = '';
		bc.searchObj.AVTRRT__p8__c = 'none';
		bc.searchObj.AVTRRT__e8__c = 'none';
		bc.searchObj.AVTRRT__v8__c = '';
		bc.searchObj.AVTRRT__p9__c = 'none';
		bc.searchObj.AVTRRT__e9__c = 'none';
		bc.searchObj.AVTRRT__v9__c = '';
		bc.searchObj.AVTRRT__p10__c = 'none';
		bc.searchObj.AVTRRT__e10__c = 'none';
		bc.searchObj.AVTRRT__v10__c = '';
		bc.buildSearchSOSL();
	}
	
	static testMethod void testGetBroadcastDateString() {
		List<AVTRRT__Job__c> v_jobList = new List<AVTRRT__Job__c> {
				new AVTRRT__Job__c(
						AVTRRT__Job_Title__c = 'Java developer',
						AVTRRT__Job_Description__c = 'Java developer description',
						AVTRRT__Start_Date__c = Date.valueOf('2011-10-21'),
						AVTRRT__Job_Term__c = 'Permanent',
						AVTRRT__Experience__c = '01-03 Years',
						AVTRRT__Salary_Range__c = '25000.0 - 35000.0',
						AVTRRT__Country_Locale__c = 'India',
						Idibu_Salary_Range__c = '25000.0 - 35000.0',
						Idibu_Job_Type__c = '3',
						Idibu_Location__c = '37,1',
						Idibu_Category__c = '18',
						AVTRRT__Recruiter__c = v_recruiterId
				),
				new AVTRRT__Job__c(
						AVTRRT__Job_Title__c = 'Java developer',
						AVTRRT__Job_Description__c = 'Java developer description',
						AVTRRT__Start_Date__c = Date.valueOf('2011-10-21'),
						AVTRRT__Job_Term__c = 'Permanent',
						AVTRRT__Experience__c = '01-03 Years',
						AVTRRT__Salary_Range__c = '25000.0 - 35000.0',
						AVTRRT__Country_Locale__c = 'India',
						Idibu_Salary_Range__c = '25000.0 - 35000.0',
						Idibu_Job_Type__c = '3',
						Idibu_Location__c = '37,1',
						Idibu_Category__c = '18',
						AVTRRT__Recruiter__c = v_recruiterId
				)
		};
		insert v_jobList;
		
		List<BCST__Job_Broadcast__c> v_jobBroadcastList = new List<BCST__Job_Broadcast__c> {
				new BCST__Job_Broadcast__c(
						BCST__Job_Board_Name__c = 'Twitter',
						BCST__Jobs__c = v_jobList[1].Id,
						BCST__Broadcasted__c = true
				)
		};
		insert v_jobBroadcastList;
		
		String v_bStr = 'j_id0:broadcastingForm:j_id259:j_id260:j_id261:0:j_id266:0:postDate&' +
				v_jobList[0].Id + '&Career Builder UK,j_id0:broadcastingForm:j_id259:j_id260:' +
				'j_id261:0:j_id266:1:postDate&' + v_jobList[1].Id + '&Twitter';
		String v_broadcastDateString = BroadcastingController.getBroadcastDateString(v_bStr);
		
		BCST__Job_Broadcast__c v_insertedJobBroadcast = [
				SELECT CreatedDate
				FROM BCST__Job_Broadcast__c
				WHERE Id = :v_jobBroadcastList[0].Id
				LIMIT 1
		];
		
		String v_expectedResult = 'j_id0:broadcastingForm:j_id259:j_id260:j_id261:0:j_id266:1:postDate*' +
				v_insertedJobBroadcast.CreatedDate;
		//System.assertEquals(v_broadcastDateString, v_expectedResult);
		//new MissingJobPostingScheduler().execute(null);
	}
	
	static testMethod void testFormatDateTimeToQueryString() {
		createCustomSettings();
		
		AVTRRT__Job__c job = new AVTRRT__Job__c();
		job.AVTRRT__Job_Title__c = 'Java developer UTest';
		job.AVTRRT__Job_Description__c = 'Java developer description';
		job.AVTRRT__Start_Date__c = Date.valueOf('2011-10-21');
		job.AVTRRT__Job_Term__c = 'Permanent';
		job.AVTRRT__Experience__c = '01-03 Years';
		job.AVTRRT__Salary_Range__c = '25000 - 35000';
		job.AVTRRT__Country_Locale__c = 'India';
		job.BCST__Idibu_Salary_Range__c = '25000 - 35000';
		insert job;
		ApexPages.currentPage().getParameters().put('Id', job.Id);
		BroadcastingController bc = new BroadcastingController();
		bc.init();
		
		Datetime dTime = Datetime.now();
		String result = bc.FormatDateTimeToQueryString(dTime);
		String expected = dTime.format('yyyy-MM-dd') + 'T' + dTime.format('HH:mm:ss') + 'Z';
		System.assertEquals(result, expected);
		
		List<String> pickListValues = BroadcastingController.getPicklistValues('AVTRRT__Job_Term__c');
		System.assertEquals(pickListValues.size(), 6);
		
		bc.getConstraint('AVTRRT__Start_Date__c', '=', Date.today().format());
		bc.getConstraint('', '', null);
		bc.getConstraint('AVTRRT__Closed__c', '=', 'false');
		bc.getConstraint('Name', '=', 'test_text');
		bc.getConstraint('AVTRRT__Job_Term__c', '=', 'Permanent');
		bc.getConstraint('AVTRRT__Number_of_Positions__c', '=', '0');
		bc.getConstraint('AVTRRT__Job_Term__c', '=', 'Permanent,Permanent');
		bc.getConstraint('AVTRRT__Job_Term__c', 'contains', 'Permanent');
		bc.getConstraint('AVTRRT__Job_Term__c', 'startswith', 'Permanent');
		bc.getConstraint('AVTRRT__Job_Term__c', 'not contains', 'Permanent');
	}
	
	static testMethod void testDeleteBroadcasted() {
		String portionString = 'a0AA0000009OAVuMAO,1193';
		/* Edited by Aliaksandr Satskou, 03/13/2013 */
		Integer v_jobBoardCount = IdibuHelper.getIdibuJobBoardList(false).size();
		
		if ((v_jobBoardCount != null) && (v_jobBoardCount > 0)) {
			for (Integer i = 0; i < v_jobBoardCount - 1; i++) {
				portionString += ', ';
			}
		}
		
		String idibuJBListObj = '[{"tags":["general"],"name":"Career Builder UK","description":"",' +
				'"board_id":"188"},{"tags":null,"name":"idibu Developer Board","description":"",' +
				'"board_id":"517"},{"tags":["international"],"name":"Monster India","description":"",' +
				'"board_id":"1001"},{"tags":["general"],"name":"Americas Job Exchange","description":' +
				'"","board_id":"1193"}]';
		BroadcastingController.deleteBroadcasted(portionString);
	}
	
	static testMethod void testGetWhereClause() {
		createCustomSettings();
		
		AVTRRT__Job__c job = new AVTRRT__Job__c();
		job.AVTRRT__Job_Title__c = 'Java developer UTest';
		job.AVTRRT__Job_Description__c = 'Java developer description';
		job.AVTRRT__Start_Date__c = Date.valueOf('2011-10-21');
		job.AVTRRT__Job_Term__c = 'Permanent';
		job.AVTRRT__Experience__c = '01-03 Years';
		job.AVTRRT__Salary_Range__c = '25000 - 35000';
		job.AVTRRT__Country_Locale__c = 'India';
		job.BCST__Idibu_Salary_Range__c = '25000 - 35000';
		insert job;
		ApexPages.currentPage().getParameters().put('Id', job.Id);
		BroadcastingController bc = new BroadcastingController();
		bc.init();
		bc.searchObj.AVTRRT__Advanced_Filter_Conditions__c = 'www';
		String result = bc.getWhereClause('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', false);
		System.assertEquals(result, '(www)');
		bc.searchObj.AVTRRT__Advanced_Filter_Conditions__c = '1';
		bc.getWhereClause('', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', false);
		bc.searchObj.AVTRRT__Advanced_Filter_Conditions__c = '2';
		bc.getWhereClause('a', '', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', false);
		bc.searchObj.AVTRRT__Advanced_Filter_Conditions__c = '3';
		bc.getWhereClause('a', 'b', '', 'd', 'e', 'f', 'g', 'h', 'i', 'j', false);
		bc.searchObj.AVTRRT__Advanced_Filter_Conditions__c = '4';
		bc.getWhereClause('a', 'b', 'c', '', 'e', 'f', 'g', 'h', 'i', 'j', false);
		bc.searchObj.AVTRRT__Advanced_Filter_Conditions__c = '5';
		bc.getWhereClause('a', 'b', 'c', 'd', '', 'f', 'g', 'h', 'i', 'j', false);
		bc.searchObj.AVTRRT__Advanced_Filter_Conditions__c = '6';
		bc.getWhereClause('a', 'b', 'c', 'd', 'e', '', 'g', 'h', 'i', 'j', false);
		bc.searchObj.AVTRRT__Advanced_Filter_Conditions__c = '7';
		bc.getWhereClause('a', 'b', 'c', 'd', 'e', 'f', '', 'h', 'i', 'j', false);
		bc.searchObj.AVTRRT__Advanced_Filter_Conditions__c = '8';
		bc.getWhereClause('a', 'b', 'c', 'd', 'e', 'f', 'g', '', 'i', 'j', false);
		bc.searchObj.AVTRRT__Advanced_Filter_Conditions__c = '9';
		bc.getWhereClause('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '', 'j', false);
		bc.searchObj.AVTRRT__Advanced_Filter_Conditions__c = '10';
		bc.getWhereClause('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', '', false);
	}
	
	/**
	* @name testSort
	* @author Shashish kumar
	* @date 08/08/2013
	* @description Test method to Test Sort and Broadcast method.
	* @return Void.
	*/
	static testMethod void testSort() {
		AVTRRT__Job__c job = new AVTRRT__Job__c();
		job.AVTRRT__Job_Title__c = 'Java developer UTest';
		job.AVTRRT__Job_Description__c = 'Java developer description';
		job.AVTRRT__Start_Date__c = Date.valueOf('2011-10-21');
		job.AVTRRT__Job_Term__c = 'Permanent';
		job.AVTRRT__Experience__c = '01-03 Years';
		job.AVTRRT__Salary_Range__c = '25000 - 35000';
		job.AVTRRT__Country_Locale__c = 'India';
		job.BCST__Idibu_Salary_Range__c = '25000 - 35000';
		insert job;
		ApexPages.currentPage().getParameters().put('Id', job.Id);
		BroadcastingController bc = new BroadcastingController();
		bc.init();
		bc.titleSortCriteria = '';
		bc.sort();
		bc.titleSortCriteria = 'ASC';
		bc.sort();
		bc.titleSortCriteria = 'DESC';
		bc.sort();
		bc.returnToJob();
		
		BroadcastingController.broadcast(job.Id, 'p_boardIdsString');
		BroadcastingController.broadcast('p_portionString');
		BroadcastingController.broadcast('p_portionString', 'p_jobListJSON', 'p_idibuJobBoardListJSON');
		BroadcastingController.insertQuotaDetails(job.Id);
	
	}
	
	//Method to increase code coverage
	static testMethod void codeCoverage() {
		String v_CurrencyXml = '<idibu generator="idibu" version="1.0">' +
				'<response><currencies><currency><id>AED</id><name>UAE Dirham</name>' +
				'<symbol>AED</symbol><html>AED</html><code>AED</code></currency></currencies>' +
				'</response><status>success</status></idibu>';
		dom.Document v_document = new dom.Document();
		v_document.load(v_CurrencyXml);
		IdibuHelper.buildCurrencyDomXML(v_document);
		List<SelectOption> cities = IdibuHelper.getListOfLocation('US', 'San Diego');
		//Quota Helper Methods testing
		//Map<String,Integer> quotas = IdibuHelper.getJobBoardQuotaDetailsFromIdibu();
	
	}
	
	public static void createCustomSettings() {
		AVTRRT__Config_Settings__c cs = AVTRRT__Config_Settings__c.getValues('Default');
		if (cs == null) {
			cs = new AVTRRT__Config_Settings__c(Name = 'Default');
		}
		cs.AVTRRT__Enable_Geosearch_for_Account__c = false;
		cs.AVTRRT__Enable_Geosearch_for_Candidate__c = false;
		cs.AVTRRT__Enable_Geosearch_for_Contact__c = false;
		cs.AVTRRT__Enable_Geosearch_for_Job__c = false;
		upsert cs;
	}
}