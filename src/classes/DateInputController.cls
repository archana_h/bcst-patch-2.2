public class DateInputController {
	public String DateNow {
		get {
			Datetime dt = Datetime.now();
			return dt.format('M/dd/yyyy');
		}
	}
}