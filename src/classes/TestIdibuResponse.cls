/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@IsTest
private class TestIdibuResponse {

	static testMethod void testIdibuResponseClass() {
		IdibuResponse iResponse = new IdibuResponse();
		iResponse.job_status = 'delayed';
		iResponse.job_client_reference = '';
		iResponse.job_id = '31035750';
		iResponse.job_postingid = '174';
		iResponse.postcompletionurl = 'https://adpost.idibu.com/eemwscY.html';
		List<String> errorsList = new List<String>();
		errorsList.add('Field \'monsterind_Industry\' was not supplied, or was supplied with an option that is an empty string or contains only whitespace.');
		errorsList.add('Field \'monsterind_CatOpp\' was not supplied, or was supplied with an option that is an empty string or contains only whitespace.');
		errorsList.add('Field \'monsterind_mainloc\' is missing, an empty string, or contains only whitespace.');
		iResponse.errorsList = errorsList;
		List<String> warningsList = new List<String>();
		warningsList.add('The partner id provided is not valid. Please contact your account administrator or email support@idibu.com');
		warningsList.add('Field \'monsterind_hidesal\' is missing, an empty string, or contains only whitespace.');
		warningsList.add('Field \'monsterind_moreloca\' was not supplied, or was supplied with an option that is an empty string or contains only whitespace.');
		iResponse.warningsList = warningsList;
		List<IdibuResponse.posts> postsList = new List<IdibuResponse.posts>();
		IdibuResponse.posts posts = new IdibuResponse.posts();
		postsList.add(posts);
		iResponse.postsList = postsList;
	}
}