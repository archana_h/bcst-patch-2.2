/**
* @description Controller class holding logic for posting the selected Job, 
* deleting adverts from the Broadbean for selected Job and getting the job status for the Job to show on UI.  
**/

public with sharing class BroadBeanJobPostingController {
	public BroadBeanJobPostingUtils.AdvertWrapper advertWrapper { get; set; }
	public Set<String> postedJobBoardIdSet { get; set; }
	public Set<String> postedJobBoardStatus { get; set; }
	public List<SelectOption> m_PostedJobBoards { get; set; } //job boards list
	public List<String> m_Channels { get; set; } //Channels
	
	private BroadBeanJobPostingUtils.BroadBeanConnectionInfoWrapper connectionInfo; //Connection info, Broadbean,VK,4/23/2018
	public AVTRRT__Job__c currentJob { get; set; } //Current job Data
	private String selectedJobId; // Selected Job record Id
	
	//ChannelStatusWrapper List to dispaly on Channel Status component
	public List<BroadBeanJobPostingUtils.ChannelStatusWrapper> channelStatusWrapperList { get; set; }
	// Boolean to hold if status check record available or not.
	public Boolean isBroadbeanJobPostingStatusAvailable {
		get {
			return (channelStatusWrapperList == null ||
					channelStatusWrapperList.isEmpty())
					? false : true;
		}
		private set;
	}
	
	String[] channels = new String[] {};
	
	/* Constructor */
	
	public BroadBeanJobPostingController(ApexPages.StandardController controller) {
		selectedJobId = ApexPages.currentPage().getParameters().get('id');
	}
	
	public void init() {
		try {
			m_PostedJobBoards = new List<SelectOption>();
			connectionInfo = BroadBeanJobPostingUtils.getBroadBeanConnectionInfo();
			currentJob = BroadBeanJobPostingUtils.getJobDetailById(selectedJobId);
			advertWrapper = BroadBeanJobPostingUtils.getJobBoardStatusCheck(connectionInfo, currentJob.Name);
			System.debug('advertWrapper -- ' + advertWrapper);
			
			if (advertWrapper != null && advertWrapper.Id != null) {
				postedJobBoardIdSet = BroadBeanJobPostingUtils.getPostedJobBoardsV1(connectionInfo, currentJob.Name, advertWrapper);
				//VK,5/15/2018 getBoards();
				m_PostedJobBoards.addAll(getBoards());
			} else {
				if (advertWrapper.isFailed) {
					String v_ErrSt = 'Error on Broadbean.';
					if (String.isNotEmpty(advertWrapper.failedMessage)) {
						v_ErrSt += advertWrapper.failedMessage;
					}
					ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.INFO, v_ErrSt));
					
					return;
				
				}
			}
			
			//getJobChannels();
		} catch (Exception ex) {
			ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
			return ;
		}
	}
	
	public String[] getChannels() {
		return channels;
	}
	
	public void setChannels(String[] channels) {
		this.channels = channels ;
	}
	
	public List<SelectOption> getBoards() {
		List<SelectOption> options = new List<SelectOption>();
		postedJobBoardStatus = new Set<String>();
		Map<String, String> postedBoards = new Map<String, String>();
		Map<String, BroadBeanJobPostingUtils.ChannelStatusWrapper> channelStatusWrapperByChannelIdMap =
				advertWrapper.channelStatusWrapperByChannelIdMap;
		
		if (channelStatusWrapperByChannelIdMap != null && channelStatusWrapperByChannelIdMap.size() > 0) { //vishwajeet,Case#:00059107
			System.debug('channelStatusWrapperByChannelIdMap -- ' + channelStatusWrapperByChannelIdMap);
			System.debug('channelStatusWrapperByChannelIdMap.values() -- ' + channelStatusWrapperByChannelIdMap.values());
			
			for (BroadBeanJobPostingUtils.ChannelStatusWrapper channelData : channelStatusWrapperByChannelIdMap.values()) {
				//added by vk,03/08/2017
				String boardCurrentStatus = channelData.channelStatus;
				if (String.isNotEmpty(boardCurrentStatus)
						&& BroadBeanJobPostingUtils.postedJobBBStatusList.contains(boardCurrentStatus.toLowerCase())) { //add only if board status is still delivered
					System.debug('channelData ---' + channelData);
					System.debug('channelData.channelName ---' + channelData.channelName);
					System.debug('channelData.channelStatus ---' + channelData.channelStatus);
					postedJobBoardStatus.add(channelData.channelName + ' (' + channelData.channelStatus + ' )');
					System.debug('postedJobBoardStatus ----' + postedJobBoardStatus);
					postedBoards.put(channelData.channelId, channelData.channelName + ' (' + channelData.channelStatus + ' )');
				}
			
			}
			System.debug('postedBoards ----' + postedBoards);
			// for(String jobBoard : postedBoards ){
			for (String jobBoard : postedBoards.keySet()) {
				options.add(new SelectOption(jobBoard, postedBoards.get(jobBoard)));
			}
		}
		
		System.debug('options ----' + options);
		return options;
	}
	
	/**
* @description Method for posting to broad bean for open job record. 
* This will display only unposted channels for posting. 
* This method is called from BroadbeanJobPosting VF page.
**/
	public PageReference exportJobPosting() {
		PageReference ref = null;
		try {
			if (String.isBlank(selectedJobId)) {
				throw new BroadBeanJobPostingUtils.BroadbeanJobPostingException(system.Label.BB_Posting_No_Job_Selected_Error);
			}
			AVTRRT__Job__c job = BroadBeanJobPostingUtils.getJobDetailById(selectedJobId);
			
			// Validate the job status. if equals to Closed, return from
			// method with error message to show on UI
			if (job != null && job.AVTRRT__Closed__c) {
				
				// Add error message to show on UI
				ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, system.Label.BB_Posting_Closed_Job_Selected_Error));
				return ref;
			}
			
			// getting a wrapper instance with Broadbean connection inputs
			BroadBeanJobPostingUtils.BroadBeanConnectionInfoWrapper connectionInfo = BroadBeanJobPostingUtils.getBroadBeanConnectionInfo();
			
			// Getting set of channel ids to whom job is not yet posted i.e unposted
			Set<String> unPostedJobBoardIdSet = BroadBeanJobPostingUtils.getUnPostedJobBoards(connectionInfo, job.Name);
			System.debug('unPostedJobBoardIdSet --- ' + unPostedJobBoardIdSet);
			
			// If unPostedJobBoardIdSet is null or empty that mean given job is posted to all available channels.
			if (unPostedJobBoardIdSet == null || unPostedJobBoardIdSet.isEmpty()) {
				
				// Add error message to show on UI
				ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, system.Label.BB_Posting_Error_Job_has_been_posted_to_all_available_Boards));
				return ref;
			}
			
			String jobURL = BroadBeanJobPostingUtils.exportJobPostings(connectionInfo, job, unPostedJobBoardIdSet);
			//BroadBeanJobPostingUtils.updateJobBroadCastObject(connectionInfo, job, 'posting');
			System.debug(LoggingLevel.ERROR, 'jobURL -- ' + jobURL);
			if (String.isNotBlank(jobURL)) {
				ref = new PageReference('' + jobURL);
				ref.setRedirect(true);
				//BroadBeanJobPostingUtils.updateJobBroadCastObject(connectionInfo, job);
			}
		} catch (Exception e) {
			ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, String.format(system.Label.BB_Posting_Error_Problem_occurred_while_posting_the_Job, new List<String> {e.getMessage()})));
		}
		return ref;
	}
	
	/**
* @description Method for deleting the existing adverts posted (if any) from the broadbean 
* for selected job. Validation is added to check if any existing. 
* This method is called from ManageDeleteFromBroadbean VF page.
**/
	
	public void deleteFromBroadBean() {
		PageReference ref = null;
		
		String[] jobBoardToDelete = new String[] {};
		jobBoardToDelete = getChannels();
		System.debug('jobBoardToDelete  ---' + jobBoardToDelete);
		
		if (jobBoardToDelete.size() == 0) {
			ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, 'please Select atleast one job board to delete.'));
			return;
		}
		
		try {
			// getting a wrapper instance with Broadbean connection inputs
			/*BroadBeanJobPostingUtils.BroadBeanConnectionInfoWrapper connectionInfo =
				BroadBeanJobPostingUtils.getBroadBeanConnectionInfo();
			
			AVTRRT__Job__c job = BroadBeanJobPostingUtils.getJobDetailById(selectedJobId);*/
			// Getting status for all posted job boards
			advertWrapper = BroadBeanJobPostingUtils.getJobBoardStatusCheck(connectionInfo, currentJob.Name);
			// Getting set of channel ids to whom job is not yet posted i.e unposted
			//postedJobBoardIdSet = BroadBeanJobPostingUtils.getUnPostedJobBoards(connectionInfo, job.Name);--vk,03/10/2017
			postedJobBoardIdSet = BroadBeanJobPostingUtils.getPostedJobBoardsV1(connectionInfo, currentJob.Name, advertWrapper);
			//vk,4/23/2018 postedJobBoardIdSet = BroadBeanJobPostingUtils.getPostedJobBoards(connectionInfo, currentJob.Name);
			System.debug('postedJobBoardIdSet --- ' + postedJobBoardIdSet);
			//System.debug('getChannels ()  in deleteFromBroadBean --- '+ getChannels());
			
			// If postedJobBoardIdSet is null or empty that mean given job is posted to all available channels.
			if (postedJobBoardIdSet == null || postedJobBoardIdSet.isEmpty()) {
				// Add error message to show on UI
				ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, system.Label.BB_Posting_Job_is_not_currently_posted_on_any_job_board));
				return;
			}
			
			// channelStatusWrapperByChannelIdMap from advertWrapper
			// Map<String, BroadBeanJobPostingUtils.ChannelStatusWrapper> channelStatusWrapperByChannelIdMap = advertWrapper.channelStatusWrapperByChannelIdMap;
			
			//if(channelStatusWrapperByChannelIdMap.isEmpty()) {
			//Add error message to show on UI
			//Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, system.label.BB_Posting_Job_is_not_currently_posted_on_any_job_board));
			//return;
			// }
			
			Boolean isSuccessFullyDeleted = BroadBeanJobPostingUtils.deleteAdvert(connectionInfo, currentJob.Name, jobBoardToDelete);
			// Add success info message to show on UI on successful delete of adverts
			if (isSuccessFullyDeleted) {
				
				ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.INFO, system.Label.BB_Posting_Info_successfully_removed_this_requisition_from_all_job_boards));
				
				// If job posting are deleted successfully. Update the Posted_to_Broadbean__c field and set it false if it is true.
				/*vk,4/23/2018
				 * if(currentJob.Posted_to_Broadbean__c) {
					system.debug('job.Posted_to_Broadbean__c false-- ' + currentJob.Posted_to_Broadbean__c );
					//job.Posted_to_Broadbean__c = false;
					update currentJob;
				}*/
				
				BroadBeanJobPostingUtils.updateJobBroadCastObject(connectionInfo, currentJob, 'delete', advertWrapper, jobBoardToDelete);
			}
		} catch (Exception e) {
			ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, String.format(system.Label.BB_Posting_Error_Problem_occurred_while_deleting_from_Broadbean, new List<String> {e.getMessage()})));
		}
	}
	
	/**
* @description Method for loading the status for all advert posts done for a job record. 
* The status details are shown as inline Vf on job details record. 
* This method is called from ShowBroadbeanJobStatus VF page.
**/
	
	public void loadCurrentBroadbeanJobStatus() {
		
		try {
			// getting a wrapper instance with Broadbean connection inputs
			BroadBeanJobPostingUtils.BroadBeanConnectionInfoWrapper connectionInfo = BroadBeanJobPostingUtils.getBroadBeanConnectionInfo();
			AVTRRT__Job__c job = BroadBeanJobPostingUtils.getJobDetailById(selectedJobId);
			// Getting status for all posted job boards
			System.debug(LoggingLevel.ERROR, 'loadCurrentBroadbeanJobStatus-before:: ' + advertWrapper);
			advertWrapper = BroadBeanJobPostingUtils.getJobBoardStatusCheck(connectionInfo, job.Name);
			System.debug(LoggingLevel.ERROR, 'loadCurrentBroadbeanJobStatus-after::' + advertWrapper);
			// channelStatusWrapperByChannelIdMap from advertWrapper
			Map<String, BroadBeanJobPostingUtils.ChannelStatusWrapper> channelStatusWrapperByChannelIdMap;
			if (advertWrapper != null && advertWrapper.Id != null) {
				channelStatusWrapperByChannelIdMap = advertWrapper.channelStatusWrapperByChannelIdMap;
				Map<String, BCST__Job_Broadcast__c> jobIdJobBroadcastObjectMap = new Map<String, BCST__Job_Broadcast__c>();
				channelStatusWrapperList = channelStatusWrapperByChannelIdMap.values();
				System.debug('channelStatusWrapperList ' + channelStatusWrapperList);
				String[] jobBoardToDelete = new String[] {};
				jobBoardToDelete = getChannels();
				System.debug('jobBoardToDelete  ' + jobBoardToDelete);
				
				//If there is entry for job posting. Update the Posted_to_Broadbean__c field and set it true if it is false.
				// if(channelStatusWrapperList != null && !channelStatusWrapperList.isEmpty() )// && !job.Posted_to_Broadbean__c) {
				
				if (channelStatusWrapperList.size() > 0) {
					System.debug('job.Posted_to_Broadbean__c -- ' + job.Posted_to_Broadbean__c);
					job.Posted_to_Broadbean__c = true;
					update job;
					BroadBeanJobPostingUtils.updateJobBroadCastObject(connectionInfo, job, 'posting', advertWrapper, jobBoardToDelete);
				
				}
				// String xmlInputString = BroadBeanJobPostingUtils.getRetrieveApplicationsXMLInputString(connectionInfo, job.id);
				//System.debug('xmlInputString Retrieve Applications' +xmlInputString);
				
				//retrieveApplications();
			} else { //if request falied,VK,4/23/2018
				if (advertWrapper.isFailed) {
					String v_ErrSt = 'Error From BroadBean::';
					if (String.isNotEmpty(advertWrapper.failedMessage)) {
						v_ErrSt += advertWrapper.failedMessage;
					}
					
					ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, v_ErrSt));
				
				}
			}
		} catch (Exception e) {
			ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, String.format(system.Label.BB_Posting_Error_Problem_occurred_while_loading_Job_Broadbean_Status, new List<String> {e.getMessage()})));
		}
	}

}