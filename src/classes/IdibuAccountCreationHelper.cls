/**
* @author Shashish kumar
* @date 28/06/2013
* @description Class usued for creating Idibu Exempt Account, adding services to Account and adding User..
*/
public class IdibuAccountCreationHelper {
	
	/**
	* @author Shashish kumar
	* @date 28/06/2013
	* @param  p_userCredentialMap User information map Object
	* @description Creates An Xml in formate specified by Idibu
	* @return Returns an Xml String of format :
	  <?xml version="1.0"?>
	  	<idibu>
	  		<firstname>Firstname</firstname>
	  		<lastname>Lastname</lastname>
	  		<email>email@test.com</email>
	  		<username>user2</username>
	 	 	<password>pas123</password>
	  		<company-name>company</company-name>
	 	 	<phone>123465</phone>
	  		<referred-by>n/a</referred-by>
	  		<partner-password>S0meP4rtn3rP4ssw0rd1sH3r3</partner-password>
	  		<user-number>1</user-number>
	  		<renewal-date>2020-01-01</renewal-date>
	  		<exempt-id>1</exempt-id>
		</idibu> 
	 
	*/
	
	public static String createAccountCreationXml(Map<String, String> p_userCredentialMap) {
		Dom.Document document = new Dom.Document();
		Dom.XmlNode rootNode = document.createRootElement('idibu', null, null);
		Dom.XmlNode firstNameNode = rootNode.addChildElement('firstname', null, null);
		Dom.XmlNode lastNameNode = rootNode.addChildElement('lastname', null, null);
		Dom.XmlNode emailNode = rootNode.addChildElement('email', null, null);
		Dom.XmlNode userNameNode = rootNode.addChildElement('username', null, null);
		Dom.XmlNode passwordNode = rootNode.addChildElement('password', null, null);
		Dom.XmlNode CompanyNameNode = rootNode.addChildElement('company-name', null, null);
		Dom.XmlNode phoneNode = rootNode.addChildElement('phone', null, null);
		Dom.XmlNode referredByNode = rootNode.addChildElement('referred-by', null, null).addTextNode('N/A');
		Dom.XmlNode partnerPasswordNode = rootNode.addChildElement('partner-password', null, null).addTextNode('4v4nk1a2012');
		
		//Added by Vishwajeet,6/8/2017, increasing number of user to 1000.
		Dom.XmlNode userNumberNode = rootNode.addChildElement('user-number', null, null).addTextNode('1000');
		Dom.XmlNode renewalDateNode = rootNode.addChildElement('renewal-date', null, null).addTextNode('2020-05-05');
		Dom.XmlNode exemptIdNode = rootNode.addChildElement('exempt-id', null, null).addTextNode('14');
		if (p_userCredentialMap.get('firstName') != null) {
			firstNameNode.addTextNode(p_userCredentialMap.get('firstName'));
		}
		if (p_userCredentialMap.get('lastName') != null) {
			lastNameNode.addTextNode(p_userCredentialMap.get('lastName'));
		}
		if (p_userCredentialMap.get('email') != null) {
			emailNode.addTextNode(p_userCredentialMap.get('email'));
		}
		if (p_userCredentialMap.get('username') != null) {
			userNameNode.addTextNode(p_userCredentialMap.get('username'));
		}
		if (p_userCredentialMap.get('password') != null) {
			passwordNode.addTextNode(p_userCredentialMap.get('password'));
		}
		System.debug(p_userCredentialMap.get('company-name') + '....p_userCredentialMap.....' + p_userCredentialMap.get('phone'));
		if (p_userCredentialMap.get('company-name') != null) {
			CompanyNameNode.addTextNode(p_userCredentialMap.get('company-name'));
		}
		if (p_userCredentialMap.get('phone') != null) {
			phoneNode.addTextNode(p_userCredentialMap.get('phone'));
		}
		System.debug('Account Creation Xml : ' + document.toXmlString());
		return document.toXmlString();
	}
	
	/**
	* @author Vishwajeet
	* @date 22/02/2013
	* @param p_clientIdibuHash Idibu Account Hash Value.
	* @description  Creates An Xml to add Aptrack Services to account. 
	* @return Returns an Xml String of format :
	  	<?xml version="1.0"?>
			<idibu>
 		 		<partner-password>123</partner-password>
  				<exempt-id>0</exempt-id>
  				<client-hash>373ed1cdfac5168e61d6dcea98d48f56</client-hash>
  				<services>
     		 		<service>aptrack</service>
  				</services>
			</idibu>
	*/
	
	public static String createAptrackServicesXml(String p_clientIdibuHash) {
		Dom.Document document = new Dom.Document();
		Dom.XmlNode rootNode = document.createRootElement('idibu', null, null);
		Dom.XmlNode partnerPasswordNode = rootNode.addChildElement('partner-password', null, null).addTextNode('4v4nk1a2012');
		Dom.XmlNode exemptIdNode = rootNode.addChildElement('exempt-id', null, null).addTextNode('14');
		Dom.XmlNode clientHashNode = rootNode.addChildElement('client-hash', null, null);
		Dom.XmlNode servicesNode = rootNode.addChildElement('services', null, null);
		servicesNode.addChildElement('service', null, null).addTextNode('aptrack');
		if (p_clientIdibuHash != null) {
			clientHashNode.addTextNode(p_clientIdibuHash);
		}
		System.debug('Aptrack Services Xml :' + document.toXmlString());
		return document.toXmlString();
	}
	
	/**
	* @author Pandeiswari
	* @date 03/08/2014
	* @param p_clientIdibuHash Idibu Account Hash Value.
	* @param p_aptrackEmail Aptrack Email id from Email service
	* @description  Creates An Xml to add Global cc email to idibu account. 
	* @return Returns an Xml String of format :
	  	<?xml version="1.0"?>
			<idibu>
 		 		<partner-password>123</partner-password>
  				<exempt-id>0</exempt-id>
  				<client-hash>373ed1cdfac5168e61d6dcea98d48f56</client-hash>
  				<cc-email>ccemail@just.hr</cc-email>
			</idibu>
	*/
	
	public static String createGlobalCCServicesXml(String p_clientIdibuHash, String p_aptrackEmail) {
		Dom.Document document = new Dom.Document();
		Dom.XmlNode rootNode = document.createRootElement('idibu', null, null);
		Dom.XmlNode partnerPasswordNode = rootNode.addChildElement('partner-password', null, null).addTextNode('4v4nk1a2012');
		Dom.XmlNode exemptIdNode = rootNode.addChildElement('exempt-id', null, null).addTextNode('14');
		Dom.XmlNode clientHashNode = rootNode.addChildElement('client-hash', null, null);
		Dom.XmlNode emailCCNode = rootNode.addChildElement('cc-email', null, null);
		if (p_clientIdibuHash != null) {
			clientHashNode.addTextNode(p_clientIdibuHash);
		}
		if (p_aptrackEmail != null) {
			emailCCNode.addTextNode(p_aptrackEmail);
		}
		System.debug('Global CCServices Xml:' + document.toXmlString());
		return document.toXmlString();
	}
	
	/**
	* @author Shashish Kumar
	* @date 27/06/2013
	* @param p_userCredentialMap User information map object
	* @description Creates An Xml to crate a new user. 
	* @return Returns an Xml String of format :
	  	<?xml version="1.0"?>
			<idibu>
    			<profile>
            		<firstname>John</firstname>
            		<lastname>Doe</lastname>
            		<company>Company</company>
            		<contacts>
            			<address>Far away 1234</address>
               			<address-line1>Far away</address-line1>
               			<address-line2></address-line2>
               			<address-line3></address-line3>
               			<country>Far away</country>
               			<postcode>7777</postcode>
               			<email>tescior@tescior.pl</email>
               			<phone>+777 0 777 77777</phone>
               			<fax></fax>
               			<www>www.page.com</www>
            		</contacts>
    			</profile>
    			//Auth not required : commented by Shashish
    			<auth>
        			<disabled>No</disabled>
        			<login>newuser4</login>
        			<password>somepwd4</password>
    			</auth>
		</idibu>

	 */
	public static String createUserInfoXml(Map<String, String> p_userCredentialMap) {
		Dom.Document document = new Dom.Document();
		Dom.XmlNode rootNode = document.createRootElement('idibu', null, null);
		
		Dom.XmlNode profileNode = rootNode.addChildElement('profile', null, null);
		//Dom.Xmlnode titleNode = profileNode.addChildElement('title', null, null).addTextNode('');
		Dom.XmlNode firstNameNode = profileNode.addChildElement('firstname', null, null);
		Dom.XmlNode lastNameNode = profileNode.addChildElement('lastname', null, null);
		Dom.XmlNode CompanyNode = profileNode.addChildElement('company', null, null);
		
		Dom.XmlNode contactsNode = profileNode.addChildElement('contacts', null, null);
		Dom.XmlNode addressNode = contactsNode.addChildElement('address', null, null);
		Dom.XmlNode addressLine1Node = contactsNode.addChildElement('address-line1', null, null);
		Dom.XmlNode addressLine2Node = contactsNode.addChildElement('address-line2', null, null);
		Dom.XmlNode addressLine3Node = contactsNode.addChildElement('address-line3', null, null);
		Dom.XmlNode countryNode = contactsNode.addChildElement('country', null, null);
		Dom.XmlNode postcodeNode = contactsNode.addChildElement('postcode', null, null);
		Dom.XmlNode emailNode = contactsNode.addChildElement('email', null, null);
		Dom.XmlNode phoneNode = contactsNode.addChildElement('phone', null, null);
		Dom.XmlNode faxNode = contactsNode.addChildElement('fax', null, null);
		Dom.XmlNode wwwNode = contactsNode.addChildElement('www', null, null);
		
		/*Auth not required : commented by Shashish
		Dom.Xmlnode authNode = rootNode.addChildElement('auth', null, null);
		Dom.Xmlnode disabledNode = authNode.addChildElement('disabled', null, null).addTextNode('No');
		Dom.Xmlnode loginNode = authNode.addChildElement('login', null, null);
		Dom.Xmlnode passwordNode = authNode.addChildElement('password', null, null);
		*/
		
		if (p_userCredentialMap.get('firstName') != null) {
			firstNameNode.addTextNode(p_userCredentialMap.get('firstName'));
		}
		if (p_userCredentialMap.get('lastName') != null) {
			lastNameNode.addTextNode(p_userCredentialMap.get('lastName'));
		}
		if (p_userCredentialMap.get('company') != null) {
			CompanyNode.addTextNode(p_userCredentialMap.get('company'));
		}
		
		if (p_userCredentialMap.get('address') != null) {
			addressNode.addTextNode(p_userCredentialMap.get('address'));
		}
		if (p_userCredentialMap.get('addressLine1') != null) {
			addressLine1Node.addTextNode(p_userCredentialMap.get('addressLine1'));
		}
		if (p_userCredentialMap.get('addressLine2') != null) {
			addressLine2Node.addTextNode(p_userCredentialMap.get('addressLine2'));
		}
		if (p_userCredentialMap.get('addressLine3') != null) {
			addressLine3Node.addTextNode(p_userCredentialMap.get('addressLine3'));
		}
		if (p_userCredentialMap.get('country') != null) {
			countryNode.addTextNode(p_userCredentialMap.get('country'));
		}
		if (p_userCredentialMap.get('postcode') != null) {
			postcodeNode.addTextNode(p_userCredentialMap.get('postcode'));
		}
		if (p_userCredentialMap.get('email') != null) {
			emailNode.addTextNode(p_userCredentialMap.get('email'));
		}
		if (p_userCredentialMap.get('phone') != null) {
			phoneNode.addTextNode(p_userCredentialMap.get('phone'));
		}
		if (p_userCredentialMap.get('www') != null) {
			wwwNode.addTextNode(p_userCredentialMap.get('www'));
		}
		/*if(p_userCredentialMap.get('login')!= null){
			loginNode.addTextNode(p_userCredentialMap.get('login'));
		}
		if(p_userCredentialMap.get('password')!= null){
			passwordNode.addTextNode(p_userCredentialMap.get('password'));
		}*/
		System.debug('User Creation Xml : ' + document.toXmlString());
		return document.toXmlString();
	}
	
	/**
	* @author Shashish Kumar
	* @date 27/06/2013
	* @param  p_xmlString XML string value
	* @param  p_requestTo requestTo value(which type of request like : to create account, add user or add services)
	* @description  Method helps to call Http request to create "Idibu Account" or "Idibu User" or "add Services".
	* @return Returns an Http response String.
	*/
	public static String callHttpRequest(String p_xmlString, String p_requestTo) {
		System.debug(p_xmlString + '.......xmlString.......');
		String v_AccResponse = '';
		String url = '';
		Map<String, String> accResponseMap = new Map<String, String>();
		if (p_requestTo != null && p_requestTo == 'CreateAccount') {
			url = 'http://ws.idibu.com/ws/rest/v1/service/newaccount?idibupartner=yes';
		} else if (p_requestTo.contains('AddUser')) {//if(p_requestTo == 'AddUser'){
			String hash = p_requestTo.removeStart('AddUser');
			url = 'http://ws.idibu.com/ws/rest/v1/users/new?hash=' + hash;
			System.debug('..........User URL .....' + url);
		} else if (p_requestTo.contains('AddGlobalCC')) {
			url = 'http://ws.idibu.com/ws/rest/v1/service/setccemail?idibupartner=yes';
		} else {
			url = 'http://ws.idibu.com/ws/rest/v1/service/add?idibupartner=yes';
		}
		Http http = new Http();
		
		HttpRequest httpRequest = new HttpRequest();
		HttpResponse httpResponse = new HttpResponse();
		httpRequest.setHeader('content-type', 'application/x-www-form-urlencoded');
		Integer timeout = 120000;
		httpRequest.setMethod('POST');
		httpRequest.setTimeout(timeout);
		httpRequest.setEndpoint(url);
		httpRequest.setBody('&data=' + p_xmlString);
		
		if (Test.isRunningTest()) {
			if (p_requestTo == 'CreateAccount') {
				v_AccResponse = '<?xml version="1.0" encoding="utf8"?><idibu generator="idibu" version="1.0"><response><message>New client successfully created</message><client-hash>373ed1cdfac5168e61d6dcea98d48f56</client-hash><client-id>1000916</client-id><member-id>1546</member-id></response><status>success</status></idibu>';
			} else if (p_requestTo.contains('AddUser')) {
				v_AccResponse = '<?xml version="1.0" encoding="utf8"?><idibu generator="idibu" version="1.0"><response><message>User added</message><id>1198</id></response><status>success</status></idibu>';
			} else if (p_requestTo.contains('AddGlobalCC')) {
				v_AccResponse = '<?xml version="1.0" encoding="utf8"?><idibu generator="idibu" version="1.0"><response><message>Client`s central CC email set</message><cc-email>ccemail@just.hr</cc-email></response><status>success</status></idibu>';
			} else {
				v_AccResponse = '<?xml version="1.0" encoding="utf8"?><idibu generator="idibu" version="1.0"><response><message>Services added</message></response><status>success</status></idibu>';
			}
		
		} else {
			httpResponse = http.send(httpRequest);
			v_AccResponse = httpResponse.getBody();
		}
		return v_AccResponse;
	}
}