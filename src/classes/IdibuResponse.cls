public with sharing class IdibuResponse {

	public String job_status { get; set; }
	public String job_id { get; set; }
	public String job_postingid { get; set; }
	public String job_client_reference { get; set; }
	public String postcompletionurl { get; set; }
	public List<String> errorsList { get; set; }
	public List<String> warningsList { get; set; }
	public List<posts> postsList { get; set; }

	public IdibuResponse() {
	}

	public class posts {
		/*Changed by vishwajeet,Date-08/20/2013,Case#:00020694,Added variable(posts_warningString,posts_errorString)*/
		public String posts_status { get; set; }
		public String posts_boardid { get; set; }
		public String posts_queueid { get; set; }
		public String posts_type { get; set; }
		public String posts_publish { get; set; }
		public Date posts_endDate { get; set; }
		public String posts_duration { get; set; }
		public String posts_remoteid { get; set; }
		public String posts_errorString { get; set; }
		public String posts_warningString { get; set; }
		public String innerText { get; set; }
	}
}