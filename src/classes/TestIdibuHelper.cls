@IsTest
private class TestIdibuHelper {

	private static testMethod void testGetIdibuLocationSelectOptionList() {
		System.assertEquals(6, IdibuHelper.getIdibuLocationSelectOptionList(false).size());
	}

	private static testMethod void testCreateJobBoardXMLDocument() {
		String v_idibuFolderId = IdibuHelper.getIdibuFolderId();

		Document v_document = new Document(
				Name = 'board_id-board_name(' + String.valueOf(Date.today()) + ').xml',
				Body = Blob.valueOf('Some Body'),
				FolderId = v_idibuFolderId
		);

		Document v_documentResult =
				IdibuHelper.createJobBoardXMLDocument('board_id-board_name', 'Some Body', v_idibuFolderId);

		System.assertEquals(v_document.Name, v_documentResult.Name);
	}

	private static testMethod void testGetHashFromCustomSettings() {
		Job_Broadcasting_Settings__c v_jobBroadcastingSettings = Job_Broadcasting_Settings__c.getInstance();
		v_jobBroadcastingSettings.Idibu_Hash__c = 'b39122f4ffd1defb89cb5f202f4417a2';
		v_jobBroadcastingSettings.BCST__Idibu_User_Id__c ='1222';
		/* Edited by Aliaksandr Satskou, 03/14/2013 */
		upsert v_jobBroadcastingSettings;

		System.assertEquals('b39122f4ffd1defb89cb5f202f4417a2', IdibuHelper.getHashFromCustomSettings());
	}

	private static testMethod void testGetUserIdFromCustomSettings() {
		Job_Broadcasting_Settings__c v_jobBroadcastingSettings = Job_Broadcasting_Settings__c.getInstance();
		v_jobBroadcastingSettings.Idibu_User_Id__c = '31000602';
		/* Edited by Aliaksandr Satskou, 03/14/2013 */
		upsert v_jobBroadcastingSettings;

		System.assertEquals('31000602', IdibuHelper.getUserIdFromCustomSettings());
	}

	private static testMethod void testPostingJobToIdibu() {
		String v_responseXML = '<?xml version="1.0" encoding="UTF-8" ?><idibu><job status="added" ' +
				'id="00000008"><posts><posts status="pending" boardid="517" queueid="0000000" type="add" ' +
				'publish="2011-10-21 00:01" duration="2" /></posts></job><log><warnings><warning>' +
				'Warning_Some_Text</warning></warnings><errors><error>Error_Some_Text</error>' +
				'</errors></log></idibu>';
		System.assertEquals(v_responseXML, IdibuHelper.postingJobToIdibu('Hash', 'XML'));
	}

	private static testMethod void testGetJobBoardsFromIdibu() {
		String v_responseXML = '<?xml version="1.0" encoding="ISO8859-1" ?><idibu><boards><board id="517" ' +
				'last_modified="2010-01-14 00:00:00" published="true" allows_html="true"><name>' +
				'idibu Developer Board</name><description><![CDATA[Some_Description]]>' +
				'</description><tags><tag>general</tag></tags></board></boards></idibu>';
		System.assertEquals(v_responseXML, IdibuHelper.getJobBoardsFromIdibu('Hash'));
	}

	private static testMethod void testParseJobBoardsXML() {

		IdibuJobBoard v_idibuJobBoard = new IdibuJobBoard();
		v_idibuJobBoard.board_id = '517';
		v_idibuJobBoard.description = '';
		v_idibuJobBoard.name = 'idibu Developer Board';
		v_idibuJobBoard.tags = new List<String> {'general'};

		String v_jobBoardsXML = '<?xml version="1.0" encoding="ISO8859-1" ?><idibu><boards><board id="517" ' +
				'last_modified="2010-01-14 00:00:00" published="true" allows_html="true"><name>' +
				'idibu Developer Board</name><description><![CDATA[Some_Description]]>' +
				'</description><tags><tag>general</tag></tags></board></boards></idibu>';

		List<IdibuJobBoard> v_idibuJobBoardList = IdibuHelper.parseJobBoardsXML(v_jobBoardsXML);
		EncodeUtil.KeywordsEncode('\\test');
	}

	private static testMethod void testCreatePostingXML() {
		createCustomSettings();
		
		AVTRRT__Job__c v_job = new AVTRRT__Job__c(
				AVTRRT__Job_Title__c = 'Java developer',
				AVTRRT__Job_Description__c = 'Java developer description',
				AVTRRT__Job_Description_Rich__c = 'Java developer description rich',
				AVTRRT__Start_Date__c = Date.valueOf('2011-10-21'),
				AVTRRT__Job_Term__c = 'Permanent',
				AVTRRT__Experience__c = '01-03 Years',
				AVTRRT__Salary_Range__c = '25000.0 - 35000.0',
				AVTRRT__Country_Locale__c = 'India',
				Idibu_Salary_Range__c = '25000.0 - 35000.0',
				Idibu_Job_Type__c = '3',
				Idibu_Location__c = '37,1',
				Idibu_Category__c = '18',
				Idibu_Job_Time__c = '2'
		);
		insert v_job;
		System.assertNotEquals(null, v_job.Id);

		List<BCST__Job_Broadcast__c> v_jobBroadcastList = new List<BCST__Job_Broadcast__c> {
				new BCST__Job_Broadcast__c(
						BCST__Idibu_Job_Id__c = '000000001',
						BCST__Job_Board_Id__c = '999',
						BCST__Job_Board_Name__c = 'idibu Developer Board',
						BCST__Jobs__c = v_job.Id
				),
				new BCST__Job_Broadcast__c(
						BCST__Idibu_Job_Id__c = '000000002',
						BCST__Job_Board_Id__c = '999',
						BCST__Job_Board_Name__c = 'idibu Developer Board',
						BCST__Jobs__c = v_job.Id
				)
		};
		insert v_jobBroadcastList;

		Map<String, List<String>> v_mapOfPostingJobs = new Map<String, List<String>>();
		v_mapOfPostingJobs.put(v_job.Id, new List<String> {'999'});

		IdibuJobBoard v_idibuJobBoard = new IdibuJobBoard();
		v_idibuJobBoard.board_id = '999';
		v_idibuJobBoard.description = 'idibu Developer Board description';
		v_idibuJobBoard.name = 'idibu Developer Board';

		v_idibuJobBoard.tags = new List<String>();

		v_idibuJobBoard.durationMap = new Map<String, String>();
		v_idibuJobBoard.durationMap.put('7', '1');
		v_idibuJobBoard.durationMap.put('14', '2');
		v_idibuJobBoard.durationMap.put('21', '3');
		v_idibuJobBoard.durationMap.put('28', '4');

		IdibuJobBoard.Extrafield v_jobBoardExtrafield = new IdibuJobBoard.Extrafield();
		v_jobBoardExtrafield.extrafield_description = 'description';
		v_jobBoardExtrafield.extrafield_multi = 'false';
		v_jobBoardExtrafield.extrafield_name = 'name';
		v_jobBoardExtrafield.extrafield_order = '1';
		v_jobBoardExtrafield.extrafield_required = null;
		v_jobBoardExtrafield.extrafield_type = 'select';
		v_jobBoardExtrafield.isDependentLists = true;
		v_jobBoardExtrafield.extrafieldOptionMap = new Map<String, List<IdibuJobBoard.ExtrafieldOption>>();
		v_jobBoardExtrafield.parentIdParentNameMap = new Map<String, String>();

		v_idibuJobBoard.extrafieldsList = new List<IdibuJobBoard.Extrafield> {v_jobBoardExtrafield};

		List<IdibuJobBoard> v_idibuJobBoardList = new List<IdibuJobBoard> {v_idibuJobBoard};

		List<BCST__JobBroadcasting_Field_Mapping__c> v_mappingList =
				new List<BCST__JobBroadcasting_Field_Mapping__c> {
						new BCST__JobBroadcasting_Field_Mapping__c(
								BCST__Extrafield_Name__c = 'Idibu_Board_Duration-999',
								BCST__Mapped_Formula__c = '7'
						)
				};
		insert v_mappingList;

		Map<AVTRRT__Job__c, List<String>> v_jobIdXMLListMap =
				IdibuHelper.createPostingXML(v_mapOfPostingJobs, v_idibuJobBoardList, true);
	}

	private static testMethod void testParseExtrafieldsJobBoardDataXML() {
		String v_jobBoardXML = '<?xml version="1.0" encoding="ISO8859-1" ?><idibu><boards>' +
				'<board id="517" last_modified="2010-01-14 00:00:00" published="true" ' +
				'supports_delete="Yes"><durations><duration value="1">7</duration><duration value="2">' +
				'14</duration></durations><extrafields><extrafield name="ex1" type="text"/>' +
				'<extrafield name="ex2" description="ex2_description" type="select" required="true">' +
				'<data><option>Please Select...</option><option id="1">Programmers</option>' +
				'<option id="5">Testers</option><option id="3">Administrators</option></data>' +
				'</extrafield></extrafields></board></boards></idibu>';

		v_jobBoardXML = v_jobBoardXML.replace('\'', '');
		v_jobBoardXML = v_jobBoardXML.replace('`', '');

		Dom.Document v_document = new Dom.Document();
		v_document.load(v_jobBoardXML);
		List<IdibuJobBoard.Extrafield> v_extrafieldList = IdibuHelper.parseExtrafieldsJobBoardDataXML(v_document);
		//List<IdibuJobBoard.extrafield> v_extrafieldList = IdibuHelper.parseExtrafieldsJobBoardDataXML(v_jobBoardXML);
		System.assertEquals(2, v_extrafieldList.size());
	}

	private static testMethod void testgetListOfLocation() {
		BCST__JobBroadcastingCountryCodeSettings__c country = new BCST__JobBroadcastingCountryCodeSettings__c(
				Name = 'US', BCST__Country_Name__c = 'USA,US', BCST__Country_Code__c = 'US');
		insert country;

		System.debug('country_______' + country);

		//List<SelectOption> listOfLocations = IdibuHelper.getListOfLocation('US','Chicago','Illionis');--Commented by VK,03/22/2017
		List<SelectOption> listOfLocations = IdibuHelper.getListOfLocation('US', 'Chicago');
		Map<String, String> countryCodeMap = new Map<String, String>();
		countryCodeMap.put('US', 'US');

		System.debug('countryCodeMap_____Start' + countryCodeMap);

		//--Commented by VK,03/22/2017List<String> firstLocation = IdibuHelper.getFirstLocation('US', 'Chicago','Illionis', countryCodeMap);
		List<String> firstLocation = IdibuHelper.getFirstLocation('US', 'Chicago', countryCodeMap);

		Dom.Document v_document = new Dom.Document();
		String locationresponse = '<idibu generator="idibu" version="1.0"><response><locations><location><id>5001420</id><place_name>Nashville</place_name><country_code>US</country_code><postal_code>71852</postal_code><latitude>33.96</latitude><longitude>-93.87</longitude><state>Arkansas</state><country>United States</country></location></locations></response></idibu>';
		v_document.load(locationresponse);

		System.debug('v_document_____Start' + v_document);

		//IdibuHelper.buildLocationDomXML(v_document,'Illionis');--Commented by VK,03/22/2017
		IdibuHelper.buildLocationDomXML(v_document, 'US');
		IdibuHelper.encodeJobDescription('Description');
		//IdibuHelper.removeLocationExtraFieldFromJobBoardXml('facebook','<boards><board><extrafields></extrafields></board></boards>','keyword');
	}
	private static testMethod void testParseDurationJobBoardDataXML() {
		String v_jobBoardXML = '<?xml version="1.0" encoding="ISO8859-1" ?><idibu><boards>' +
				'<board id="517" last_modified="2010-01-14 00:00:00" published="true" ' +
				'supports_delete="Yes"><durations><duration value="1">7</duration><duration value="2">' +
				'14</duration></durations><extrafields><extrafield name="ex1" type="text"/>' +
				'<extrafield name="ex2" description="ex2_description" type="select" required="true">' +
				'<data><option>Please Select...</option><option id="1">Programmers</option>' +
				'<option id="5">Testers</option><option id="3">Administrators</option></data>' +
				'</extrafield></extrafields></board></boards></idibu>';

		v_jobBoardXML = v_jobBoardXML.replace('\'', '');
		v_jobBoardXML = v_jobBoardXML.replace('`', '');
		Dom.Document v_document = new Dom.Document();
		v_document.load(v_jobBoardXML);
		Map<String, String> v_durationMap = IdibuHelper.parseDurationJobBoardDataXML(v_document);

		System.assertEquals(2, v_durationMap.size());
	}

	private static testMethod void testGetJobBoardDataFromIdibu() {
		IdibuHelper.getCurrencyList();
		String v_jobBoardDataResultXML = IdibuHelper.getJobBoardDataFromIdibu('517', 'Hash');

		String v_jobBoardDataXML = '<?xml version="1.0" encoding="ISO8859-1" ?><idibu><boards>' +
				'<board id="517" last_modified="2010-01-14 00:00:00" published="true" ' +
				'supports_delete="Yes"><durations><duration value="1">7</duration><duration value="2">' +
				'14</duration></durations><extrafields><extrafield name="ex1" type="text"/>' +
				'<extrafield name="ex2" description="ex2_description" type="select" required="true">' +
				'<data><option>Please Select...</option><option id="1">Programmers</option>' +
				'<option id="5">Testers</option><option id="3">Administrators</option></data>' +
				'</extrafield></extrafields></board></boards></idibu>';

		System.assertEquals(v_jobBoardDataResultXML, v_jobBoardDataXML);
	}

	private static testMethod void testCreateMapOfPostingJobs() {
		String v_idibuFolderId = IdibuHelper.getIdibuFolderId();

		delete [
				SELECT Id
				FROM Document
				WHERE FolderId = :v_idibuFolderId
		];

		List<Document> v_documentList = new List<Document> {
				new Document(
						Name = '0-IdibuJobBoardsXML(' + String.valueOf(Date.today()) + ').xml',
						Body = Blob.valueOf('<idibu><boards><board id="517" last_modified="2010-01-14 00:00:00" ' +
								'published="true"><name>idibu Developer Board</name><description>' +
								'<![CDATA[SomeText]]></description><tags><tag>general</tag></tags>' +
								'</board></boards></idibu>'),
						FolderId = v_idibuFolderId
				),
				new Document(
						Name = '517-idibu Developer Board(' + String.valueOf(Date.today()) + ').xml',
						Body = Blob.valueOf('<?xml version="1.0" encoding="ISO8859-1" ?><idibu><boards>' +
								'<board id="517" last_modified="2010-01-14 00:00:00" published="true" ' +
								'supports_delete="Yes"><durations><duration value="1">7</duration>' +
								'<duration value="2">14</duration></durations><extrafields><extrafield ' +
								'name="ex1" type="text"/><extrafield name="ex2" description="ex2_description" ' +
								'type="select" required="true"><data><option>Please Select...</option>' +
								'<option id="1">Programmers</option><option id="5">Testers</option>' +
								'<option id="3">Administrators</option></data></extrafield></extrafields>' +
								'</board></boards></idibu>'),
						FolderId = v_idibuFolderId
				)
		};
		insert v_documentList;

		List<IdibuJobBoard> v_idibuJobBoardList = IdibuHelper.getIdibuJobBoardList(false);
		List<String> v_checkBoxValuesList = new List<String> {'jobid', '517'};

		System.debug(LoggingLevel.ERROR, ':::::::::::v_idibuJobBoardList=' + v_idibuJobBoardList);

		Map<String, List<String>> v_postingJobsMap =
				IdibuHelper.createMapOfPostingJobs(v_idibuJobBoardList, v_checkBoxValuesList);
		System.assertEquals(1, v_postingJobsMap.size());
	}

	private static testMethod void testGetJobDeleteList() {
		createCustomSettings();
		
		List<AVTRRT__Job__c> v_jobList = new List<AVTRRT__Job__c> {
				new AVTRRT__Job__c(
						AVTRRT__Job_Title__c = 'Job Title #1',
						AVTRRT__Job_Description__c = 'Job Description #1',
						AVTRRT__Start_Date__c = Date.valueOf('2011-10-21'),
						AVTRRT__Job_Term__c = 'Permanent',
						AVTRRT__Experience__c = '01-03 Years',
						AVTRRT__Salary_Range__c = '25000 - 35000',
						AVTRRT__Country_Locale__c = 'India',
						BCST__Idibu_Salary_Range__c = '25000 - 35000'
				),
				new AVTRRT__Job__c(
						AVTRRT__Job_Title__c = 'Job Title #2',
						AVTRRT__Job_Description__c = 'Job Description #2',
						AVTRRT__Start_Date__c = Date.valueOf('2011-10-21'),
						AVTRRT__Job_Term__c = 'Permanent',
						AVTRRT__Experience__c = '01-03 Years',
						AVTRRT__Salary_Range__c = '25000 - 35000',
						AVTRRT__Country_Locale__c = 'India',
						BCST__Idibu_Salary_Range__c = '25000 - 35000'
				)
		};
		insert v_jobList;

		List<Job_Broadcast__c> v_jobBroadcastList = new List<Job_Broadcast__c> {
				new Job_Broadcast__c(
						Jobs__c = v_jobList[0].Id,
						Job_Board_Id__c = '517',
						Idibu_Job_Id__c = 'controlID'
				),
				new Job_Broadcast__c(
						Jobs__c = v_jobList[0].Id,
						Job_Board_Id__c = '517',
						Idibu_Job_Id__c = 'controlID'
				),
				new Job_Broadcast__c(
						Jobs__c = v_jobList[1].Id,
						Job_Board_Id__c = '517',
						Idibu_Job_Id__c = 'controlID'
				)
		};
		insert v_jobBroadcastList;

		Map<String, List<String>> v_deleteJobsMap = new Map<String, List<String>>();
		v_deleteJobsMap.put(v_jobList[0].Id, new List<String> {' ', '517', ' ', ' '});
		v_deleteJobsMap.put(v_jobList[1].Id, new List<String> {' ', '517', ' ', ' '});

		List<JobDelete> v_jobDeleteList = IdibuHelper.getJobDeleteList(v_deleteJobsMap);

		System.assertEquals(1, v_jobDeleteList.size());

		List<String> v_deleteXMLList = IdibuHelper.createDeleteXML(v_jobDeleteList);

		System.assertEquals(1, v_deleteXMLList.size());
		String v_deleteXMLResult = IdibuHelper.createDeleteXML(v_jobDeleteList[0]);

		System.assertEquals(v_deleteXMLResult, '<?xml version="1.0" encoding="UTF-8"?><idibu><method>DELETE' +
				'</method><job id="controlID"><sender id="' + IdibuHelper.getUserIdFromCustomSettings() +
				'" /><posts><board id="517" /><board id="517" /></posts></job></idibu>');
	}

	private static testMethod void testGetIdibuDeleteResponse() {
		String v_deleteXML = '<?xml version="1.0" encoding="UTF-8"?><IDIBU><JOB ID="00000008"><POST ' +
				'STATUS="PENDING" TYPE="Del" BOARDID="1193" REMOTEID="0000000000000000019" QUEUEID=' +
				'"0000007">Job was posted for deletion to this board.</POST></JOB></IDIBU>';

		IdibuResponse v_idibuResponse = new IdibuResponse();
		v_idibuResponse.job_id = '00000008';

		v_idibuResponse.postsList = new List<IdibuResponse.posts>();
		IdibuResponse.posts v_idibuPost = new IdibuResponse.posts();

		v_idibuPost.posts_status = 'PENDING';
		v_idibuPost.posts_boardid = '1193';
		v_idibuPost.posts_type = 'Del';
		v_idibuPost.posts_queueid = '0000007';
		v_idibuPost.posts_remoteid = '0000000000000000019';
		v_idibuPost.innerText = 'Job was posted for deletion to this board.';
		v_idibuResponse.postsList.add(v_idibuPost);

		IdibuResponse v_idibuResponseResult = IdibuHelper.getIdibuDeleteResponse(v_deleteXML);
		System.assertEquals(v_idibuResponse.postsList[0].posts_queueid,
				v_idibuResponseResult.postsList[0].posts_queueid);
	}

	private static testMethod void testGetIdibuResponse() {
		String v_responseXML = '<?xml version="1.0" encoding="UTF-8" ?><idibu><job status="added" ' +
				'id="00000008"><posts><posts status="pending" boardid="517" queueid="0000000" type="add" ' +
				'publish="2011-10-21 00:01" duration="2" /></posts></job><log><warnings><warning>' +
				'Warning_Some_Text</warning></warnings><errors><error>Error_Some_Text</error>' +
				'</errors></log></idibu>';
		String v_quotaResponseXML = '<?xml version="1.0" encoding="UTF-8" ?><idibu><job status="Delayed" ' +
				'id="00000018"><ERROR>Some_Error_Text</ERROR></job><log><warnings><warning>' +
				'no_quota_left</warning></warnings><errors><error>Error_Some_Text</error>' +
				'</errors></log></idibu>';
		/*testing successfull posting*/
		IdibuResponse v_idibuResponse = new IdibuResponse();
		v_idibuResponse.errorsList = new List<String>();
		v_idibuResponse.warningsList = new List<String>();

		IdibuResponse.posts v_idibuPost = new IdibuResponse.posts();

		v_idibuResponse.job_status = 'added';
		v_idibuResponse.job_id = '00000008';

		v_idibuPost.posts_boardid = '517';
		v_idibuPost.posts_duration = '2';
		v_idibuPost.posts_publish = '2011-10-21 00:01';
		v_idibuPost.posts_queueid = '0000000';
		v_idibuPost.posts_status = 'pending';
		v_idibuPost.posts_type = 'add';
		v_idibuPost.posts_warningString = 'Warning_Some_Text';
		v_idibuPost.posts_errorString = 'Error_Some_Text';

		v_idibuResponse.warningsList.add('Warning_Some_Text');
		v_idibuResponse.errorsList.add('Error_Some_Text');

		v_idibuResponse.postsList = new List<IdibuResponse.posts> {v_idibuPost};

		System.assertEquals(v_idibuResponse.job_status,
				IdibuHelper.getIdibuResponse(v_responseXML).job_status);

		/*testing error in posting-quota error,Date-09/19/2013*/

		IdibuResponse v_QuotaIdibuResponse = new IdibuResponse();
		v_QuotaIdibuResponse.errorsList = new List<String>();
		v_QuotaIdibuResponse.warningsList = new List<String>();

		v_QuotaIdibuResponse.job_status = 'Delayed';
		v_QuotaIdibuResponse.job_id = '00000018';
		v_QuotaIdibuResponse.errorsList.add('no_quota_left');

		System.assertEquals(v_QuotaIdibuResponse.job_status,
				IdibuHelper.getIdibuResponse(v_quotaResponseXML).job_status);

		//Testing Quota Method,6/27/2017
		IdibuHelper.getJobBoardQuotaDetailsFromIdibu();
		String v_idibuRemainingQuotaXML = '<idibu generator="idibu" version="1.0"><response><quotas>' +
				'<quota><portal><id>1058</id><name>Facebook</name></portal><posted><user>47</user>' +
				'<team>47</team><office>47</office></posted><queued><user>0</user><team>0</team>' +
				'<office>0</office></queued><remain><user/><team/><office/></remain><quota>' +
				'<user/><team/><office/></quota></quota></quotas></response><status>' +
				'success</status></idibu>';
		IdibuHelper.parseIdibuRemainingQuotaXML(v_idibuRemainingQuotaXML);

	}

	private static testMethod void testParseJobBoardValidators() {
		String v_jobBoardXML = '<?xml version="1.0" encoding="ISO8859-1" ?><idibu><boards>' +
				'<board id="517" last_modified="2010-01-14 00:00:00" published="true" ' +
				'supports_delete="Yes"><validators><rule type="fieldMaxLength" control="40">' +
				'<field>ex1</field><message>Some_Message</message></rule></validators><durations>' +
				'<duration value="1">7</duration><duration value="2">14</duration></durations>' +
				'<extrafields><extrafield name="ex1" type="text"/><extrafield name="ex2" ' +
				'description="ex2_description" type="select" required="true"><data><option>' +
				'Please Select...</option><option id="1">Programmers</option><option id="5">' +
				'Testers</option><option id="3">Administrators</option></data></extrafield>' +
				'</extrafields></board></boards></idibu>';

		v_jobBoardXML = v_jobBoardXML.replace('\'', '');
		v_jobBoardXML = v_jobBoardXML.replace('`', '');
		Dom.Document v_document = new Dom.Document();
		v_document.load(v_jobBoardXML);
		List<IdibuJobBoard.ExtrafieldRule> v_extrafieldRuleList =
				IdibuHelper.parseJobBoardValidators(v_document);

		System.assertEquals(1, v_extrafieldRuleList.size());
	}

	private static testMethod void testGetJobBoardsIDNameMap() {
		String v_jobBoardsXML = '<idibu><boards><board id="517" last_modified="2010-01-14 00:00:00" ' +
				'published="true"><name>idibu Developer Board</name><description>' +
				'<![CDATA[SomeText]]></description><tags><tag>general</tag></tags>' +
				'</board></boards></idibu>';
		Map<String, String> v_jobBoardsIDNameMap = IdibuHelper.getJobBoardsIDNameMap(v_jobBoardsXML);
		System.assertEquals(1, v_jobBoardsIDNameMap.size());

		Set<String> v_jobBoardsIDSet = v_jobBoardsIDNameMap.keySet();
		String v_jobBoardId = new List<String>(v_jobBoardsIDSet)[0];

		System.assertEquals('517', v_jobBoardId);
		System.assertEquals('idibu Developer Board', v_jobBoardsIDNameMap.get(v_jobBoardId));
	}
	
	public static void createCustomSettings() {
		AVTRRT__Config_Settings__c cs = AVTRRT__Config_Settings__c.getValues('Default');
		if (cs == null) {
			cs = new AVTRRT__Config_Settings__c(Name = 'Default');
		}
		cs.AVTRRT__Enable_Geosearch_for_Account__c = false;
		cs.AVTRRT__Enable_Geosearch_for_Candidate__c = false;
		cs.AVTRRT__Enable_Geosearch_for_Contact__c = false;
		cs.AVTRRT__Enable_Geosearch_for_Job__c = false;
		upsert cs;
		
		Job_Broadcasting_Settings__c v_jobBroadcastingSettings =
				Job_Broadcasting_Settings__c.getInstance();
		v_jobBroadcastingSettings.Idibu_User_Id__c = '31000602';
		upsert v_jobBroadcastingSettings;
	}
}