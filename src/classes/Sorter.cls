public abstract with sharing class Sorter {
	private List<Object> objects;
	private String direction;
	
	private static Logger logger = new Logger('Sorter');
	
	public Sorter(List<Object> objects, String direction) {
		this.objects = objects;
		this.direction = direction;
	}
	
	public List<Object> sort() {
		Map<Object, List<Object>> forSort = new Map<Object, List<Object>>();
		
		for (Object obj : objects) {
			Object key = getKey(obj);
			if (forSort.get(key) == null) {
				forSort.put(key, new List<Object>());
			}
			
			forSort.get(key).add(obj);
		}
		
		List<Object> keyList = new List<Object>(forSort.keySet());
		keyList.sort();
		
		objects.clear();
		
		if (String.isBlank(direction) || direction == 'asc') {
			for (Integer i = 0; i < keyList.size(); i++) {
				for (Object obj : forSort.get(keyList[i])) {
					objects.add(obj);
				}
			}
		}
		
		if (direction == 'desc') {
			for (Integer i = keyList.size() - 1; i >= 0; i--) {
				for (Object obj : forSort.get(keyList[i])) {
					objects.add(obj);
				}
			}
		}
		return objects;
	}
	
	public abstract Object getKey(Object obj);
}