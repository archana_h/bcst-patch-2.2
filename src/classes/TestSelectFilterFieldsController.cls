/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@IsTest
private class TestSelectFilterFieldsController {
	
	static testMethod void testSelectFFController() {
		AVTRRT__Config_Settings__c cs = AVTRRT__Config_Settings__c.getValues('Default');
		if (cs == null) {
			cs = new AVTRRT__Config_Settings__c(Name = 'Default');
		}
		cs.AVTRRT__Enable_Geosearch_for_Account__c = false;
		cs.AVTRRT__Enable_Geosearch_for_Candidate__c = false;
		cs.AVTRRT__Enable_Geosearch_for_Contact__c = false;
		cs.AVTRRT__Enable_Geosearch_for_Job__c = false;
		upsert cs;
		
		Job_Broadcasting_Settings__c v_jobBroadcastingSettings =
				Job_Broadcasting_Settings__c.getInstance();
		v_jobBroadcastingSettings.Idibu_User_Id__c = '31000602';
		upsert v_jobBroadcastingSettings;
		
		AVTRRT__Job__c job = new AVTRRT__Job__c(
				AVTRRT__Job_Title__c = 'Java developer',
				AVTRRT__Job_Description__c = 'Java developer description',
				AVTRRT__Start_Date__c = Date.valueOf('2011-10-21'),
				AVTRRT__Job_Term__c = 'Permanent',
				AVTRRT__Experience__c = '01-03 Years',
				AVTRRT__Salary_Range__c = '25000 - 35000',
				AVTRRT__Country_Locale__c = 'India',
				Idibu_Job_Type__c = '3',
				Idibu_Location__c = '37,1'
		);
		insert job;
		
		ApexPages.currentPage().getParameters().put('object', 'AVTRRT__Job__c');
		ApexPages.StandardController standardController = new ApexPages.StandardController(job);
		SelectFilterFieldsController selectFFController =
				new SelectFilterFieldsController(standardController);
		
		List<SelectFilterFieldsController.rcrdWrapper> rcrds = selectFFController.rcrds;
		selectFFController.selectFields();
	}
}