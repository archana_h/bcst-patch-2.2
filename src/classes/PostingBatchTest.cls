@isTest
private class PostingBatchTest {
	static testMethod void testPostingBatch() {
		AVTRRT__Config_Settings__c configSetting = new AVTRRT__Config_Settings__c(
				Name = 'Default',
				AVTRRT__Enable_Geosearch_for_Job__c = false,
				AVTRRT__Enable_Geosearch_for_Account__c = false,
				AVTRRT__Enable_Geosearch_for_Candidate__c = false,
				AVTRRT__Enable_Geosearch_for_Contact__c = false
		);
		insert configSetting;
		
		Job_Broadcasting_Settings__c jobBroadcastingSettings = 
				Job_Broadcasting_Settings__c.getInstance();
		jobBroadcastingSettings.BCST__Failed_Jobs_To_Sent_Email__c = 'true';
		jobBroadcastingSettings.BCST__Idibu_User_Id__c ='12434';
		upsert jobBroadcastingSettings;
		
		AVTRRT__Job__c job = new AVTRRT__Job__c();
		job.AVTRRT__Job_Title__c = 'Java developer UTest';
		job.AVTRRT__Job_Description__c = 'Java developer description';
		job.AVTRRT__Job_Description_Rich__c = 'Test job description Rich';
		job.AVTRRT__Start_Date__c = Date.valueOf('2011-10-21');
		job.AVTRRT__Job_Term__c = 'Permanent';
		job.AVTRRT__Experience__c = '01-03 Years';
		job.AVTRRT__Salary_Range__c = '25000 - 35000';
		job.AVTRRT__Country_Locale__c = 'India';
		job.BCST__Idibu_Salary_Range__c = '25000 - 35000';
		job.Idibu_Job_Type__c = '4';
		job.Idibu_Location__c = '9,0';
		job.Idibu_Category__c = '18';
		job.AVTRRT__Billable__c = true;
		job.AVTRRT__Recruiter__c = UserInfo.getUserId();
		insert job;
		
		Test.startTest();
		
		PostingBatch postingBatch = 
				new PostingBatch('AVTRRT__Billable__c=true', '188');
		Database.executeBatch(postingBatch);
		
		Test.stopTest();
	}
}