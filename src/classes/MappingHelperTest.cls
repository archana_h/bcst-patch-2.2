/**
 Class

 @name MappingHelperTest
 @author Aliaksandr Satskou
 @description Unit Test - Class for MappingHelper.
 @data 08/14/2012
*/

@IsTest
private class MappingHelperTest {
	
	private static AVTRRT__Job__c activeJob;
	private static List<BCST__JobBroadcasting_Field_Mapping__c> mappingList;
	private static List<BCST__JobBroadcasting_Value_Mapping__c> valueMapping;
	private static List<IdibuJobBoard> idibuJobBoardList;
	
	/**
	 Method

	 @name initData
	 @author Aliaksandr Satskou
	 @data 08/16/2012
	 @description Create and initialize all needed data.
	*/
	private static void initData() {
		IdibuJobBoard v_idibuJobBoard = new IdibuJobBoard();
		
		IdibuJobBoard.Extrafield v_extrafield1 = new IdibuJobBoard.Extrafield();
		v_extrafield1.extrafield_name = 'extrafield_name #1';
		IdibuJobBoard.Extrafield v_extrafield2 = new IdibuJobBoard.Extrafield();
		v_extrafield2.extrafield_name = 'extrafield_name #2';
		
		v_idibuJobBoard.extrafieldsList = new List<IdibuJobBoard.Extrafield> {
				v_extrafield1, v_extrafield2
		};
		
		idibuJobBoardList = new List<IdibuJobBoard> {v_idibuJobBoard};
		
		AVTRRT__Config_Settings__c v_configSetting = new AVTRRT__Config_Settings__c(
				Name = 'Default',
				AVTRRT__Enable_Creation_Job_Candidate_Search__c = false
		);
		insert v_configSetting;
		
		activeJob = new AVTRRT__Job__c(
				AVTRRT__Job_Title__c = 'Job Title #1',
				AVTRRT__Start_Date__c = Date.today() + 1,
				AVTRRT__Stage__c = 'Open',
				Idibu_Salary_Range__c = '25000.0 - 35000.0',
				Idibu_Job_Type__c = '3',
				Idibu_Location__c = '37,1',
				Idibu_Category__c = '18',
				Idibu_Currency__c = 'US'
		);
		insert activeJob;
		
		mappingList = new List<BCST__JobBroadcasting_Field_Mapping__c> {
				new BCST__JobBroadcasting_Field_Mapping__c(
						BCST__Extrafield_Name__c = idibuJobBoardList[0].extrafieldsList[0].extrafield_name,
						BCST__Mapped_Formula__c = 'SomeText'
				),
				new BCST__JobBroadcasting_Field_Mapping__c(
						BCST__Extrafield_Name__c = idibuJobBoardList[0].extrafieldsList[1].extrafield_name,
						BCST__Mapped_Formula__c = '{!AVTRRT__Stage__c}'
				)
		};
		insert mappingList;
		
		valueMapping = new List<BCST__JobBroadcasting_Value_Mapping__c> {
				new BCST__JobBroadcasting_Value_Mapping__c(
						BCST__Job_Field_Value__c = 'Open',
						BCST__Extrafield_Mapped_Value__c = '1',
						BCST__Extrafield_Parent_Value__c = '',
						BCST__JobBroadcasting_Field_Mapping__c = mappingList[1].Id
				),
				new BCST__JobBroadcasting_Value_Mapping__c(
						BCST__Job_Field_Value__c = 'Sourcing',
						BCST__Extrafield_Mapped_Value__c = '3',
						BCST__Extrafield_Parent_Value__c = '',
						BCST__JobBroadcasting_Field_Mapping__c = mappingList[1].Id
				)
		};
		insert valueMapping;
	}
	
	/**
	 Method

	 @name testMappingHelper
	 @author Aliaksandr Satskou
	 @data 08/16/2012
	 @description Test functionality of MappingHelper class.
	*/
	static testMethod void testMappingHelper() {
		initData();
		
		MappingHelper.initData(new Set<String> {activeJob.Id});
		MappingHelper.initJob(activeJob.Id);
		
		String v_mappedFormulaValue = MappingHelper.getMappedFormulaValue(
				idibuJobBoardList[0].extrafieldsList[0].extrafield_name);
		System.assertEquals('SomeText', v_mappedFormulaValue);
		
		v_mappedFormulaValue = MappingHelper.getMappedFormulaValue(
				idibuJobBoardList[0].extrafieldsList[1].extrafield_name);
		
		//System.assertEquals('1', v_mappedFormulaValue);
	}
}