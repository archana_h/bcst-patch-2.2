/**
* @description  Helper class for removing Html charecters from a Html string or adding Html charecters to a string.
*/
public with sharing class HtmlHelper {
	/**
	* @name StripHtml
	* @param source String 
	* @return String
	*/
	public static String StripHtml(String source) {
		String result;
		
		result = source.replaceAll('(?i)( )+', ' ');
		
		// Remove the header (prepare first by clearing attributes)
		result = result.replaceAll('(?i)<( )*head([^>])*>', '<head>');
		
		result = result.replaceAll('(?i)(<( )*(/)( )*head( )*>)', '</head>');
		result = result.replaceAll('(?i)(<head>).*(</head>)', ' ');
		
		// remove all scripts (prepare first by clearing attributes)
		result = result.replaceAll('(?i)<( )*script([^>])*>', '<script>');
		
		result = result.replaceAll('(?i)(<( )*(/)( )*script( )*>)', '</script>');
		result = result.replaceAll('(?i)(<script>).*(</script>)', ' ');
		
		// remove all styles (prepare first by clearing attributes)
		result = result.replaceAll('(?i)<( )*style([^>])*>', '<style>');
		result = result.replaceAll('(?i)(<( )*(/)( )*style( )*>)', '</style>');
		result = result.replaceAll('(?i)(<style>).*(</style>)', ' ');
		
		// insert tabs in spaces of <td> tags
		result = result.replaceAll('(?i)<( )*td([^>])*>', '\t');
		
		// insert line breaks in places of <BR> and <LI> tags
		result = result.replaceAll('(?i)<( )*br( )*>', '\r\n');
		result = result.replaceAll('(?i)<( )*li( )*>', '\r\n');
		
		// if <P>, <DIV> and <TR> tags
		result = result.replaceAll('(?i)<( )*div([^>])*>', '\r\n');
		result = result.replaceAll('(?i)<( )*tr([^>])*>', '\r\n');
		result = result.replaceAll('(?i)<( )*p([^>])*>', '\r\n');
		
		// Remove remaining tags like <a>, links, images,
		// comments etc - anything that's enclosed inside < >
		result = result.replaceAll('(?i)<[^>]*>', ' ');
		
		// replace special characters:
		result = result.replaceAll(' ', ' ');
		
		result = result.replaceAll('(?i)(&bull;)', ' * ');
		
		result = result.replaceAll('(?i)(&lsaquo;)', '<');
		
		result = result.replaceAll('(?i)(&rsaquo;)', '>');
		
		result = result.replaceAll('(?i)(&trade;)', '(tm)');
		
		result = result.replaceAll('&frasl;', '/');
		
		result = result.replaceAll('&lt;', '<');
		
		result = result.replaceAll('&gt;', '>');
		
		result = result.replaceAll('&copy;', '(c)');
		
		result = result.replaceAll('&reg;', '(r)');
		
		result = result.replaceAll('&amp;', '&');
		
		result = result.replaceAll('&quot;', '"');
		
		// Remove all others. More can be added
		result = result.replaceAll('&(.{2,6});', ' ');
		
		// That's it.*/
		return result;
	}
	
	/**
	* @name plainToHtml
	* @param plainText String 
	* @return String
	*/
	public static String plainToHtml(String plainText) {
		return plainText.replace('\n', '<br />');
	}
}