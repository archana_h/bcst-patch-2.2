/**
 Class

 @Name: PostingHelper
 @Author: Aliaksandr Satskou
 @Data: 09/07/2012
*/
global class PostingHelper {
	
	/**
	 Method

	 @Name: doBroadcast
	 @Author: Aliaksandr Satskou
	 @Data: 09/07/2012
	 @Parameters:
		1. |String p_portionString| - String of Job Object and job boards.
	 @Description: Posting jobs to Idibu.
	*/
	@Future(Callout = true)
	global static void doBroadcast(String p_portionString) {
		BroadcastingController.broadcast(p_portionString);
	}
}