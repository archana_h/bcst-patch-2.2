@IsTest
private class FilesTest {
	private static Logger logger = new Logger('FilesTest');
	
	@IsTest
	private static void testCreate() {
		AVTRRT__Config_Settings__c configSetting = new AVTRRT__Config_Settings__c(
				Name = 'Default',
				AVTRRT__Trigger_Disabled_Contact__c = true
		);
		insert configSetting;
		
		Account account = new Account(Name = 'Test Account');
		insert account;
		
		Contact contact = new Contact(LastName = 'Test', AccountId = account.Id);
		insert contact;
		
		Files.File[] filesToCreate = new Files.File[] {};
		
		Files.File file = new Files.File();
		file.name = 'Test1.pdf';
		file.body = Blob.valueOf('Ha-ha');
		file.parentId = contact.Id;
		filesToCreate.add(file);
		
		file = new Files.File();
		file.name = 'Test2.pdf';
		file.body = Blob.valueOf('Ha-ha');
		file.parentId = contact.Id;
		filesToCreate.add(file);
		
		file = new Files.File();
		file.name = 'Test3.pdf';
		file.body = Blob.valueOf('Ha-ha');
		file.parentId = contact.Id;
		filesToCreate.add(file);
		
		Test.startTest();
		
		ContentVersion[] versions = Files.create(filesToCreate);
		
		Test.stopTest();
		
		System.assertEquals(3, versions.size());
	}
	
	@IsTest
	private static void testConvert() {
		AVTRRT__Config_Settings__c configSetting = new AVTRRT__Config_Settings__c(
				Name = 'Default',
				AVTRRT__Trigger_Disabled_Contact__c = true
		);
		insert configSetting;
		
		Account account = new Account(Name = 'Test');
		insert account;
		
		Contact contact = new Contact(LastName = 'Test', AccountId = account.Id);
		insert contact;
		
		Attachment[] attachments = new Attachment[] {};
		for (Integer i = 0; i < 3; i++) {
			createAttachment(attachments, contact.Id, i + 1);
		}
		insert attachments;
		
		attachments = [
				SELECT ParentId, OwnerId, Name, Description, Body, IsPrivate,
						CreatedById, CreatedDate, LastModifiedById, LastModifiedDate
				FROM Attachment
		];
		
		Test.startTest();
		
		ContentVersion[] versions = Files.convert(attachments, true);
		
		Test.stopTest();
		
		System.assertEquals(3, versions.size());
		
		attachments = [
				SELECT ParentId, OwnerId, Name, Description, Body, IsPrivate,
						CreatedById, CreatedDate, LastModifiedById, LastModifiedDate
				FROM Attachment
				WHERE ParentId = :contact.Id
		];
		
		System.assertEquals(0, attachments.size());
	}
	
	private static void createAttachment(Attachment[] attachments, Id parentId, Integer num) {
		if (attachments != null) {
			attachments.add(new Attachment(
					Name = 'Test Attachment' + num,
					Body = Blob.valueOf('Test Attachment Body' + num),
					ParentId = parentId
			));
		}
	}
}