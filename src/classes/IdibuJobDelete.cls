global with sharing class IdibuJobDelete
		implements Database.Batchable<JobDelete>, Database.AllowsCallouts {
	//List of JobDelete. Contains information about removed Job's and Idibu Job Boards.
	private List<JobDelete> jobDeleteList;

	//Formation and sending a letter with the report of the removal from Idibu.
	public void doSendEmail(IdibuResponse objResponse) {
		User currentUser = [
				SELECT Name, Email
				FROM User
				WHERE Id = :UserInfo.getUserId()
		];
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		String[] toAddresses = new String[] {currentUser.Email};
		mail.setToAddresses(toAddresses);

		mail.setSenderDisplayName(currentUser.Name);
		mail.setSubject('Job Broadcasting Delete Report');
		mail.setUseSignature(false);

		System.debug('::::::::000::::::::objResponse.postsList= ' + objResponse.postsList);
		if (objResponse.postsList.size() > 0) {
			String htmlBody = '<div style="font-family: Tahoma; border: 1px solid #b7caff; ' +
					'background: #e9eefd;"><h1 style="font-size: 18px; font-weight: normal; ' +
					'color: #4b4b4b; padding: 5px 10px 5px 10px;">Job Broadcasting Delete Report</h1>' +
					'<p style="padding: 5px 10px 5px 10px;">Date: <b>' + Datetime.now().format() + '</b></p>';
			
			for (IdibuResponse.posts post : objResponse.postsList) {
				htmlBody += '<div style="padding: 5px 10px 5px 10px;"><p>JOB ID: ' + objResponse.job_id +
						', JOB BOARD ID: ' + post.posts_boardid + '</p>' +
						'<p style="font-size: 14px; color: #c66a6a;">? ' + post.innerText + '</p></div>';
			}

			htmlBody += '</div>';
			mail.setHtmlBody(htmlBody);
		} else {
			mail.setPlainTextBody('Job Broadcasting Delete Report\n\nDate: ' +
					Datetime.now().format() + '\nStatus: Unknown result.');
		}

		System.debug('::::::::::::::::::::::::doSendMail(), mail=' + mail);

		if (!Test.isRunningTest()) {
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
		}

	}

	global IdibuJobDelete(List<JobDelete> jDeleteList) {
		jobDeleteList = jDeleteList;
	}

	global Iterable<JobDelete> start(Database.BatchableContext bc) {
		return jobDeleteList;
	}

	//Formation and sending a request for remove to Idibu.
	global void execute(Database.BatchableContext bc, List<JobDelete> jobDeleteRList) {
		for (JobDelete jDelete : jobDeleteRList) {
			//Creation delete XML.
			String deleteXml = IdibuHelper.createDeleteXML(jDelete);
			System.debug(LoggingLevel.ERROR, '::::deleteXml' + deleteXml);
			String deleteResponse = IdibuHelper.postingJobToIdibu(
					IdibuHelper.getHashFromCustomSettings(), deleteXml);

			Support.sendSupportEmail('Delete XML', deleteXml, false);
			Support.sendSupportEmail('Delete Response', deleteResponse, false);
			System.debug(LoggingLevel.ERROR, '::::deleteResponse' + deleteResponse);
			//Parse delete response from Idibu.
			IdibuResponse objResponse = IdibuHelper.getIdibuDeleteResponse(deleteResponse);
			System.debug(LoggingLevel.ERROR, ':::::::::::::::::::::::::::objResponse=' + objResponse);

			List<BCST__Job_Broadcast__c> jobBroadcastList = [
					SELECT BCST__Idibu_Job_Id__c, BCST__Job_Board_Id__c
					FROM BCST__Job_Broadcast__c
					WHERE BCST__Idibu_Job_Id__c = :objResponse.job_id
			];

			for (IdibuResponse.posts postSingle : objResponse.postsList) {
				String jobBoardId = postSingle.posts_boardid;

				BCST__Job_Broadcast__c jobBroadcastForDelete;
				if (postSingle.posts_status == 'PENDING') {
					for (BCST__Job_Broadcast__c jobBroadcast : jobBroadcastList) {
						if (jobBroadcast.BCST__Job_Board_Id__c == jobBoardId) {
							jobBroadcastForDelete = jobBroadcast;
							jobBroadcastForDelete.BCST__Broadcasted_Status__c = 'Inactive';
							break;
						}
					}
					//delete jobBroadcastForDelete;
					update jobBroadcastForDelete;
				}
			}

			doSendEmail(objResponse);
		}
	}

	global void finish(Database.BatchableContext bc) {
	}
}