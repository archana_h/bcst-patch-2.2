global class ProcessJobApplicantEmail implements Messaging.InboundEmailHandler {

	global Messaging.InboundEmailResult handleInboundEmail(
			Messaging.InboundEmail p_email, Messaging.InboundEnvelope p_envelope) {
		
		if (!Test.isRunningTest()) {
			new EmailToCandidateBroadcasting().createETCObject(p_email);
		}
		
		return null;
	}
}