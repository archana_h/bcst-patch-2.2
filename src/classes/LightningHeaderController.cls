global with sharing class LightningHeaderController {
	public List<String> listOfFields { get; set; }

	public SObject recordData { get; set; }

	public Boolean showRecord { get; set; }

	private Integer assignedToDoneCounter = 0;

	private static Logger logger = new Logger('LightningHeaderController');

	public String recordIds {
		get;
		set {
			recordIds = value;
			callInitIfNeeded();
		}
	}

	public String sObjectName {
		get;
		set {
			sObjectName = value;
			callInitIfNeeded();
		}
	}

	public String intermediatePageName {
		get;
		set {
			intermediatePageName = value;
			callInitIfNeeded();
		}
	}

	public LightningHeaderController() {
		showRecord = false;
	}

	public void init() {
		try {
			loadRecord();
		} catch (Exception e) {
			//Removed the catch because to not to show error message in another pages
			//logger.catchBlockMethodNew(e);
		}
	}

	public void loadRecord() {
		if (!String.isBlank(intermediatePageName)) {
			List<BCST__Intermediate_Pages__c> intermediatePagesFields = [
					SELECT Name, BCST__Field_API_Name__c, BCST__Page_Name__c
					FROM BCST__Intermediate_Pages__c
					WHERE BCST__Page_Name__c = :intermediatePageName
					ORDER BY BCST__Order__c ASC
			];
			
			List<String> fields = new List<String>();
			for (BCST__Intermediate_Pages__c inter : intermediatePagesFields) {
				fields.add(inter.BCST__Field_API_Name__c);
			}
			
			String fieldsString = String.join(fields, ',');
			listOfFields = fieldsString.split(',');
			sObjectName = findObjectAPIName(recordIds);
			String query =
					' SELECT ' + fieldsString +
							' FROM ' + sObjectName +
							' WHERE Id = :recordIds';
			recordData = Database.query(query);
			
			showRecord = true;
		}
	}

	@RemoteAction
	global static SObject recordData(String record, String sObjName, String allFields) {
		try {
			if (record == null) {
				return null;
			}
			String objectAPIName = '';
			String keyPrefix = record.substring(0, 3);
			for (Schema.SObjectType obj : Schema.getGlobalDescribe().values()) {
				String prefix = obj.getDescribe().getKeyPrefix();
				if (prefix == keyPrefix) {
					objectAPIName = obj.getDescribe().getName();
					break;
				}
			}
			
			sObjName = objectAPIName;
			String query =
					' SELECT ' + allFields +
							' FROM ' + sObjName +
							' WHERE Id = :record';
			
			return Database.query(query);
		} catch (Exception e) {
			logger.catchBlockMethodNew(e);
			
			return null;
		}
	}
	
	private void callInitIfNeeded() {
		assignedToDoneCounter++;
		
		if (assignedToDoneCounter == 3) {
			init();
		}
	}

	public String findObjectAPIName(String recordId) {
		if (recordId == null) {
			return null;
		}
		String objectAPIName = '';
		String keyPrefix = recordId.substring(0, 3);
		for (Schema.SObjectType obj : Schema.getGlobalDescribe().values()) {
			String prefix = obj.getDescribe().getKeyPrefix();
			if (prefix == keyPrefix) {
				objectAPIName = obj.getDescribe().getName();
				break;
			}
		}
		return objectAPIName;
	}
}