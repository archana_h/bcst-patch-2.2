global with sharing class IdibuJobBoard {
	/*
	*@author-vishwajeet
	*@date-09/03/2013
	*Case#: 00021059
	*Descritpion : Modified class type to 'global with sharing' from 'public'
	*/
	public String board_id { get; set; }
	public String name { get; set; }
	public String description { get; set; }
	public List<String> tags { get; set; }
	public List<Extrafield> extrafieldsList { get; set; }
	public Map<String, String> durationMap { get; set; }
	
	public class Extrafield {
		public String extrafield_name { get; set; }
		public String extrafield_description { get; set; }
		public String extrafield_type { get; set; }
		public String extrafield_multi { get; set; }
		public String extrafield_required { get; set; }
		public String extrafield_order { get; set; }
		public String extrafield_validated { get; set; }
		public String validationMessage { get; set; }
		public Boolean isDependentLists { get; set; }
		
		public Map<String, List<ExtrafieldOption>> extrafieldOptionMap;
		public Map<String, String> parentIdParentNameMap;
	}
	
	public class ExtrafieldOption {
		public String option_id;
		public String nodeText;
		
		public ExtrafieldOption(String p_optionId, String p_nodeText) {
			option_id = p_optionId;
			nodeText = p_nodeText;
		}
	}
	
	public class ExtrafieldRule {
		public String rule_type;
		public String rule_control;
		public String field;
		public String message;
	}
}