global with sharing class JobDelete {
	global String jobId { get; set; }
	global String idibuControlId { get; set; }
	global List<String> idibuJobBoardId { get; set; }
}