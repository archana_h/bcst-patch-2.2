// Encode string to XSS HTML protected string
// For other encodings (JS, URL, JSINHTML) please see
// http://wiki.developerforce.com/images/4/4e/VisualForce_escaping.pdf
public class EncodeUtil {
	private static Map<String, String> htmlMap = new Map<String, String> {
			'&' => '&amp;',
			'"' => '&quot;',
			'\'' => '&#39;',
			'<' => '&lt;',
			'>' => '&gt;'
	};

	public static String HtmlEncode(String str) {
		for (String key : htmlMap.keySet()) {
			str = str.replace(key, htmlMap.get(key));
		}
		return str;
	}
	/* Added by vishwajeet,Case#-14902,Date-04/04/2013 */
	public static String HtmlDecode(String str) {
		for (String key : htmlDecodeMap.keySet()) {
			str = str.replace(key, htmlDecodeMap.get(key));
		}
		return str;
	}

	/* Added by vishwajeet,Case#-14902,Date-04/04/2013 */
	private static Map<String, String> htmlDecodeMap = new Map<String, String> {
			'&amp;' => '&',
			'&quot;' => '"',
			'&#39;' => '\'',
			'&lt;' => '<',
			'&gt;' => '>'
	};

	private static Map<String, String> keywordsMap = new Map<String, String> {
			'?' => '\\?',
			//'&' => '\\&',
			'|' => '\\|',
			'!' => '\\!',
			'{' => '\\{',
			'}' => '\\}',
			'[' => '\\[',
			']' => '\\]',
			//'(' => '\\(',
			//')' => '\\)',
			//'^' => '\\^',
			'~' => '\\~',
			//'*' => '\\*',
			':' => '\\:',
			//'"' => '\\"',
			'\'' => '\\\'',
			'+' => '\\+',
			'-' => '\\-'
	};

	public static String KeywordsEncode(String str) {
		str = str.replace('\\', '\\\\');
		
		for (String key : keywordsMap.keySet()) {
			str = str.replace(key, keywordsMap.get(key));
		}

		return str;
	}
}