@IsTest
public class CronParserTest {
	private testMethod static void testIsRunToday() {
		Datetime todayDate = Datetime.now();
		Datetime tomorrowDate = Datetime.now() + 1;
		
		// Checks everyday cron
		System.assertEquals(true, CronParser.isRunToday('0 0 0 * * ?'));
		
		// Checks on current day cron
		System.assertEquals(true, CronParser.isRunToday(
				'0 0 0 ? * ' + todayDate.format('EEE').toUpperCase()));
		
		// Checks on tomorrow cron
		System.assertEquals(false, CronParser.isRunToday(
				'0 0 0 ? * ' + tomorrowDate.format('EEE').toUpperCase()));
	}
}