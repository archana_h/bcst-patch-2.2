@IsTest
private class JobTriggerTest {
	private static List<AVTRRT__Job__c> jobList;
	private static List<BCST__Job_Broadcasting_Settings__c> jobBroadcastingSettingList;

	/**
	 Method

	 @Name: initData
	 @Author: Aliaksandr Satskou & Vishwajeet
	 @Data: 04/12/2012
	 @Description: Initialize all needed data.
  	 @return void.
	*/
	private static void initData() {
		String v_currentUserId = UserInfo.getUserId();

		AVTRRT__Config_Settings__c v_configSetting = new AVTRRT__Config_Settings__c(
				Name = 'Default',
				AVTRRT__Enable_Creation_Job_Candidate_Search__c = false
		);
		insert v_configSetting;

		jobList = new List<AVTRRT__Job__c> {
				new AVTRRT__Job__c(
						AVTRRT__Job_Title__c = 'Job Title #1',
						AVTRRT__Recruiter__c = v_currentUserId
				),
				new AVTRRT__Job__c(
						AVTRRT__Job_Title__c = 'Job Title #2',
						AVTRRT__Recruiter__c = v_currentUserId
				),
				new AVTRRT__Job__c(
						AVTRRT__Job_Title__c = 'Job Title #3'
				)
		};
		insert jobList;

		jobBroadcastingSettingList = new List<BCST__Job_Broadcasting_Settings__c> {
				new BCST__Job_Broadcasting_Settings__c(
						SetupOwnerId = v_currentUserId,
						Idibu_User_Id__c = 'SENDER_ID',
						BCST__DeleteJobsFromJobBoardOnJobStageInactive__c = true
				),
				new BCST__Job_Broadcasting_Settings__c(
						Idibu_User_Id__c = 'DEFAULT_ID'
				)
		};
		insert jobBroadcastingSettingList;
	}

	/**
	 Method

	 @Name: testJobTrigger
	 @Author: Aliaksandr Satskou & Vishwajeet
	 @Data: 04/12/2012
	 @Description: Testing JobTrigger.trigger.
	 @return void.
	*/
	static testMethod void testJobTrigger() {
		initData();

		update jobList;

		jobList = [
				SELECT BCST__Idibu_Sender_Id__c
				FROM AVTRRT__Job__c
				WHERE Id IN :jobList
		];

		System.assertEquals('SENDER_ID', jobList[0].BCST__Idibu_Sender_Id__c);
		System.assertEquals('SENDER_ID', jobList[1].BCST__Idibu_Sender_Id__c);
		System.assertEquals('DEFAULT_ID', jobList[2].BCST__Idibu_Sender_Id__c);
	}

	/**
	 Method

	 @Name: testJobTriggerDelete
	 @Author: Vishwajeet
	 @Data: 08/26/2013
	 @Description: Testing deletion job from of all job board, once job stage is made inactive .
	 @return void.
	*/
	static testMethod void testJobTriggerDelete() {
		initData();
		String v_user = UserInfo.getUserId();
		/* Creating job broadcast objects*/
		List<BCST__Job_Broadcast__c> v_JobBroadcastList = new List<BCST__Job_Broadcast__c> {
				new BCST__Job_Broadcast__c(
						BCST__Broadcasted__c = true,
						BCST__Broadcasted_By__c = v_user,
						BCST__Idibu_Job_Id__c = '111144',
						BCST__Job_Board_Name__c = 'IdibuDeveloperJobBoard',
						BCST__Job_Board_Id__c = '517',
						BCST__Jobs__c = jobList[0].Id,
						BCST__Status__c = 'Added',
						BCST__Notes__c = 'Test job added successfully')
		};
		insert v_JobBroadcastList;
		jobList[0].AVTRRT__Stage__c = 'Inactive';
		update jobList[0];

	}
}