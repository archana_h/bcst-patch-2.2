public with sharing class JB_Logger {
	
	/*
		private static String messages = '';

		private String className;

		public Boolean throwException = false;
		public static Boolean throwExceptionOnTest = true;

		public JB_Logger(String className)
		{
			this.className = className;
		}

		public JB_Logger(String className, Boolean throwException)
		{
			this.className = className;
			this.throwException = throwException;
		}

		public void log(String methodName)
		{
			log(methodName, '');
		}

		public void log(String methodName, String variable, Object value)
		{
			log(methodName, variable + ' = ' + value);
		}

		public void show(String methodName, String variable, Object value)
		{
			String logMessage = log(methodName, variable + ' = ' + value);
			addWarningMessage(logMessage);
		}

		public String log(String methodName, String message) {

			String logMessage =
					'::::::::::' +
					'Class name: \'' + className +
				'\' Method name: \'' + methodName +
				//'\' Line: \'' + line +
				'\' Message: \'' + message + '\'';

			System.debug(LoggingLevel.ERROR, logMessage);
			return logMessage;
		}

		public void catchBlockMethodNew(Exception e)
		{
			addErrorMessage(e.getMessage());
			addErrorMessage(e.getStackTraceString());

			sendLog(e.getMessage(), className);

			if ((Test.isRunningTest() && throwExceptionOnTest) || throwException) {
				throw e;
			}
		}

		public String getMessagesString()
		{
			List<String> results = new List<String>();
			for (ApexPages.Message message : ApexPages.getMessages())
			{
				results.add(message.getSeverity() + ': ' + message.getSummary());
			}
			return String.join(results, '<br />');
		}

		public static void addErrorMessage(String message)
		{
			// We can show the message if request was done from VF page only.
			// We can't call ApexPages.addMessage if we made request from webservice or trigger.
			if (ApexPages.currentPage() != null)
			{
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, message);
				ApexPages.addMessage(msg);
			}
		}

		public static void addWarningMessage(String message)
		{
			// We can show the message if request was done from VF page only.
			// We can't call ApexPages.addMessage if we made request from webservice or trigger.
			if (ApexPages.currentPage() != null)
			{
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, message);
				ApexPages.addMessage(msg);
			}
		}

		public static void addInfoMessage(String message)
		{
			// We can show the message if request was done from VF page only.
			// We can't call ApexPages.addMessage if we made request from webservice or trigger.
			if (ApexPages.currentPage() != null)
			{
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, message);
				ApexPages.addMessage(msg);
			}
		}


		public static void sendLog(String lastMessage, String subject) {
			messages =
					'Organization: \'' + UserInfo.getOrganizationName() + '\'\r\n' +
					((ApexPages.currentPage() != null) ? ('Page Url: \'' + ApexPages.currentPage().getUrl() + '\'\r\n') : '') +
					((Site.getName() != null) ? ('Site Name: \'' + Site.getName() + '\'\r\n') : '') +
					'\r\n' + messages;


		}

		public class LoggerException extends Exception{}
		*/
}