/**
* @author Mikhail Ivanov
* @date 08/31/2012
* @description Parser of CRON time strings.
*/
public with sharing class CronParser {
	
	/**
	* @author Mikhail Ivanov
	* @date 08/31/2012
	* @description Parses the CRON string and tells if current date is there in this CRON string.
	*		Note: current version of this method supports '*' or '?' symbol for 'day' component 
	*				and 'day of week' component with '?' or single value (SUN, MON, TUE, WED, THU, FRI, SAT).
	* @param p_cron CRON string.
	* @return Boolean Returns true if current date is in CRON string, overwise returns false. 
	*/
	public static Boolean isRunToday(String p_cron) {
		String[] v_cronComponentList = p_cron.split(' ');
		if (v_cronComponentList[3] == '*') {
			return true;
		} else if (v_cronComponentList[5] == Datetime.now().format('EEE').toUpperCase()) {
			return true;
		}
		
		return false;
	}
}