global with sharing class MissingJobPostingScheduler implements Schedulable {

	global void execute(SchedulableContext p_schedulableContext) {
		
		AVTRRT__Config__c config = [
				SELECT a.AVTRRT__Value__c
				FROM AVTRRT__Config__c a
				WHERE a.AVTRRT__Name__c = 'Failed Jobs'
		];
		config.AVTRRT__Value__c = '';
		update config;

		Map<String, BCST__Job_Broadcasting_Scheduler__c> bsList =
				BCST__Job_Broadcasting_Scheduler__c.getAll();

		for (BCST__Job_Broadcasting_Scheduler__c bs : bsList.values()) {
			if (CronParser.isRunToday(bs.BCST__Cron__c)) {
				Database.executeBatch(new MissingJobPostingBatch(bs.BCST__Criteria__c, bs.BCST__Board_IDs__c), 1);
			}
		}

	}

}