@IsTest
private class LoggerTest {
	
	@IsTest
	private static void test() {
		
		Logger logg = new Logger('Test');
		logg = new Logger('Test', true);
		
		logg.log('Temp');
		logg.log('Temp', '');
		logg.log('Temp', '', '');
		Logger.addErrorMessage('temp');
		Logger.addWarningMessage('temp');
		Logger.addInfoMessage('temp');
		
		try {
			// Generates an exception.
			Integer i = 100 / 0;
		} catch (Exception e) {
			try {
				logg.catchBlockMethodNew(e);
			} catch (Exception ex) {
			}
		}
	}
}