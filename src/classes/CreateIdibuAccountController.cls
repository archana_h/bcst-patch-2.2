/**
* @author Shashish kumar
* @date 28/06/2013
* @description Controller class is used to create Idibu account or Idibu User or add services in Idibu Account.
*/
public class CreateIdibuAccountController {

	// Variables needed to add Account or User.
	public String firstName { get; set; }
	public String lastName { get; set; }
	public String email { get; set; }
	public String companyName { get; set; }
	public String userName { get; set; }
	public String password { get; set; }
	public String phoneNum { get; set; }

	//Additional variables needed to Add user.
	public String address { get; set; }
	public String addressLine1 { get; set; }
	public String addressLine2 { get; set; }
	public String addressLine3 { get; set; }
	public String country { get; set; }
	public String postcode { get; set; }
	public String www { get; set; }

	public Boolean isIdibuAccountExist { get; set; }
	String requestTo; // Variable helps to indentify which type of HTTP request needs to be called (To create Account/ to create User/ to add servies).

	public List<BCST__Job_Broadcasting_Settings__c> jobBroadcastingSettingList { get; set; }

	/**
	* @name CreateIdibuAccountController(Constructor)
	* @author Shashish kumar
	* @date 28/06/2013
	* @description Method is used to identify Idibu account is created or not with the help of Hash value.
	*/
	public CreateIdibuAccountController() {

		jobBroadcastingSettingList = [
				SELECT BCST__Idibu_Hash__c, SetupOwnerId,
						BCST__Idibu_User_Name__c, BCST__Idibu_Password__c
				FROM BCST__Job_Broadcasting_Settings__c
				WHERE BCST__Idibu_Hash__c != NULL
				LIMIT 1
		];
		// Check Idibu account created or not (identifed by Hash id exist or not).
		if (jobBroadcastingSettingList != null && jobBroadcastingSettingList.size() > 0) {
			isIdibuAccountExist = true;
			jobBroadcastingSettingList[0].SetupOwnerId = null;
			userName = jobBroadcastingSettingList[0].BCST__Idibu_User_Name__c;
			password = jobBroadcastingSettingList[0].BCST__Idibu_Password__c;
		} else {
			isIdibuAccountExist = false;
		}
		//isIdibuAccountExist = false;
	}

	/**
	* @name createAccount
	* @author Shashish kumar
	* @date 28/06/2013
	* @description Method is used to prepare XML for adding Account or User in Idibu
	* 			   and call the respective Http  request.
	* @return Returns error message on the same page else redirect it to Configuration page with success message.
	*/
	public PageReference createAccount() {
		String v_successMsg = '';
		String v_clientHash;
		//Check Idibu account created or not for instance
		if (isIdibuAccountExist == false) {

			requestTo = 'CreateAccount';

			Map<String, String> v_accInfoMap = new Map<String, String>();
			if (firstName != null && firstName != '' && lastName != null && lastName != ''
					&& companyName != null && companyName != '' && password != null && password != ''
					&& userName != null && userName != '' && phoneNum != null && phoneNum != ''
					&& email != null && email != '') {

				Boolean v_isValidEmail = isValid(email); // Call a method to check valid Email id.
				if (v_isValidEmail == true) {
					//Prepare Map, which will be used to Create XML String.
					v_accInfoMap.put('firstName', firstName);
					v_accInfoMap.put('lastName', lastName);
					v_accInfoMap.put('email', email);
					v_accInfoMap.put('username', userName);
					v_accInfoMap.put('password', password);
					v_accInfoMap.put('company-name', companyName);
					v_accInfoMap.put('phone', phoneNum);

					//Call a method by passing Map object to create XML String.
					String v_xmlString = IdibuAccountCreationHelper.createAccountCreationXml(v_accInfoMap);
					System.debug('....xmlString.....' + v_xmlString);

					//map<String,String> accResponseMap = new map<String,String>();
					//Call Http Request by passing XML string and type of request Like: Account Create.
					String v_accResponseString = IdibuAccountCreationHelper.callHttpRequest(v_xmlString, requestTo);
					String v_messageString = '';
					if (v_accResponseString != null && v_accResponseString != '') {
						//Add Http Response to Document object to read.
						Dom.Document v_document = new Dom.Document();
						v_document.load(v_accResponseString);
						Dom.XmlNode v_accResponse = v_document.getRootElement();
						Dom.XmlNode v_responseNode = v_accResponse.getChildElement('response', null);

						v_messageString = v_responseNode.getChildElement('message', null).getText();
						//check the response message and if its success then insert the Account information in salesforce custom setting.
						if (v_messageString == 'New client successfully created') {
							v_successMsg = v_messageString;
							v_clientHash = v_responseNode.getChildElement('client-hash', null).getText();
							//String v_clientId = v_responseNode.getChildElement('client-id', null).getText();
							//String v_memberId = v_responseNode.getChildElement('member-id', null).getText();

							BCST__Job_Broadcasting_Settings__c v_JBSobj = new BCST__Job_Broadcasting_Settings__c();
							v_JBSobj.BCST__Idibu_User_Id__c = 'noUser';
							v_JBSobj.BCST__Idibu_User_Name__c = userName;
							v_JBSobj.BCST__Idibu_Password__c = password;
							v_JBSobj.BCST__Idibu_Hash__c = v_clientHash;
							//v_JBSobj.SetupOwnerId = UserInfo.getUserId();
							// ---commented vy vishwajeet,Date-08/21/2013,To resolve Adding the values in default custom setting when first time a user profile gets created
							/*
							*@author-vishwajeet
							*@date-08/21/2013
							*@Decription : Changed the order of callout request and insert
							*@Case# :00020694(Callout request error)
							*/
							addAptrackServices(v_clientHash);//call method to add service.

							/*
							*@author-Pandeiswari
							*@date-03/08/2014
							*@Decription : Call methos to add global cc to Idibu account
							*@Case# :00023158
							*/

							String statusMessage = addGlobalccService(v_clientHash);
							if (statusMessage == 'Success') {
								v_JBSobj.BCST__global_CC_Added__c = true;
							}
							insert v_JBSobj;

							/*
							*Author :Pandeiswari
							* Date : 03/08/2014
							* Description : COde to add Global cc webservice.
							*/

							//system.debug(clientHash+'....accResponseMap.....'+clientId +'...UserInfo.getUserId()..'+UserInfo.getUserId());
						} else {
							ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.ERROR, v_messageString);
							ApexPages.addMessage(message);
						}

					}

				}//else{
				//	ApexPages.Message extfile = new ApexPages.Message(ApexPages.Severity.ERROR, 'Email is not in correct format(ex. smith@gmail.com)');
				//}
		} else {
			ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.ERROR, '' + System.Label.Field_Required_Message);
			ApexPages.addMessage(message);
		}
		} else {
			// Add "Hash" value to RequestTo string because Hash value also needed in url while sending Http Request to add user.
			requestTo = 'AddUser' + jobBroadcastingSettingList[0].BCST__Idibu_Hash__c;
			//system.debug(firstName + '.....firstName.... '+ lastName +' '+ email + ' '+companyName +' '+userName +' '+password +' '+phoneNum);

			Map<String, String> v_userInfoMap = new Map<String, String>();
			if (firstName != null && firstName != '' && lastName != null && lastName != ''
					&& companyName != null && companyName != '' && address != null && address != ''
					&& addressLine1 != null && addressLine1 != '' && addressLine2 != null && addressLine2 != ''
					&& addressLine3 != null && addressLine3 != '' && country != null && country != ''
					&& postcode != null && postcode != '' && email != null && email != ''
					&& phoneNum != null && phoneNum != '' && www != null && www != ''
					&& userName != null && userName != '' && password != null && password != '') {

				System.debug('....prepare User Info map....' + requestTo);
				Boolean v_isValidEmail = isValid(email); // Call a method to check valid Email id.

				if (v_isValidEmail == true) {
					//Prepare Map, which will be used to Create XML String.
					v_userInfoMap.put('firstName', firstName);
					v_userInfoMap.put('lastName', lastName);
					v_userInfoMap.put('company', companyName);
					v_userInfoMap.put('address', address);
					v_userInfoMap.put('addressLine1', addressLine1);
					v_userInfoMap.put('addressLine2', addressLine2);
					v_userInfoMap.put('addressLine3', addressLine3);
					v_userInfoMap.put('country', country);
					v_userInfoMap.put('postcode', postcode);
					v_userInfoMap.put('email', email);
					v_userInfoMap.put('phone', phoneNum);
					v_userInfoMap.put('www', www);
					//v_userInfoMap.put('login',userName);
					//v_userInfoMap.put('password',password);

					String v_xmlString = IdibuAccountCreationHelper.createUserInfoXml(v_userInfoMap);
					System.debug('....v_xmlString.....' + v_xmlString);

					//map<String,String> accResponseMap = new map<String,String>();
					//Call Http Request by passing XML string and type of request Like: Add User.
					String v_userResponseString = IdibuAccountCreationHelper.callHttpRequest(v_xmlString, requestTo);
					String v_messageString = '';
					if (v_userResponseString != null && v_userResponseString != '') {
						Dom.Document v_document = new Dom.Document();
						v_document.load(v_userResponseString);
						Dom.XmlNode v_userResponse = v_document.getRootElement();
						Dom.XmlNode v_responseNode = v_userResponse.getChildElement('response', null);

						v_messageString = v_responseNode.getChildElement('message', null).getText();
						//check the response message and if its success then insert the User information in salesforce custom setting.
						if (v_messageString == 'User added') {
							v_successMsg = v_messageString + ' successfully.';
							String userId = v_responseNode.getChildElement('id', null).getText();

							BCST__Job_Broadcasting_Settings__c v_JBSobj = new BCST__Job_Broadcasting_Settings__c();
							v_JBSobj.BCST__Idibu_User_Id__c = userId;
							v_JBSobj.BCST__Idibu_User_Name__c = userName;
							v_JBSobj.BCST__Idibu_Password__c = password;
							v_JBSobj.BCST__Idibu_Hash__c = jobBroadcastingSettingList[0].BCST__Idibu_Hash__c;
							v_JBSobj.SetupOwnerId = jobBroadcastingSettingList[0].SetupOwnerId;
							insert v_JBSobj;

							//system.debug(userId+'....userId.....'+jobBroadcastingSettingList[0].SetupOwnerId);
							//system.debug('....Hash Value.....'+jobBroadcastingSettingList[0].BCST__Idibu_Hash__c);

							// update Account user id.
							List<BCST__Job_Broadcasting_Settings__c> jobBroadcastingSettingList = [
									SELECT Id
									FROM BCST__Job_Broadcasting_Settings__c
									WHERE BCST__Idibu_Hash__c != NULL
											AND BCST__Idibu_User_Id__c = :'noUser'
									LIMIT 1
							];

							if (jobBroadcastingSettingList != null && jobBroadcastingSettingList.size() > 0) {
								jobBroadcastingSettingList[0].BCST__Idibu_User_Id__c = userId;
								update jobBroadcastingSettingList;
							}
						} else {
							ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.ERROR, v_messageString);
							ApexPages.addMessage(message);
						}
					}

				}
			} else {
				ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.ERROR, '' + System.Label.Field_Required_Message);
				ApexPages.addMessage(message);
			}
		}
		//if account or user successfully created, redirect the page back to Configureation page with Success message.
		if (v_successMsg.contains('successfully')) {
			String v_messageString = '';
			//assign success message value from the Custom setting.
			if (v_successMsg == 'User added successfully.') {

				v_messageString = System.Label.User_Added_Message;
			} else if (v_successMsg == 'New client successfully created') {

				v_messageString = System.Label.Account_Created_Message;
			}
			PageReference pg = Page.Configuration;
			pg.getParameters().put('msg', v_messageString);
			pg.setRedirect(true);
			return pg ;
		} else {
			return null;
		}
	}

	/*
	*Author : Pandeiswari
	* Date: 03/08/2014
	* Description : Method to add global cc service to Ididbu account
	*/

	private String addGlobalccService(String p_HashString) {
		requestTo = 'AddGlobalCC';
		String statusMessage = '';
		EmailServicesAddress emailService = [
				SELECT e.LocalPart,e.EmailDomainName
				FROM EmailServicesAddress e
				WHERE e.LocalPart = 'ProcessJobApplicantEmail'
				LIMIT 1
		];
		if (emailService != null) {
			String apTrackEmail = emailService.LocalPart + '@' + emailService.EmailDomainName;
			String v_xmlString;
			if (apTrackEmail != null) {
				v_xmlString = IdibuAccountCreationHelper.createGlobalCCServicesXml(p_HashString, apTrackEmail);
			}

			if (v_xmlString != null) {
				//Call Http request method to add global cc services.
				String v_globalCCeResponseString = IdibuAccountCreationHelper.callHttpRequest(v_xmlString, requestTo);
				if (v_globalCCeResponseString != null && v_globalCCeResponseString != '') {
					Dom.Document v_document = new Dom.Document();
					v_document.load(v_globalCCeResponseString);
					Dom.XmlNode v_serviceResponse = v_document.getRootElement();
					Dom.XmlNode v_responseNode = v_serviceResponse.getChildElement('response', null);
					String v_messageString = v_responseNode.getChildElement('message', null).getText();
					statusMessage = v_serviceResponse.getChildElement('status', null).getText();

					if (v_messageString != 'Client`s central CC email set') {
						ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.ERROR, v_messageString);
						ApexPages.addMessage(message);
					}
				}
			}
		}
		return statusMessage;
	}

	/**
	* @name addAptrackServices
	* @author Shashish kumar
	* @date 28/06/2013
	* @param p_HashString hash value from the existing Idibu Account.
	* @description Method is used to prepare XML for adding services in Idibu Account
	* 			   and call the respective Http  request.
	*/
	private void addAptrackServices(String p_HashString) {
		requestTo = 'AddServices';
		//List<BCST__Job_Broadcasting_Settings__c> jobBroadcastingSettingList = [select 	BCST__Idibu_Hash__c
		//	from BCST__Job_Broadcasting_Settings__c where BCST__Idibu_Hash__c!= null limit 1];

		if (p_HashString != null && p_HashString != '') {
			//Prepare XML string.
			String v_xmlString = IdibuAccountCreationHelper.createAptrackServicesXml(p_HashString);
			if (v_xmlString != null) {
				//Call Http request method to add services.
				String v_aptServiceResponseString = IdibuAccountCreationHelper.callHttpRequest(v_xmlString, requestTo);
				if (v_aptServiceResponseString != null && v_aptServiceResponseString != '') {
					Dom.Document v_document = new Dom.Document();
					v_document.load(v_aptServiceResponseString);
					Dom.XmlNode v_serviceResponse = v_document.getRootElement();
					Dom.XmlNode v_responseNode = v_serviceResponse.getChildElement('response', null);

					String v_messageString = v_responseNode.getChildElement('message', null).getText();
					if (v_messageString != 'Services added') {
						ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.ERROR, v_messageString);
						ApexPages.addMessage(message);
					}
					//system.debug(v_messageString+'....messageString.....');
				}
				//system.debug(xmlString+'......xmlString.....');
			}

		}

		//return null;
	}

	/**
	* @name isValid
	* @author Shashish kumar
	* @date 28/06/2013
	* @param p_email User Email id.
	* @description Method is used to Validate Email id.
	* @return If Email is valid return True value or else False.
	*/
	private Boolean isValid(String p_email) {
		String v_emailRegex = '^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';

		Pattern v_MyPattern = Pattern.compile(v_emailRegex);

		// then instantiate a new Matcher object â€MyMatcherâ€
		Matcher v_MyMatcher = v_MyPattern.matcher(p_email);

		if (!v_MyMatcher.matches()) {
			System.debug('...Not a Valid Email.....');//{!$Label.[Label_Name]}
			ApexPages.Message message = new ApexPages.Message(ApexPages.Severity.ERROR, '' + System.Label.InValid_Email_Message);
			ApexPages.addMessage(message);
			return false;
		} else {
			System.debug('...Valid Email.....');
			return true;
		}
	}
}